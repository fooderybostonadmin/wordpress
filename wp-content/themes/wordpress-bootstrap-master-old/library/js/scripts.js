/* imgsizer (flexible images for fluid sites) */
var imgSizer={Config:{imgCache:[],spacer:"/path/to/your/spacer.gif"},collate:function(aScope){var isOldIE=(document.all&&!window.opera&&!window.XDomainRequest)?1:0;if(isOldIE&&document.getElementsByTagName){var c=imgSizer;var imgCache=c.Config.imgCache;var images=(aScope&&aScope.length)?aScope:document.getElementsByTagName("img");for(var i=0;i<images.length;i++){images[i].origWidth=images[i].offsetWidth;images[i].origHeight=images[i].offsetHeight;imgCache.push(images[i]);c.ieAlpha(images[i]);images[i].style.width="100%";}
if(imgCache.length){c.resize(function(){for(var i=0;i<imgCache.length;i++){var ratio=(imgCache[i].offsetWidth/imgCache[i].origWidth);imgCache[i].style.height=(imgCache[i].origHeight*ratio)+"px";}});}}},ieAlpha:function(img){var c=imgSizer;if(img.oldSrc){img.src=img.oldSrc;}
var src=img.src;img.style.width=img.offsetWidth+"px";img.style.height=img.offsetHeight+"px";img.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+src+"', sizingMethod='scale')"
img.oldSrc=src;img.src=c.Config.spacer;},resize:function(func){var oldonresize=window.onresize;if(typeof window.onresize!='function'){window.onresize=func;}else{window.onresize=function(){if(oldonresize){oldonresize();}
func();}}}}

// add twitter bootstrap classes and color based on how many times tag is used
function addTwitterBSClass(thisObj) {
  var title = $(thisObj).attr('title');
  if (title) {
    var titles = title.split(' ');
    if (titles[0]) {
      var num = parseInt(titles[0]);
      if (num > 0)
      	$(thisObj).addClass('label');
      if (num == 2)
        $(thisObj).addClass('label label-info');
      if (num > 2 && num < 4)
        $(thisObj).addClass('label label-success');
      if (num >= 5 && num < 10)
        $(thisObj).addClass('label label-warning');
      if (num >=10)
        $(thisObj).addClass('label label-important');
    }
  }
  else
  	$(thisObj).addClass('label');
  return true;
}

// as the page loads, call these scripts
jQuery(document).ready(function($) {

	//page specific js
	if($('body').hasClass('wpsc')){
		$('.product_grid_display').each(function() {
			var $oEl = $(this),
				sHtml = '<div class="row-fluid"></div>';
			if (screen.width <= 480) {
				var perSlide = 1;
			} else {
				var perSlide = $oEl.attr('data-per-slide') || 2;
			}
			while ($oEl.children(".product_grid_item").length > 0) {
				$oEl.children(".product_grid_item").slice(0, perSlide).wrapAll(sHtml);
			}
		});		
	}
	//component specific js
	if ($('.cart-well').length){
		var $sidebar   = $(".cart-well"), 
    		$logo      = $("#cart-well-logo"),
        	$window    = $(window),
        	offset     = $sidebar.offset(),
        	topPadding = 30;

	    $window.scroll(function() {
			if($window.width() > 768){
		        if ($window.scrollTop() > offset.top) {
		            $sidebar.stop().animate({
		                marginTop: $window.scrollTop() - offset.top + topPadding
		            });
		            $logo.fadeIn();
		        } else {
		            $sidebar.stop().animate({
		              marginTop: 0
		            });
		            $logo.hide();
		        }
		    }
	    });
	}

	if ($('body').hasClass('woocommerce') && $('ul.products').length){
		//WooCommerce purchase page add-to-cart with quantity and AJAX, by customising the add-to-cart template in the WooCommerce loop. 
		//See blog post for details: http://snippets.webaware.com.au/snippets/woocommerce-add-to-cart-with-quantity-and-ajax/
		//https://gist.github.com/webaware/6260326
		/* when product quantity changes, update quantity attribute on add-to-cart button */
		$("form.cart").on("change", "input.qty", function() {
			//alert('change');
		    if (this.value === "0")
		        this.value = "1";
		 
		    $(this.form).find("button[data-quantity]").attr("data-quantity", this.value);
		});
		/* remove old "view cart" text, only need latest one thanks! */
		$(document.body).on("adding_to_cart", function() {
		    $("a.added_to_cart").remove();
		});		
	}

	// modify tag cloud links to match up with twitter bootstrap
	$("#tag-cloud a").each(function() {
	    addTwitterBSClass(this);
	    return true;
	});
	
	$("p.tags a").each(function() {
		addTwitterBSClass(this);
		return true;
	});
	
	$("ol.commentlist a.comment-reply-link").each(function() {
		$(this).addClass('btn btn-success btn-mini');
		return true;
	});
	
	$('#cancel-comment-reply-link').each(function() {
		$(this).addClass('btn btn-danger btn-mini');
		return true;
	});
	
	$('article.post').hover(function(){
		$('a.edit-post').show();
	},function(){
		$('a.edit-post').hide();
	});
	
	// Input placeholder text fix for IE
	$('[placeholder]').focus(function() {
	  var input = $(this);
	  if (input.val() == input.attr('placeholder')) {
		input.val('');
		input.removeClass('placeholder');
	  }
	}).blur(function() {
	  var input = $(this);
	  if (input.val() == '' || input.val() == input.attr('placeholder')) {
		input.addClass('placeholder');
		input.val(input.attr('placeholder'));
	  }
	}).blur();
	
	// Prevent submission of empty form
	$('[placeholder]').parents('form').submit(function() {
	  $(this).find('[placeholder]').each(function() {
		var input = $(this);
		if (input.val() == input.attr('placeholder')) {
		  input.val('');
		}
	  })
	});
	
	$('#s').focus(function(){
		if( $(window).width() < 940 ){
			$(this).animate({ width: '200px' });
		}
	});
	
	$('#s').blur(function(){
		if( $(window).width() < 940 ){
			$(this).animate({ width: '100px' });
		}
	});
			
	$('.alert-message').alert();
	
	$('.dropdown-toggle').dropdown();
 
}); /* end of as page load scripts */