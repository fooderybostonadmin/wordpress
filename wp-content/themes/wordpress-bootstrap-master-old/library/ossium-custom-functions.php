<?php

	function enqueue_style_after_wc(){
	    $deps = class_exists( 'WooCommerce' ) ? array( 'woocommerce-layout', 'woocommerce-smallscreen', 'woocommerce-general' ) : array();
	    wp_enqueue_style( 'zocalo-woocommerce-overrides', get_template_directory_uri() . '/library/css/woocommerce-overrides.css', $deps, '1.0' );
	}

	add_action( 'wp_enqueue_scripts', 'enqueue_style_after_wc' );

	// Adding back in the neat plus/minus quantity next to the add to cart button
	add_action( 'wp_enqueue_scripts', 'wcqi_enqueue_polyfill' );
	function wcqi_enqueue_polyfill() {
	    wp_enqueue_script( 'wcqi-number-polyfill' );
	}

	function woocommerce_remove_related_products(){
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
	}

	add_action('woocommerce_after_single_product_summary', 'woocommerce_remove_related_products');

	// Removing stuff from woocomercer_single_product_summary
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price',10 );


	// Getting rid of the tabs we don't need and adding news ones
	add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

	function woo_remove_product_tabs( $tabs ) {

	    unset( $tabs['description'] );      	// Remove the description tab
	    unset( $tabs['reviews'] ); 			// Remove the reviews tab
	    unset( $tabs['additional_information'] );  	// Remove the additional information tab

		$tabs['nutrition'] = array(
			'title'   	=> __( 'Nutrition Information', 'woocommerce' ),
			'priority'	=> 50,
			'callback'	=> 'woo_nutrition_tab_content'
		);

	    return $tabs;

	}

	function woo_nutrition_tab_content() {

		global $post;

		// The new tab content

		//echo '<h2>New Product Tab</h2>';
		echo $post->post_excerpt;

	}

	function woocommerce_theme_support() {
		// add support for woocommerce
		add_theme_support( 'woocommerce' );
		define('WOOCOMMERCE_USE_CSS', false);
	}
	// launching this stuff after theme setup
	add_action('after_setup_theme','woocommerce_theme_support');


	// Change number or products per row to 3
	// http://docs.woothemes.com/document/change-number-of-products-per-row/
	add_filter('loop_shop_columns', 'loop_columns');
	if (!function_exists('loop_columns')) {
		function loop_columns() {
			return 2; // 3 products per row
		}
	}

	// let's get our cute little shopping cart icon in there
	add_filter( 'add_to_cart_text', 'woo_custom_cart_button_text' );
	//add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
	function woo_custom_cart_button_text() {
        return __( '<i class="icon-shopping-cart icon-white"></i> Add To Cart', 'woocommerce' );
	}

	// add_filter('adding','woocommerce_single_product_summary')
	// woocommerce_single_product_summary

	// Display variation's price even if min and max prices are the same
	add_filter('woocommerce_available_variation', function ($value, $object = null, $variation = null) {
			if ($value['price_html'] == '') {
				$value['price_html'] = '<span class="price">' . $variation->get_price_html() . '</span>';
			}
			return $value;
		}, 10, 3);

	// Abbreviated Order Box
	// @see woocommerce_variable_add_to_cart()
	// @see woocommerce_template_single_meta()

	add_action( 'woocommerce_single_product_summary_mini', 'woocommerce_template_single_add_to_cart', 30 );
	add_action( 'woocommerce_single_product_summary_mini', 'woocommerce_template_single_meta', 10 );
