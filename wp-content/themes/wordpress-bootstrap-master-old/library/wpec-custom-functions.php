<?php

	function get_alergen_terms($postID){

		$terms = get_the_terms( $post->ID, 'allergen' );

		if ( $terms && ! is_wp_error( $terms ) ) {

			$allergen_links = array();

			foreach ( $terms as $term ) {
				$allergen_links[] = $term->name;
			}

			$allergen = join( ", ", $allergen_links );

			echo 'Allergens: <span>'.$allergen.'</span>';
		}
	};
?>
