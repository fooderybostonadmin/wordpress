<?php get_header(); ?>
			
<div class="row-fluid brownbg">
		<h1 class="text-center">We take pride in community and sustainability. <br><strong>See what we're up to.</strong></h1>
    </div><p>&nbsp;</p>			
			<div id="content" class="clearfix row-fluid">
			<div class="container">
				<div id="main" class="span9 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="clearfix article">
						<h1 class=""><a href="<?php  echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
						<?php the_post_thumbnail( 'medium', array('class' => 'featurette-image img-polaroid pull-left') ); ?> 
						<div class=""><?php the_content(); //get_template_part( 'content', get_post_format() ); ?></div>					
						</div>
					<?php endwhile; ?>	
					
					<?php if (function_exists('page_navi')) { // if expirimental feature is active ?>
						
						<?php page_navi(); // use the page navi function ?>
						
					<?php } else { // if it is disabled, display regular wp prev & next links ?>
						<nav class="wp-prev-next">
							<ul class="clearfix">
								<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
								<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
							</ul>
						</nav>
					<?php } ?>		
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php get_sidebar(); // sidebar 1 ?>
			</div> <!-- end container -->
			</div> <!-- end #content -->

<?php get_footer(); ?>