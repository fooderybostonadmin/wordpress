<?php
/*
Template Name: How It Works
*/
?>

<?php get_header(); ?>

			<div id="content" class="clearfix row-fluid">

				<div id="main" class="span12 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php get_template_part('partials/tres-tiles');?>
						<?php get_template_part('partials/featurette');?>
						<?php get_template_part('partials/weekly-menu') ?>
						<?php get_template_part('partials/vs');?>



					<?php endwhile; ?>

					<?php else : ?>

					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>

					<?php endif; ?>

				</div> <!-- end #main -->

			</div> <!-- end #content -->

<?php get_footer(); ?>