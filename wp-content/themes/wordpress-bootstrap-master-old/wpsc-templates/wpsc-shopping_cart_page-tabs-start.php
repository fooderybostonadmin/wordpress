<?php
global $wpsc_cart, $wpdb, $wpsc_checkout, $wpsc_gateway, $wpsc_coupons, $wpsc_registration_error_messages;
$wpsc_checkout = new wpsc_checkout();
$alt = 0;
$coupon_num = wpsc_get_customer_meta( 'coupon' );
if( $coupon_num )
   $wpsc_coupons = new wpsc_coupons( $coupon_num );
// var_dump(wpsc_cart_total(false,false,false));
if( wpsc_cart_item_count() < 1 ) :
   echo 'You need 4 meals or a gift card in your shopping cart to checkout.</';
   echo "<br/><br><a href=" . esc_url( get_option( "product_list_url" ) ) . "> Please visit our shop.</a>";
   return;
endif;
?>
<div id="checkout_page_container">

<!-------->
<div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#login" data-toggle="tab"><?php echo (is_user_logged_in() ? 'Review Order' : 'Login, Register or Guest');?></a></li>
        <li><a href="#orange" data-toggle="tab">Orange</a></li>
        <li><a href="#yellow" data-toggle="tab">Yellow</a></li>
        <li><a href="#green" data-toggle="tab">Green</a></li>
        <li><a href="#blue" data-toggle="tab">Blue</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="red">

            <?php if ( !is_user_logged_in() ) {
				echo '<h1>Login or Register</h1>';
	            echo do_shortcode('[theme-my-login instance="1" default_action="login"]');
            } else { ?>
<h3><?php _e('Please review your order', 'wpsc'); ?></h3>
<table class="table table-striped">
	<thead>
	   <tr class="header">
	      <th colspan="2" ><?php _e('Product', 'wpsc'); ?></th>
	      <th><?php _e('Quantity', 'wpsc'); ?></th>
	      <th><?php _e('Price', 'wpsc'); ?></th>
	      <th><?php _e('Total', 'wpsc'); ?></th>
	        <th>&nbsp;</th>
	   </tr>
	</thead>
	<tbody>
   <?php while (wpsc_have_cart_items()) : wpsc_the_cart_item(); ?>
      <?php
       $alt++;
       if ($alt %2 == 1)
         $alt_class = 'alt';
       else
         $alt_class = '';
       ?>
      <?php  //this displays the confirm your order html ?>

	  <?php do_action ( "wpsc_before_checkout_cart_row" ); ?>
      <tr class="product_row product_row_<?php echo wpsc_the_cart_item_key(); ?> <?php echo $alt_class;?>">

         <td class="firstcol wpsc_product_image wpsc_product_image_<?php echo wpsc_the_cart_item_key(); ?>">
         <?php if('' != wpsc_cart_item_image()): ?>
			<?php do_action ( "wpsc_before_checkout_cart_item_image" ); ?>
            <img src="<?php echo wpsc_cart_item_image(); ?>" alt="<?php echo wpsc_cart_item_name(); ?>" title="<?php echo wpsc_cart_item_name(); ?>" class="product_image" />
			<?php do_action ( "wpsc_after_checkout_cart_item_image" ); ?>
         <?php else:
         /* I dont think this gets used anymore,, but left in for backwards compatibility */
         ?>
            <div class="item_no_image">
				<?php do_action ( "wpsc_before_checkout_cart_item_image" ); ?>
               <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>">
               <span><?php _e('No Image','wpsc'); ?></span>

               </a>
				<?php do_action ( "wpsc_after_checkout_cart_item_image" ); ?>
            </div>
         <?php endif; ?>
         </td>

         <td class="wpsc_product_name wpsc_product_name_<?php echo wpsc_the_cart_item_key(); ?>">
			<?php do_action ( "wpsc_before_checkout_cart_item_name" ); ?>
            <a href="<?php echo esc_url( wpsc_cart_item_url() );?>"><?php echo wpsc_cart_item_name(); ?></a>
			<?php do_action ( "wpsc_after_checkout_cart_item_name" ); ?>
         </td>

         <td class="wpsc_product_quantity wpsc_product_quantity_<?php echo wpsc_the_cart_item_key(); ?>">
            <form action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" method="post" class="adjustform qty">
               <input type="text" name="quantity" size="2" value="<?php echo wpsc_cart_item_quantity(); ?>" class="quant_small" />
               <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
               <input type="hidden" name="wpsc_update_quantity" value="true" />
               <button class="btn" type="submit"><i class="icon-refresh"></i></button>
            </form>
         </td>


            <td><?php echo wpsc_cart_single_item_price(); ?></td>
         <td class="wpsc_product_price wpsc_product_price_<?php echo wpsc_the_cart_item_key(); ?>"><span class="pricedisplay"><?php echo wpsc_cart_item_price(); ?></span></td>

         <td class="wpsc_product_remove wpsc_product_remove_<?php echo wpsc_the_cart_item_key(); ?>">
            <form action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" method="post" class="adjustform remove">
               <input type="hidden" name="quantity" value="0" />
               <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
               <input type="hidden" name="wpsc_update_quantity" value="true" />
               <button type="submit" class="btn btn-danger"><i class="icon-remove icon-white"></i></button>


<!--                <input type="submit" value="<?php _e('Remove', 'wpsc'); ?>" /> -->
            </form>
         </td>
      </tr>
	  <?php do_action ( "wpsc_after_checkout_cart_row" ); ?>
   <?php endwhile; ?>
	</tbody>
   </table>

          <?php  }


            ;?>
        </div>
        <div class="tab-pane" id="orange">
            <h1>Orange</h1>
            <p>orange orange orange orange orange</p>
        </div>
        <div class="tab-pane" id="yellow">
            <h1>Yellow</h1>
            <p>yellow yellow yellow yellow yellow</p>
        </div>
        <div class="tab-pane" id="green">
            <h1>Green</h1>
            <p>green green green green green</p>
        </div>
        <div class="tab-pane" id="blue">
            <h1>Blue</h1>
            <p>blue blue blue blue blue</p>
        </div>
    </div>
</div>

<!-- <div id="checkout_page_container"> -->
<h3><?php _e('Please review your order', 'wpsc'); ?></h3>
<table class="table table-striped">
	<thead>
	   <tr class="header">
	      <th colspan="2" ><?php _e('Product', 'wpsc'); ?></th>
	      <th><?php _e('Quantity', 'wpsc'); ?></th>
	      <th><?php _e('Price', 'wpsc'); ?></th>
	      <th><?php _e('Total', 'wpsc'); ?></th>
	        <th>&nbsp;</th>
	   </tr>
	</thead>
	<tbody>
   <?php while (wpsc_have_cart_items()) : wpsc_the_cart_item(); ?>
      <?php
       $alt++;
       if ($alt %2 == 1)
         $alt_class = 'alt';
       else
         $alt_class = '';
       ?>
      <?php  //this displays the confirm your order html ?>

	  <?php do_action ( "wpsc_before_checkout_cart_row" ); ?>
      <tr class="product_row product_row_<?php echo wpsc_the_cart_item_key(); ?> <?php echo $alt_class;?>">

         <td class="firstcol wpsc_product_image wpsc_product_image_<?php echo wpsc_the_cart_item_key(); ?>">
         <?php if('' != wpsc_cart_item_image()): ?>
			<?php do_action ( "wpsc_before_checkout_cart_item_image" ); ?>
            <img src="<?php echo wpsc_cart_item_image(); ?>" alt="<?php echo wpsc_cart_item_name(); ?>" title="<?php echo wpsc_cart_item_name(); ?>" class="product_image" />
			<?php do_action ( "wpsc_after_checkout_cart_item_image" ); ?>
         <?php else:
         /* I dont think this gets used anymore,, but left in for backwards compatibility */
         ?>
            <div class="item_no_image">
				<?php do_action ( "wpsc_before_checkout_cart_item_image" ); ?>
               <a href="<?php echo esc_url( wpsc_the_product_permalink() ); ?>">
               <span><?php _e('No Image','wpsc'); ?></span>

               </a>
				<?php do_action ( "wpsc_after_checkout_cart_item_image" ); ?>
            </div>
         <?php endif; ?>
         </td>

         <td class="wpsc_product_name wpsc_product_name_<?php echo wpsc_the_cart_item_key(); ?>">
			<?php do_action ( "wpsc_before_checkout_cart_item_name" ); ?>
            <a href="<?php echo esc_url( wpsc_cart_item_url() );?>"><?php echo wpsc_cart_item_name(); ?></a>
			<?php do_action ( "wpsc_after_checkout_cart_item_name" ); ?>
         </td>

         <td class="wpsc_product_quantity wpsc_product_quantity_<?php echo wpsc_the_cart_item_key(); ?>">
            <form action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" method="post" class="adjustform qty">
               <input type="text" name="quantity" size="2" value="<?php echo wpsc_cart_item_quantity(); ?>" class="quant_small" />
               <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
               <input type="hidden" name="wpsc_update_quantity" value="true" />
               <button class="btn" type="submit"><i class="icon-refresh"></i></button>
            </form>
         </td>


            <td><?php echo wpsc_cart_single_item_price(); ?></td>
         <td class="wpsc_product_price wpsc_product_price_<?php echo wpsc_the_cart_item_key(); ?>"><span class="pricedisplay"><?php echo wpsc_cart_item_price(); ?></span></td>

         <td class="wpsc_product_remove wpsc_product_remove_<?php echo wpsc_the_cart_item_key(); ?>">
            <form action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" method="post" class="adjustform remove">
               <input type="hidden" name="quantity" value="0" />
               <input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
               <input type="hidden" name="wpsc_update_quantity" value="true" />
               <button type="submit" class="btn btn-danger"><i class="icon-remove icon-white"></i></button>


<!--                <input type="submit" value="<?php _e('Remove', 'wpsc'); ?>" /> -->
            </form>
         </td>
      </tr>
	  <?php do_action ( "wpsc_after_checkout_cart_row" ); ?>
   <?php endwhile; ?>
	</tbody>
   </table>
   <?php //this HTML displays coupons if there are any active coupons to use ?>
   <!-- START HERE -->
   <div id="not-enough-in-cart">
   	You must have at least four meals or a gift certificate in your cart to checkout.
   </div>
   <div id="hidden1">
   <?php

   if(wpsc_uses_coupons()): ?>

      <?php if(wpsc_coupons_error()): ?>
         <div class="wpsc_coupon_row wpsc_coupon_error_row">
         	<?php _e('Coupon is not valid.', 'wpsc'); ?>
         </div>
      <?php endif; ?>
	      <div class="wpsc_coupon_row">
			 <h3>Coupon or Gift Card</h3>
			 <?php _e('Enter coupon or gift card code below', 'wpsc'); ?>
	         <div  colspan="4" class="coupon_code">
	            <form  method="post" action="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>">
	               <input type="text" name="coupon_num" id="coupon_num" value="<?php echo $wpsc_cart->coupons_name; ?>" />
	               <input type="submit" value="<?php _e('Update', 'wpsc') ?>" />
	            </form>
	         </div>
      </div>
      <div class="wpsc_total_before_shipping">
	      <?php _e('Cost before delivery charge:','wpsc'); ?> <span class="wpsc_total_amount_before_shipping"><?php echo wpsc_cart_total_widget(false,false,false);?></span>
      </div>
   <?php endif; ?>
   <!-- cart contents table close -->
  <?php if(wpsc_uses_shipping()): ?>
	   <p class="wpsc_cost_before"></p>
   <?php endif; ?>
   <?php  //this HTML dispalys the calculate your order HTML   ?>

   <?php if(wpsc_has_category_and_country_conflict()): ?>
      <p class='validation-error'><?php echo esc_html( wpsc_get_customer_meta( 'category_shipping_conflict' ) ); ?></p>
   <?php endif; ?>

   <?php if(isset($_SESSION['WpscGatewayErrorMessage']) && $_SESSION['WpscGatewayErrorMessage'] != '') :?>
      <p class="validation-error"><?php echo $_SESSION['WpscGatewayErrorMessage']; ?></p>
   <?php
   endif;
   ?>

   <?php do_action('wpsc_before_shipping_of_shopping_cart'); ?>

   <div id="wpsc_shopping_cart_container">
   <?php if(wpsc_uses_shipping()) : ?>
      <h3><?php _e('Calculate Delivery Charge', 'wpsc'); ?></h3>
      <table class="productcart hidden-shipping">
         <tr class="wpsc_shipping_info">
            <td colspan="5">
               <?php _e('Please enter the <strong>delivery zipcode</strong> to calculate your delivery charge', 'wpsc'); ?>
            </td>
         </tr>
		 <tr>
		 	<td colspan="5">
			 	<form id="zip_calc">
			 		<input name="zip_calc_code" type="text" maxlength="5" placeholder="Zip Code"/>
			 		<button type="submit" class="btn btn-success">Calculate</button>
			 	</form>
		 	</td>
		 </tr>


         <?php if (wpsc_have_morethanone_shipping_quote()) :?>
            <?php while (wpsc_have_shipping_methods()) : wpsc_the_shipping_method(); ?>
                  <?php    if (!wpsc_have_shipping_quotes()) { continue; } // Don't display shipping method if it doesn't have at least one quote ?>
                  <tr class='hide wpsc_shipping_header'><td class='shipping_header' colspan='5'><?php echo wpsc_shipping_method_name().__(' - Choose a Shipping Rate', 'wpsc'); ?> </td></tr>
                  <?php while (wpsc_have_shipping_quotes()) : wpsc_the_shipping_quote();  ?>
                     <tr class='hide <?php echo wpsc_shipping_quote_html_id(); ?>'>
                        <td class='wpsc_shipping_quote_name wpsc_shipping_quote_name_<?php echo wpsc_shipping_quote_html_id(); ?>' colspan='3'>
                           <label for='<?php echo wpsc_shipping_quote_html_id(); ?>'><?php echo wpsc_shipping_quote_name(); ?></label>
                        </td>
                        <td class='wpsc_shipping_quote_price wpsc_shipping_quote_price_<?php echo wpsc_shipping_quote_html_id(); ?>' style='text-align:center;'>
                           <label for='<?php echo wpsc_shipping_quote_html_id(); ?>'><?php echo wpsc_shipping_quote_value(); ?></label>
                        </td>
                        <td class='wpsc_shipping_quote_radio wpsc_shipping_quote_radio_<?php echo wpsc_shipping_quote_html_id(); ?>' style='text-align:center;'>
                           <?php if(wpsc_have_morethanone_shipping_methods_and_quotes()): ?>
                              <input type='radio' id='<?php echo wpsc_shipping_quote_html_id(); ?>' <?php echo wpsc_shipping_quote_selected_state(); ?>  onclick='switchmethod("<?php echo wpsc_shipping_quote_name(); ?>", "<?php echo wpsc_shipping_method_internal_name(); ?>")' value='<?php echo wpsc_shipping_quote_value(true); ?>' name='shipping_method' />
                           <?php else: ?>
                              <input <?php echo wpsc_shipping_quote_selected_state(); ?> disabled='disabled' type='radio' id='<?php echo wpsc_shipping_quote_html_id(); ?>'  value='<?php echo wpsc_shipping_quote_value(true); ?>' name='shipping_method' />
                                 <?php wpsc_update_shipping_single_method(); ?>
                           <?php endif; ?>
                        </td>
                     </tr>
                  <?php endwhile; ?>
            <?php endwhile; ?>
         <?php endif; ?>

         <?php wpsc_update_shipping_multiple_methods(); ?>


         <?php if (!wpsc_have_shipping_quote()) : // No valid shipping quotes ?>
               </table>
               </div>
			</div>
            <?php return; ?>
         <?php endif; ?>
      </table>
   <?php endif;  ?>

   <?php
      $wpec_taxes_controller = new wpec_taxes_controller();
      if($wpec_taxes_controller->wpec_taxes_isenabled()):
   ?>
      <table class="productcart">
         <tr class="total_price total_tax">
            <td colspan="3">
               <?php echo wpsc_display_tax_label(true); ?>
            </td>
            <td colspan="2">
               <span id="checkout_tax" class="pricedisplay checkout-tax"><?php echo wpsc_cart_tax(); ?></span>
            </td>
         </tr>
      </table>
   <?php endif; ?>
   <?php do_action('wpsc_before_form_of_shopping_cart'); ?>

	<?php if( ! empty( $wpsc_registration_error_messages ) ): ?>
		<p class="validation-error">
		<?php
		foreach( $wpsc_registration_error_messages as $user_error )
		 echo $user_error."<br />\n";
		?>
	<?php endif; ?>

	<?php if ( wpsc_show_user_login_form() && !is_user_logged_in() ): ?>
			<p><?php _e('You must sign in or register with us to continue with your purchase', 'wpsc');?></p>
			<div class="wpsc_registration_form">

				<fieldset class='wpsc_registration_form'>
					<h2><?php _e( 'Sign in', 'wpsc' ); ?></h2>
					<?php
					$args = array(
						'remember' => false,
                    	'redirect' => home_url( $_SERVER['REQUEST_URI'] )
					);
					wp_login_form( $args );
					?>
					<div class="wpsc_signup_text"><?php _e('If you have bought from us before please sign in here to purchase', 'wpsc');?></div>
				</fieldset>
			</div>
	<?php endif; ?>
   <table class='wpsc_checkout_table wpsc_checkout_table_totals'>
      <?php if(wpsc_uses_shipping()) : ?>
	      <tr class="total_price total_shipping">
	         <td class='wpsc_totals'>
	            <?php _e('Total Shipping:', 'wpsc'); ?>
	         </td>
	         <td class='wpsc_totals'>
	            <span id="checkout_shipping" class="pricedisplay checkout-shipping"><?php echo wpsc_cart_shipping(); ?></span>
	         </td>
	      </tr>
      <?php endif; ?>

     <?php if(wpsc_uses_coupons() && (wpsc_coupon_amount(false) > 0)): ?>
      <tr class="total_price">
         <td class='wpsc_totals'>
            <?php _e('Discount:', 'wpsc'); ?>
         </td>
         <td class='wpsc_totals'>
            <span id="coupons_amount" class="pricedisplay"><?php echo wpsc_coupon_amount(); ?></span>
          </td>
         </tr>
     <?php endif ?>



   <tr class='total_price'>
      <td class='wpsc_totals'>
      <?php _e('Total Price:', 'wpsc'); ?>
      </td>
      <td class='wpsc_totals'>
         <span id='checkout_total' class="pricedisplay checkout-total"><?php echo wpsc_cart_total(); ?></span>
      </td>
   </tr>
   </table>

	<form class='wpsc_checkout_forms' action='<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>' method='post' enctype="multipart/form-data">
      <?php
      /**
       * Both the registration forms and the checkout details forms must be in the same form element as they are submitted together, you cannot have two form elements submit together without the use of JavaScript.
      */
      ?>

    <?php if(wpsc_show_user_login_form()):
          global $current_user;
          get_currentuserinfo();   ?>

		<div class="wpsc_registration_form">

	        <fieldset class='wpsc_registration_form wpsc_right_registration'>
	        	<h2><?php _e('Join up now', 'wpsc');?></h2>

				<label><?php _e('Username:', 'wpsc'); ?></label>
				<input type="text" name="log" id="log" value="" size="20"/><br/>

				<label><?php _e('Password:', 'wpsc'); ?></label>
				<input type="password" name="pwd" id="pwd" value="" size="20" /><br />

				<label><?php _e('E-mail', 'wpsc'); ?>:</label>
	            <input type="text" name="user_email" id="user_email" value="" size="20" /><br />
	            <div class="wpsc_signup_text"><?php _e('Signing up is free and easy! please fill out your details your registration will happen automatically as you checkout. Don\'t forget to use your details to login with next time!', 'wpsc');?></div>
	        </fieldset>

        </div>
        <div class="clear"></div>
   <?php endif; // closes user login form
      $misc_error_messages = wpsc_get_customer_meta( 'checkout_misc_error_messages' );
      if( ! empty( $misc_error_messages ) ): ?>
         <div class='login_error'>
            <?php foreach( $misc_error_messages as $user_error ){?>
               <p class='validation-error'><?php echo $user_error; ?></p>
               <?php } ?>
         </div>

      <?php
      endif;
      ?>
<?php ob_start(); ?>
   <table class='wpsc_checkout_table table-1 pull-left span6'>
      <?php $i = 0;
      while (wpsc_have_checkout_items()) : wpsc_the_checkout_item(); ?>

        <?php if(wpsc_checkout_form_is_header() == true){
               $i++;
               //display headers for form fields ?>
               <?php if($i > 1):?>
                  </table>
                  <table class='wpsc_checkout_table table-<?php echo $i; ?> pull-left span6'>
               <?php endif; ?>

               <tr <?php echo wpsc_the_checkout_item_error_class();?>>
                  <td <?php wpsc_the_checkout_details_class(); ?> colspan='2'>
                     <h4><?php echo wpsc_checkout_form_name();?></h4>
                  </td>
               </tr>
               <?php if(wpsc_is_shipping_details()):?>
               <tr class='same_as_shipping_row'>
                  <td colspan ='2'>
                  <?php $checked = '';
                  $shipping_same_as_billing = wpsc_get_customer_meta( 'shipping_same_as_billing' );
                  if(isset($_POST['shippingSameBilling']) && $_POST['shippingSameBilling'])
                     $shipping_same_as_billing = true;
                  elseif(isset($_POST['submit']) && !isset($_POST['shippingSameBilling']))
                  	$shipping_same_as_billing = false;
                  wpsc_update_customer_meta( 'shipping_same_as_billing', $shipping_same_as_billing );
                  	if( $shipping_same_as_billing )
                  		$checked = 'checked="checked"';
                   ?>
					<label for='shippingSameBilling'><?php _e('Same as billing address:','wpsc'); ?></label>
					<input type='checkbox' value='true' name='shippingSameBilling' id='shippingSameBilling' <?php echo $checked; ?> />
					<br/><span id="shippingsameasbillingmessage"><?php _e('Your order will be shipped to the billing address', 'wpsc'); ?></span>
                  </td>
               </tr>
               <?php endif;

            // Not a header so start display form fields
            }elseif(wpsc_disregard_shipping_state_fields()){
            ?>
               <tr class='wpsc_hidden'>
                  <td class='<?php echo wpsc_checkout_form_element_id(); ?>'>
                     <label for='<?php echo wpsc_checkout_form_element_id(); ?>'>
                     <?php echo wpsc_checkout_form_name();?>
                     </label>
                  </td>
                  <td>
                     <?php echo wpsc_checkout_form_field();?>
                      <?php if(wpsc_the_checkout_item_error() != ''): ?>
                             <p class='validation-error'><?php echo wpsc_the_checkout_item_error(); ?></p>
                     <?php endif; ?>
                  </td>
               </tr>
            <?php
            }elseif(wpsc_disregard_billing_state_fields()){
            ?>
               <tr class='wpsc_hidden'>
                  <td class='<?php echo wpsc_checkout_form_element_id(); ?>'>
                     <label for='<?php echo wpsc_checkout_form_element_id(); ?>'>
                     <?php echo wpsc_checkout_form_name();?>
                     </label>
                  </td>
                  <td>
                     <?php echo wpsc_checkout_form_field();?>
                      <?php if(wpsc_the_checkout_item_error() != ''): ?>
                             <p class='validation-error'><?php echo wpsc_the_checkout_item_error(); ?></p>
                     <?php endif; ?>
                  </td>
               </tr>
            <?php
            }elseif( $wpsc_checkout->checkout_item->unique_name == 'billingemail'){ ?>
               <?php $email_markup =
               "<h3>Email Address</h3>
               	<div class='wpsc_email_address'>
                  <p class='" . wpsc_checkout_form_element_id() . "'>
                     <label class='wpsc_email_address' for='" . wpsc_checkout_form_element_id() . "'>
                     " . __('Enter your email address', 'wpsc') . "
                     </label>
                  <p class='wpsc_email_address_p'>
                  " . wpsc_checkout_form_field();

                   if(wpsc_the_checkout_item_error() != '')
                      $email_markup .= "<p class='validation-error'>" . wpsc_the_checkout_item_error() . "</p>";
               $email_markup .= "</div>";
             }else{ ?>
			<tr>
               <td class='<?php echo wpsc_checkout_form_element_id(); ?>'>
                  <label for='<?php echo wpsc_checkout_form_element_id(); ?>'>
                  <?php echo wpsc_checkout_form_name();?>
                  </label>
               </td>
               <td>
                  <?php echo wpsc_checkout_form_field();?>
                   <?php if(wpsc_the_checkout_item_error() != ''): ?>
                          <p class='validation-error'><?php echo wpsc_the_checkout_item_error(); ?></p>
                  <?php endif; ?>
               </td>
            </tr>

         <?php }//endif; ?>

      <?php endwhile; ?>

<?php
	$buffer_contents = ob_get_contents();
	ob_end_clean();
	if(isset($email_markup))
		echo $email_markup;
	echo $buffer_contents;
?>

      <?php if (wpsc_show_find_us()) : ?>
      <tr>
         <td><label for='how_find_us'><?php _e('How did you find us' , 'wpsc'); ?></label></td>
         <td>
            <select name='how_find_us'>
               <option value='Word of Mouth'><?php _e('Word of mouth' , 'wpsc'); ?></option>
               <option value='Advertisement'><?php _e('Advertising' , 'wpsc'); ?></option>
               <option value='Internet'><?php _e('Internet' , 'wpsc'); ?></option>
               <option value='Customer'><?php _e('Existing Customer' , 'wpsc'); ?></option>
            </select>
         </td>
      </tr>
  </table>
      <?php endif; ?>
      <?php do_action('wpsc_inside_shopping_cart'); ?>
	  <table class="payment-well clearfix span9 pull-left">
      <?php  //this HTML displays activated payment gateways   ?>
      <?php if(wpsc_gateway_count() > 1): // if we have more than one gateway enabled, offer the user a choice ?>
         <tr>
            <td colspan='2' class='wpsc_gateway_container'>
               <h3><?php _e('Payment Type', 'wpsc');?></h3>
               <?php wpsc_gateway_list(); ?>
               </td>
         </tr>
      <?php else: // otherwise, there is no choice, stick in a hidden form ?>
         <tr>
            <td colspan="2" class='wpsc_gateway_container'>
               <?php wpsc_gateway_hidden_field(); ?>
            </td>
         </tr>
      <?php endif; ?>

      <?php if(wpsc_has_tnc()) : ?>
         <tr>
            <td colspan='2'>
                <label for="agree"><input id="agree" type='checkbox' value='yes' name='agree' /> <?php printf(__("I agree to the <a class='thickbox' target='_blank' href='%s' class='termsandconds'>Terms and Conditions</a>", "wpsc"), esc_url( home_url( "?termsandconds=true&amp;width=360&amp;height=400" ) ) ); ?> <span class="asterix">*</span></label>
               </td>
         </tr>
      <?php endif; ?>
      </table>

<!-- div for make purchase button -->
      <div class='wpsc_make_purchase span9 pull-left'>
         <span>
            <?php if(!wpsc_has_tnc()) : ?>
               <input type='hidden' value='yes' name='agree' />
            <?php endif; ?>
               <input type='hidden' value='submit_checkout' name='wpsc_action' />
               <button type='submit' class='make_purchase wpsc_buy_button btn btn-success btn-large'><?php _e('Purchase', 'wpsc');?></button>

<!--                <input type='submit' value='<?php _e('Purchase', 'wpsc');?>' class='make_purchase wpsc_buy_button' /> -->
         </span>
      </div>
	  </form>
	</div>
	</div><!-- /#hidden-stuff -->
</div><!--close checkout_page_container-->

<script>
	//declare the arrays coming from the options panel
	<?php 	$zones = get_field('zone', 'option');
			$jsZones = array();
			foreach($zones as $zone){
				array_push($jsZones,explode(",",$zone[zip_codes]));
			};
			echo 'var zones = '. json_encode($jsZones).';';
	?>
	jQuery(document).ready(function($){
		$('#zip_calc').on('submit',function(e){
			e.preventDefault();
			var deliveryZip = $('#zip_calc input').val();
			var deliveryTest = $.each(zones,function(i,v){
				if ( $.inArray(deliveryZip,zones[i]) > -1){
					console.log('zone'+i);
					$('input#zone_delivery_'+i).click();
					}
				});
		});





		//update this to $elem.prop("checked", true) when using 1.9+ - currently using 1.8.3
		//$j('input#zone_delivery_2').attr('checked', 'checked');
		//$j('input#zone_delivery_2').click();

	});
</script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script>
<?php
do_action('wpsc_bottom_of_shopping_cart');