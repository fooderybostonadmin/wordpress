<?php if(isset($cart_messages) && count($cart_messages) > 0) { ?>
	<?php foreach((array)$cart_messages as $cart_message) { ?>
	  <span class="cart_message"><?php echo esc_html( $cart_message ); ?></span>
	<?php } ?>
<?php } ?>

<?php if(wpsc_cart_item_count() > 0): ?>
    <div class="shoppingcart cart">

		<?php while(wpsc_have_cart_items()): wpsc_the_cart_item(); ?>
<div class="item">
	<div class="span4 image">
		<a href="<?php echo esc_url( wpsc_cart_item_url() ); ?>"><img src="<?php echo wpsc_cart_item_image(); ?>" alt="<?php echo wpsc_cart_item_name(); ?>" title="<?php echo wpsc_cart_item_name(); ?>" class="product_image img-polaroid" height="40" width="40"/></a>
	</div>

<div class="span8 info">
		<?php do_action ( "wpsc_before_cart_widget_item_name" ); ?><a href="<?php echo esc_url( wpsc_cart_item_url() ); ?>"><?php echo substr(wpsc_cart_item_name(),0,15).'...'; ?></a><?php do_action ( "wpsc_after_cart_widget_item_name" ); ?>

		<span class="badge"><?php echo wpsc_cart_item_quantity(); ?></span>

		                    <form action="" method="post" class="adjustform pull-left">
							<input type="hidden" name="quantity" value="0" />
							<input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
							<input type="hidden" name="wpsc_update_quantity" value="true" />
							<button class="remove_button" type="submit"><span class="badge badge-important"><i class="icon-remove icon-white"></i> remove</span></button>
						</form>
	</div>

	</div>		<?php endwhile; ?>

<div class="clearfix">

<div class="clearfix">
<div class="clearfix"><div class="pull-left"><?php printf( _n('%d item', '%d items', wpsc_cart_item_count(), 'wpsc'), wpsc_cart_item_count() ); ?></div>
	<div class="pull-right"><?php _e('Subtotal:', 'wpsc'); ?> <?php echo wpsc_cart_total_widget( false, false ,false ); ?></div>
	</div><div>
								<small><?php _e( 'excluding discount, delivery and tax', 'wpsc' ); ?></small>
							</div>
							</div>

<div class="clearfix cart-buttons">
							<a target="_parent" href="<?php echo esc_url( get_option( 'shopping_cart_url' ) ); ?>" <?php echo (wpsc_cart_item_count() < 4) ? 'onClick="return false"' : '';?> title="<?php esc_html_e('Checkout', 'wpsc'); ?>" class="gocheckout btn btn-success pull-left <?php echo (wpsc_cart_item_count() < 4) ? 'disabled' : '';?>"><?php esc_html_e('Checkout', 'wpsc'); ?></a>
							<form action="" method="post" class="wpsc_empty_the_cart pull-right">
								<input type="hidden" name="wpsc_ajax_action" value="empty_cart" />
									<a target="_parent" href="<?php echo esc_url( add_query_arg( 'wpsc_ajax_action', 'empty_cart', remove_query_arg( 'ajax' ) ) ); ?>" class="emptycart btn btn-danger" title="<?php esc_html_e('Empty Your Cart', 'wpsc'); ?>"><?php esc_html_e('Clear cart', 'wpsc'); ?></a>
							</form>
						</div>

</div>
<?php if(wpsc_cart_item_count() < 4): ?>
<div class="clearfix">
	You need at least 4 meals to checkout.
</div>
<?php endif; ?>
	</div><!--close shoppingcart-->
<?php else: ?>
	<p class="empty">
		<?php _e('Your shopping cart is empty', 'wpsc'); ?><br />
		<a target="_parent" href="<?php echo esc_url( get_option( 'product_list_url' ) ); ?>" class="visitshop" title="<?php esc_html_e('Visit Shop', 'wpsc'); ?>"><?php esc_html_e('Visit the shop', 'wpsc'); ?></a>
	</p>
<?php endif; ?>

<?php
wpsc_google_checkout();


?>
