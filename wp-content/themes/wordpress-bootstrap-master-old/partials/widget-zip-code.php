<div class="widget well">
	<h3><?php echo get_field('zone_widget_title','options');?></h3>
	<p><?php echo get_field('zone_widget_text','options');?></p>
	<form action="">
		<div class="input-append">
		  <input class="span8" type="text" id="zipWidgetInput" placeholder="Your Zip Code">
		  <a id="zipWidget" class="btn">Check!</a>
		</div>
		<div id="zipCheckResults"></div>
<!-- 		<input type="text" placeholder="Your Zip Code"/> -->
	</form>
</div>

<script type="text/javascript">
	//declare the arrays coming from the options panel
	<?php 	$widget_zones = get_field('zone', 'option');
			$out_of_range = get_field('out_of_range_message','option');
			$in_range = get_field('we_deliver_message','option');
			$js_widget_zones = array();
			foreach($widget_zones as $zone){
				array_push($js_widget_zones,explode(",",$zone[zip_codes]));
			};
			echo 'var widgetZones = '. json_encode($js_widget_zones).';';
			echo 'var widgetOutOfRange = "'. $out_of_range.'";';
			echo 'var widgetInRange = "'.$in_range .'";';

	?>
	jQuery(document).ready(function($){

		widgetOutOfRange = $('<p/>').html(widgetOutOfRange).text();
		widgetInRange = $('<p/>').html(widgetInRange).text();
		$('#zipWidget').on('click',function(e){
			e.preventDefault();
			var widgetDeliveryZip = $('#zipWidgetInput').val();
			$.each(widgetZones,function(i,v){
				if ( $.inArray(widgetDeliveryZip, widgetZones[i]) > -1 ){
					$('#zipCheckResults').text(widgetInRange);
					return false;
				} else {
					$('#zipCheckResults').text(widgetOutOfRange);
				}
			});
		});
		$("#zipWidgetInput").keypress(function(event) {
		    if (event.which == 13) {
		        event.preventDefault();
		        $('#zipWidget').click();
		    }
		});
		//update this to $elem.prop("checked", true) when using 1.9+ - currently using 1.8.3
		//$j('input#zone_delivery_2').attr('checked', 'checked');
		//$j('input#zone_delivery_2').click();
	});
</script>