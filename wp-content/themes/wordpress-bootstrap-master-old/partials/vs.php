<div class="row-fluid">
        <div class="container versus-table">
            <div class="span12">
                <h2 class="featurette-heading text-center"><?php echo get_field(title)?></h2>
				<div class="span1"></div>
	            <div class="span5">
	            	<table class="text-center">
	            	    <thead>
	            	        <tr>
	            	            <th>
	            	                <h1><?php echo get_field('left_column_title')?></h1>
	            	            </th>
	            	        </tr>
	            	    </thead>
	            							<?php
	            							$lRows = get_field('left_row');
	            							//print_r($lRows);
	            							if($lRows)
	            							{
	            							foreach($lRows as $row):
	            							echo '<tr><td>' . $row['row'] . '</td></tr>';
	            							endforeach;
	            							}
	            							?>
	            	    	            </table>
	            </div>
	            <div class="span5">
					<div class="vs-graphic"></div>
	            	<table class="text-center">
	            	    <thead>
	            	        <tr>
	            	            <th>
	            	                <h1><?php echo get_field('right_column_title')?></h1>
	            	            </th>
	            	        </tr>
	            	    </thead>
	            			               <?php
	            							$rRows = get_field('right_row');
	            							//print_r($lRows);
	            							if($rRows)
	            							{
	            							foreach($rRows as $row):
	            							echo '<tr><td>' . $row['row'] . '</td></tr>';
	            							endforeach;
	            							}
	            							?>
	            	       	            </table>
	            </div>
				<div class="span1"></div>
            </div>
        </div>
    </div>
