<?php
//This code requires that it be inside of the loop. It will check to see if the corresponding custom field(s) are available and will fail gracefully if they aren't there.
$rows = get_field('featurette');
if($rows)
{
	 echo  '<div class="row-fluid">
      <div class="container">';
	foreach($rows as $row):  ?>

     <div class="featurette">
        <img class="featurette-image <?php echo $row['image_style'] . ' ' . $row['image_position'];?>" src="<?php echo $row['image']['sizes']['featurette-large'];?>">
        <h2 class="featurette-heading"><?php echo $row['title'];?></h2>
        <p class="lead"><?php echo $row['content'];?></p>


        <?php
        if($row['button_title'] != ''){
	    $link = ($row['button_link_manual'] != '') ? $row['button_link_manual'] : $row['button_link'];
	    echo '<a href="'. $link .'" class="btn btn-success">'. $row['button_title'] .'</a>';

        }
        ?>
      </div>
	<?php endforeach;
    echo '</div></div>';
}
/*

The markup the featurette is based on is below


      <hr class="featurette-divider">

      <div class="featurette">
        <img class="featurette-image pull-right img-circle img-polaroid" src="http://placehold.it/400x400">
        <h2 class="featurette-heading">A farmers field <span class="muted">delivered to your door.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        <a href="site-page.php" class="btn">Learn more</a>
      </div>
*/
