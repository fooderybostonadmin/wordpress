<?php
/*
Template Name: Text Slider
*/
?>
<?php get_header(); ?>
			<div class="row-fluid">
				<div class="container">
					<div id="content" class="clearfix row-fluid">

						<div id="main" class="span9 clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header>

									<div class="page-header"><h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1></div>

								</header> <!-- end article header -->

								<section class="post_content clearfix" itemprop="articleBody">
									<?php
											the_content();
											$rows = get_field('block');
											$tab = 0;
											$nav = 0;
											if($rows)
											{
												echo '<ul class="columns two-columns" id="valueTab">';

												foreach($rows as $row)
												{
													echo '<li><a href="#tab'.$nav.'" data-toggle="tab">' . $row['content_title'] . '</a></li>';
													$nav++;
												}

												echo '</ul>';

												echo '<div class="tab-columns tab-content">';

												foreach($rows as $row)
												{
													echo '<div class="tab-pane" id="tab'.$tab.'">';
													echo '<h4>'.$row['content_title'] . '</h4>';
													echo $row['content'];
													echo '</div>';
													$tab++;
												}
												echo '</div>';
											}


									?>

								</section> <!-- end article section -->

							</article> <!-- end article -->

							<?php endwhile; ?>

							<?php else : ?>

							<article id="post-not-found">
							    <header>
							    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
							    </header>
							    <section class="post_content">
							    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
							    </section>
							    <footer>
							    </footer>
							</article>
							<?php endif;?>
						</div> <!-- end #main -->

						<?php get_sidebar(); // sidebar 1 ?>

					</div> <!-- end #content -->
				</div>
			</div>
			<script type="text/javascript">
				jQuery(document).ready(function($){
					$(function () {
						$('#valueTab a:first').tab('show');
					});
				});
			</script>
<?php get_footer(); ?>