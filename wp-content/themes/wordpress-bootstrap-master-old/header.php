<!doctype html>

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title( '|', true, 'right' ); ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- theme options from options panel -->
		<?php //get_wpbs_theme_options(); ?>

		<!-- typeahead plugin - if top nav search bar enabled -->
		<?php require_once('library/typeahead.php'); ?>

		<link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>


	</head>

	<body <?php body_class(); ?>>

	<div id="cta-banner">
		<?php if( get_field('cta_banner_link', 'options') ): ?>
			<a href="<?php the_field('cta_banner_link', 'options') ;?>">
		<?php endif; ?>
		<?php the_field('cta_banner', 'options') ;?>
		<?php if( get_field('cta_banner_link', 'options') ): ?>
			</a>
		<?php endif; ?>
	</div>

		<header role="banner" class="page-top-header">
			<div class="container masthead clearfix">
				<div class="navbar">
				    <div class="container">
						<div class="button-container pull-right">
				          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>

						</div>
					  <a class="brand"  id="logo" title="<?php echo get_bloginfo('description'); ?>" href="<?php echo home_url(); ?>">
						<img src="<?php echo bloginfo('template_directory');?>/library/images/foodery_logo.png">

					  </a>
				      <div class="nav-collapse collapse">
							<?php bones_main_nav(); // Adjust using Menus in Wordpress Admin ?>

				      </div><!--/.nav-collapse -->
				    </div>
				</div>
			</div>
		</header>
