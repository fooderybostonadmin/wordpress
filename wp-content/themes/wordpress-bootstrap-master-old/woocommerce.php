<?php
/**
 * WooCommerce File Support
 */
?>

<?php get_header(); ?>
			<div class="row-fluid">
				<div class="container">
					<div id="content" class="clearfix row-fluid site-content"role="main">

						<div id="main" class="span9 clearfix" role="main">

							<?php

								$order_offline = get_field('order_offline','option');
								$at_capacity = get_field('at_capacity','option');
								
								if(is_shop()): ?>

								<div class="hero-unit">
						            <h1>This Week's Menu</h1>
						            <?php if ($order_offline === true){
											echo 'Ordering Offline';
										} elseif ($at_capacity === true){
											echo 'At Capacity';
										} else { ?>
								            <p><?php the_field('the_weeks_menu_message','option');?></p>
											<?php
$order_by_date = DateTime::createFromFormat('Ymd', get_field('order_by_date','option'));
$delivery_date = DateTime::createFromFormat('Ymd', get_field('delivery_date','option'));
?>								            <p class="text-success">Order by <strong>  <?php if (the_field('order_by_time','option') != '') echo  get_field('');?></strong>  for delivery on <strong><?php echo $delivery_date->format('l, F j'); ?></strong>.</p>
  						            <?php };?>
  								</div>
  							<?php endif;?>
  								<section class="post_content clearfix" itemprop="articleBody">
  									<?php

										if ( $order_offline === true){
											echo 'Ordering Offline';
										} elseif ($at_capacity === true){

											echo 'At Capacity';
										} else {
											woocommerce_content();
										}
								?>

								</section> <!-- end article section -->

								<footer>

									<?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","bonestheme") . ':</span> ', ', ', '</p>'); ?>

								</footer> <!-- end article footer -->

							</article> <!-- end article -->

								<div class="row-fluid brownbg">
							        <div class="container">
							            <div class="span9">
							                <h1 class="text-center">Get Our Weekly Menu In Your Inbox</h1>
							                <br/>
							                <div class="text-center newsletter-signup">
											<?php //echo do_shortcode( '[gravityform id="3" name="Newsletter Signup" title="false" description="false" ajax="true"]' ); ?>
											<?php echo do_shortcode( '[contact-form-7 id="55" class:form-inline title="Newsletter"]' ); ?>
							                </div>
							            </div>
							        </div>
							    </div>
							</div> <!-- end #main -->

							<?php get_sidebar(); // sidebar 1 ?>

							</div> <!-- end #content -->


				</div>
			</div>
<?php get_footer(); ?>
