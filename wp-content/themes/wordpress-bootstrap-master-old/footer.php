<footer class="footer">
	<div class="row-fluid footer-top">
		<div class="container">
			<div class="pull-left"><img src="<?php echo bloginfo('template_directory');?>/library/images/foodery_logo_white_30h.png" /></div>
			<div class="pull-right">
				<a class="btn btn-success pull-right" href="">Checkout</a>
				<a class="btn btn-inverse pull-right " href="">Log In</a>
			</div>
		</div>
	</div> <!-- end of first footer row -->
	<div class="row-fluid">
		<div class="container"><?php bones_footer_links();?></div>
	</div>
	<div class="row-fluid">
		<div class="container">
			<div class="span2">info@fooderyboston.com<br/><a href="tel:6172074080">617-207-4080</a></div>
	        <div class="span2 pull-right">
	  <div class="social-buttons">
		  <a href="https://www.facebook.com/theFoodery"><img src="<?php echo bloginfo('template_directory');?>/library/images/facebook.png" /></a>
		  <a href="https://twitter.com/theFoodery"><img src="<?php echo bloginfo('template_directory');?>/library/images/twitter.png" /></a>
		  <a href="https://pinterest.com/thefoodery/"><img src="<?php echo bloginfo('template_directory');?>/library/images/pinterest.png" /></a>
	  </div>
	</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="container">
			<a href="#" class="pull-right">Back to top</a>
		</div>
	</div>
	<div class="row-fluid">
		<div class="container">
			<div class="pull-left">&copy; 2013-<?php echo date("Y") ?> The Foodery &middot; <a href="<?php echo site_url('terms-and-conditions');?>">Terms and Conditions</a></div>
		</div>
	</div>
</footer>


		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>
