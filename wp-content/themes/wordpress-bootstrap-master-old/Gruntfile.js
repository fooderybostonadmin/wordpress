module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        compass: {
            dist: {
                options: {
                    sassDir: 'library/scss',
                    cssDir: 'library/css',
                    outputStyle: 'expanded'
                }
            }
        },
        watch: {
            compass: {
                files: ['library/scss//**/*.scss'],
                tasks: ['compass:dist']
            },
            copy: {
                files: ['library/scss/**/*.css'],
                tasks: ['copy']
            }
        },
        copy: {
            dist: {
                files: [
                    {expand: true, cwd: 'styles', src: ['**/*.css'], dest: 'assets/css'}
                ]
            }
        }
    })

    grunt.loadNpmTasks('grunt-contrib-compass')
    grunt.loadNpmTasks('grunt-contrib-jshint')
    grunt.loadNpmTasks('grunt-contrib-watch')
    grunt.loadNpmTasks('grunt-contrib-copy')

    grunt.registerTask('default', ['compass:dist', 'copy:dist'])
    grunt.registerTask('release', ['compass:dist', 'copy:dist'])
}
