<?php
/*
Template Name: Homepage V2
*/
?>

<?php get_header(); ?>

					<article id="homepage-v2">

					    <section class="post_content">

								<?php get_template_part( 'partials/homepage-v2', 'hero-slider-soliloquy' ); ?>
								<?php //get_template_part( 'partials/homepage-v2', 'hero-slider' ); ?>
								<?php get_template_part( 'partials/homepage-v2', 'steps' ); ?>
								<?php get_template_part( 'partials/homepage-v2', 'featured-meals' ); ?>
								<?php get_template_part( 'partials/homepage-v2', 'ingredients' ); ?>

								<?php get_template_part( 'partials/social-media', 'tiles' ); ?>
								<?php get_template_part( 'partials/homepage-v2', 'quotes-soliloquy' ); ?>
								<?php get_template_part( 'partials/homepage-v2', 'quotes' ); ?>
								<?php //Moved to footer
                                                                //get_template_part( 'partials/homepage-v2', 'newsletter' ); ?>
					    </section>

					</article>


<?php get_footer(); ?>
