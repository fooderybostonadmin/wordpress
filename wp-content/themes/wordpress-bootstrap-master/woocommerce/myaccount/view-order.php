<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page
 *
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version   2.2.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

//var_dump(date("m-d-y", $order->date_created->getTimestamp()));
//echo date("m-d-y", strtotime('next Sunday', $order->date_created->getTimestamp()));
$dateText = "";


$end_date = getLCDay($order->date_created->getTimestamp());
$time = strtotime(date("m/d/Y g:ia"), current_time('timestamp'));
$dateText = date('l, n/j/Y', $end_date) . ' at ' . date('g:ia', $end_date);

$orderStatus = $order->status;
?>
<style>
    .page-header h1{
        opacity:0;
    }
    .page-header .header_changed{
        opacity:1;
    }

</style>
<?php wc_print_notices(); ?>
<p class="order-info"><?php printf(__('Order #<mark class="order-number">%s</mark> was placed on <span class="order-date_">%s</span> and is currently <span class="order-status_">%s</span>.', 'woocommerce'), $order->get_order_number(), date_i18n(get_option('date_format'), strtotime($order->order_date)), wc_get_order_status_name($order->get_status())); ?></p>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js"></script>
<?php foodery_display_order_delivery_date($order->get_id()); ?>
<div class="cancel-order-notification"> 
You will receive an email with a more specific delivery window once your order is being prepared.</br>
You will receive a text notification shortly before your delivery arrives.
</div>
<?php if (($time < $end_date || $time == $end_date) && $orderStatus != 'cancelled'): ?>
    <div class="cancel-order-notification">
        You can <a data-fancybox data-src="#order-info" href="javascript:;"><strong> modify or cancel</strong></a> this
        order
        before <?php echo $dateText; ?>
    </div>
<?php else: ?>
<div class="cancel-order-notification">
        This order can no longer be canceled, please contact <a href="mailto:info@fooderyboston.com">info@fooderyboston.com</a> for any delivery modifications.
    </div>
<?php endif; ?>

<div style="display: none;" id="order-info">
    <h3>
        To make any order modifications, <br> cancel the order and place a new order.
    </h3>
    <br>
    <a href="#" id="cancel-order" class="btn">Cancel Order</a>
    <div class="spinner" id="cancel-order-spinner" style="display:none">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<?php if ($notes = $order->get_customer_order_notes()) :
    ?>
    <h2><?php _e('Order Updates', 'woocommerce'); ?></h2>
    <ol class="commentlist notes">
        <?php foreach ($notes as $note) : ?>
            <li class="comment note">
                <div class="comment_container">
                    <div class="comment-text">
                        <p class="meta"><?php echo date_i18n(__('l jS \o\f F Y, h:ia', 'woocommerce'), strtotime($note->comment_date)); ?></p>
                        <div class="description">
                            <?php echo wpautop(wptexturize($note->comment_content)); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </li>
        <?php endforeach; ?>
    </ol>
    <?php
endif;
?>
<script>

    jQuery('.page-header h1').text('Order Summary:').addClass('header_changed');

    jQuery("#cancel-order").on('click', function () {

        jQuery.ajax({
            type: 'POST',
            url: window.wp_data.ajax_url,
            data: {
                action: "loadPosts",
                order_id: <?php echo $order_id; ?>,
            },
            beforeSend: function () {
                jQuery("#cancel-order").remove();
                jQuery("#cancel-order-spinner").show();
            },
            success: function (html) {
                jQuery("#cancel-order-spinner").hide();
                if (html == "true") {
                    jQuery("#order-info h3").text("Your order has been cancelled and a full refund was processed to your payment method.");
                    setTimeout(function() { location.reload(); }, 5000);
                } else {
                    jQuery("#order-info h3").text("Error");
                }
            }
        });
        return false;

    });
</script>

<?php
do_action('woocommerce_view_order', $order_id);
