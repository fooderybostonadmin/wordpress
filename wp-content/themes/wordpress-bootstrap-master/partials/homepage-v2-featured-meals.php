<?php
/*
	Partial: featured meals
*/

//Determine current menu to display
$current_menu = foodery_get_current_menus(1); //Comes back in the format $current_menu[delivery_date] = term_id;
$current_menu_term_id = '';
foreach ($current_menu as $id => $value ) :
	$current_menu_term_id = $value;
endforeach;

if( have_rows('featured_meals') ): 
    $bkgdimg = get_field('featured_meals_background'); 
	$order_now_button = foodery_order_now_button(); ?>

	<div class="row-fluid featured-meals" style="background-image:url(<?php echo $bkgdimg; ?>);">
            <div class="container">
                <div class="row">

                    <div class="featured-meals-title-block">
                        <a href="<?php echo get_field('featured_meals_link'); ?>">
                        <?php
                        $titleimgid = get_field( 'featured_meals_header_image');
                        $titletext =  get_field( 'featured_meals_title');
                        echo wp_get_attachment_image ($titleimgid, 'full', false, array( 'alt' => $titletext, 'class' => 'title-img' ));
                        ?>
                        </a>
                    </div>

                    <div class="span12">
            <?php while( have_rows('featured_meals') ): the_row(); 

                    if( $post_obj = get_sub_field('meal')): 
                        setup_postdata( $post_obj); 
                        $id = $post_obj->ID; 
                        $link = get_post_permalink( $id );
						$valid_delivery_date = false;

						//Check for valid term ID
						if ( has_term( $current_menu_term_id, 'product_cat', $post_obj ) ) {
							$valid_delivery_date = true;
						}

                        //Only show published items (unless an admin is logged in)
                        if ( ($valid_delivery_date && $post_obj->post_status == 'publish') || current_user_can('manage_options') ) :
							//Get desired orientation
                            $o = get_sub_field('orientation'); ?>

                            <div class="row">

                            <?php if ( $o == 'image-text'): ?>
                                <div class="span6  img-container-left">
                                    <a href="<?php echo $link; ?>">
                                    <?php echo get_the_post_thumbnail($id, 'home-meals', array( 'alt' => '', 'class' => 'left') ); ?>
                                    </a>
                                </div>
                            <?php endif; ?>


                                <div class="span6 featured-meal text-center">

                                    <?php 
                                    //Get meal related values from Product itself
                                    $title1 = get_post_meta($id, 'short_title_1', true);
                                    $title2 = get_post_meta($id, 'short_title_2', true);
                                    $content = get_post_meta($id, 'product_desc', true);
                                    $price = get_post_meta( $id, '_regular_price', true);
                                    $servings = get_post_meta($id, 'num_servings', true);
                                    
                                    //If custom title not given, show full meal title in smaller cursive font 
                                    if ($title1 == '' && $title2 == '') 
                                        $title1 = ucwords($post_obj->post_title);
                                    
                                    //Default to regular product description if a short one isn't given
                                    if ($content == '')
                                        $content = apply_filters('the_content', get_the_content());
                                                                        
                                    if ( $title1 != '') : ?>
                                        <div class="meal-title-small">
                                            <a href="<?php echo $link; ?>"><?php echo $title1; ?></a>
                                        </div>
                                    <?php endif;
                                    if ( $title2 != '') : ?>
                                        <div class="meal-title-large">
                                            <a href="<?php echo $link; ?>"><?php echo $title2; ?></a>
                                        </div>
                                    <?php endif; ?>                                    
                                    
                                    <div class="meal-desc"><?php echo $content; ?></div>

                                    <?php echo $order_now_button; ?>

                                    <div class="price">
                                        <?php 
                                        //Show Price
                                        if ($price != '') 
                                            echo '$' . number_format($price, 0); 
                                        
                                        //Only # of servings if available
                                        if ( $servings != '')  : 
                                            echo '&nbsp;&nbsp;|&nbsp;&nbsp;' . $servings . ' servings included';
                                        endif; ?>
                                    </div>
                                    
                                </div>

                            <?php if ( $o == 'text-image'): ?>
                                <div class="span6 img-container-right">
                                    <a href="<?php echo $link; ?>">
                                    <?php echo get_the_post_thumbnail($id, 'home-meals', array( 'alt' => '', 'class' => 'right') ); ?>
                                    </a>
                                </div>
                            <?php endif; ?>

                            </div><!-- .row -->

                    <?php 
                    endif; // $post_obj->post_status = 'publish' || current_user_can('manage_options'))
                endif;  //if( $post_obj = get_sub_field('meal'))

            endwhile; 
        
            wp_reset_postdata();  ?>

                    </div><!-- .span12 -->

                    <a class="green-button larger-text" href="<?php echo get_field('featured_meals_link'); ?>">
                        <?php echo get_field('featured_meals_link_title'); ?>
                    </a>

                </div><!-- .row -->
            </div><!-- .container -->
	</div><!-- .row-fluid featured-meals -->
<?php endif; ?>
