<?php
/*
	Partial: steps
*/
?>
<?php if( have_rows('steps') ): ?>
	<div class="row-fluid steps">
		<div class="container">
			<div class="row">

	    	<?php while( have_rows('steps') ): the_row(); ?>
          <div class="span4 step" style="">
						<img class="icon" src="<?php echo get_sub_field('image'); ?>" />
						<h3><?php echo get_sub_field('title'); ?></h3>
						<p><?php echo get_sub_field('content'); ?></p>
						<?php if( $l = get_sub_field('link')): ?>
							<a class="link" href="<?php echo $l; ?>">
					  <?php endif; ?>
								<?php echo get_sub_field( 'link_title'); ?>
						<?php if( $l): ?>
							</a>
					  <?php endif; ?>
					</div>

    		<?php endwhile; ?>

			</div>
		</div>
	</div>
<?php endif; ?>
