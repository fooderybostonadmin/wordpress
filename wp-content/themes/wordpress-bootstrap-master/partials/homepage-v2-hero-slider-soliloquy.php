<?php
/*
	Partial: hero slider soliloquy
*/
?>
	<div class="row-fluid hero-slider-soliloquy">
		<div class="container">
			<div class="row">
				<?php if ( $n = get_field('hero_slider_soliloquy')): ?>
					<?php echo do_shortcode($n); ?>
					<?php echo do_shortcode('[green-button text="View This Week\'s Menu" link="menu" ]'); ?>
					<div class="banner">
						<div class="banner-background"></div>
						<div class="banner-foreground">Eat Well<span class="ampersand"> &amp; </span>Save Time</div>
					</div>
				<?php endif; ?>

			</div>
		</div>
	</div>
