<?php
/*
	Partial: hero slider
*/
?>
<?php if( have_rows('hero_slider') ): ?>
	<div class="row-fluid hero-slider">
		<div class="container">
	    	<?php while( have_rows('hero_slider') ): the_row(); ?>
					<?php $b = get_sub_field('image'); ?>
					<div class="row"  style="background-image:url(<?php echo $b; ?>);">

          <div class="span12 arrow" style="background-color:transparent;width:100%;min-height:546px;">

						<?php if( $l = get_sub_field('button_link')): ?>
							<a class="link" href="<?php echo $l; ?>">
					  <?php endif; ?>
								<?php echo get_sub_field( 'button_title'); ?>
						<?php if( $l): ?>
							</a>
					  <?php endif; ?>

						<a class="left"><img src="<?php echo bloginfo('template_directory');?>/library/images/hero-slider-arrow-left.png" /></a>
						<a class="right"><img src="<?php echo bloginfo('template_directory');?>/library/images/hero-slider-arrow-right.png" /></a>

						<br>
						<br>
						<div style="float:clear;"></div>
					</div>


			</div>
		<?php endwhile; ?>
		</div>
	</div>
<?php endif; ?>
