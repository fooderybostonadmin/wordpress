<?php get_header(); 

$paged = (get_query_var('paged'));
        
foodery_display_blog_header(); ?>
		
    <div id="content" class="clearfix row-fluid blog-page">
            
        <div id="main" class="span12 clearfix" role="main">
                <?php 
                $count = 0;
                if (have_posts()) : while (have_posts()) : the_post(); $count++; 
                
                    $excerpt = get_the_excerpt();
                    if ( $count == 1 && $paged < 2 ) : ?>
                        <div class="container">
                            <article <?php post_class('blog-spotlight clearfix'); ?>>
                                <div class="span8">
                                    <?php the_post_thumbnail( 'page-featured-image', array('class' => '') ); ?> 
                                </div>
                                <div class="span4">
                                    <h1 class="blog-title"><a href="<?php  echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
                                    <div class="blog-meta"><?php _e("By", "foodery"); ?> <?php the_author_posts_link(); ?></div>
                                    <div class="blog-excerpt"><?php echo $excerpt; ?></div>					                            
                                </div>
                            </article>
                        </div> <!-- end container -->     
                    <?php endif; 
                    
                    if ( $count == 1 ) : 
                        foodery_display_blog_search();
                    endif; 
                
                    //For the 2nd blog post on the 1st page, or the 1st post of subsequent page
                    //Start the blog post containter 
                    if ( ($count == 2 && $paged < 2) || ($count == 1 && $paged >= 2 ) ) : ?>
                
                    <div class="blog-posts-list">
                        <div class="container">
                    <?php endif; 
                
                    //Display blog post normally
                    if ( $count > 1 || $paged >= 2 ) : 
                        $hasphoto = has_post_thumbnail(); ?>
                        <article <?php post_class('blog-normal span10 offset1 clearfix'); ?>>
                            <div class="row-fluid">
                                <?php if ($hasphoto) : ?>
                                <div class="span5">
                                    <?php the_post_thumbnail( 'medium', array('class' => '') ); ?> 
                                </div>
                                <?php endif; ?>
                                <div class="<?php echo ($hasphoto ? 'span7' : 'span12'); ?>">
                                    <h1 class="blog-title"><a href="<?php  echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
                                    <div class="blog-meta"><?php _e("By", "foodery"); ?> <?php the_author_posts_link(); ?></div>
                                    <div class="blog-excerpt"><?php echo $excerpt; ?></div>					                            
                                </div>
                            </div>
                        </article>                
                    <?php endif; ?>
                
                    <?php endwhile; ?>

                    <div class="blog-navigation span10 offset1">
                    <?php if (function_exists('page_navi')) { // if expirimental feature is active 
                           page_navi(); // use the page navi function 
                    } else { // if it is disabled, display regular wp prev & next links ?>
                            <nav class="wp-prev-next">
                                    <ul class="clearfix">
                                            <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
                                            <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
                                    </ul>
                            </nav>
                    <?php } ?>
                    </div>
                            
                    
                   <?php if ( $count > 1 || $paged >= 2 ) : ?>
                        </div> <!-- end container --> 
                    </div><!-- .blog-posts-list -->
                    <?php endif; 
                    
                else : ?>

                <article id="post-not-found">
                    <header>
                        <h1><?php _e("Not Found", "bonestheme"); ?></h1>
                    </header>
                    <section class="post_content">
                        <p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
                    </section>
                    <footer>
                    </footer>
                </article>

                <?php endif; ?>


            </div> <!-- end container -->
        </div> <!-- end #main -->
    </div> <!-- end #content -->

<?php get_footer(); ?>