/* imgsizer (flexible images for fluid sites) */
var imgSizer = {
    Config: {imgCache: [], spacer: "/path/to/your/spacer.gif"}, collate: function (aScope) {
        var isOldIE = (document.all && !window.opera && !window.XDomainRequest) ? 1 : 0;
        if (isOldIE && document.getElementsByTagName) {
            var c = imgSizer;
            var imgCache = c.Config.imgCache;
            var images = (aScope && aScope.length) ? aScope : document.getElementsByTagName("img");
            for (var i = 0; i < images.length; i++) {
                images[i].origWidth = images[i].offsetWidth;
                images[i].origHeight = images[i].offsetHeight;
                imgCache.push(images[i]);
                c.ieAlpha(images[i]);
                images[i].style.width = "100%";
            }
            if (imgCache.length) {
                c.resize(function () {
                    for (var i = 0; i < imgCache.length; i++) {
                        var ratio = (imgCache[i].offsetWidth / imgCache[i].origWidth);
                        imgCache[i].style.height = (imgCache[i].origHeight * ratio) + "px";
                    }
                });
            }
        }
    }, ieAlpha: function (img) {
        var c = imgSizer;
        if (img.oldSrc) {
            img.src = img.oldSrc;
        }
        var src = img.src;
        img.style.width = img.offsetWidth + "px";
        img.style.height = img.offsetHeight + "px";
        img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "', sizingMethod='scale')"
        img.oldSrc = src;
        img.src = c.Config.spacer;
    }, resize: function (func) {
        var oldonresize = window.onresize;
        if (typeof window.onresize != 'function') {
            window.onresize = func;
        } else {
            window.onresize = function () {
                if (oldonresize) {
                    oldonresize();
                }
                func();
            }
        }
    }
}
var emojichars = [
    '\ud83c[\udf00-\udfff]', // U+1F300 to U+1F3FF
    '\ud83d[\udc00-\uddff]', // U+1F400 to U+1F64F
    '\ud83d[\ude00-\udeff]'  // U+1F680 to U+1F6FF
];

function removeInvalidChars(el) {
    var str = jQuery(el).val();
    str = str.replace(new RegExp(emojichars.join('|'), 'g'), '');
    jQuery(el).val(str);
}

// add twitter bootstrap classes and color based on how many times tag is used
function addTwitterBSClass(thisObj) {
    var title = $(thisObj).attr('title');
    if (title) {
        var titles = title.split(' ');
        if (titles[0]) {
            var num = parseInt(titles[0]);
            if (num > 0)
                $(thisObj).addClass('label');
            if (num == 2)
                $(thisObj).addClass('label label-info');
            if (num > 2 && num < 4)
                $(thisObj).addClass('label label-success');
            if (num >= 5 && num < 10)
                $(thisObj).addClass('label label-warning');
            if (num >= 10)
                $(thisObj).addClass('label label-important');
        }
    }
    else
        $(thisObj).addClass('label');
    return true;
}

function positionSliderButton() {
    var jsoSliderBanner = document.getElementsByClassName("banner")[0];
    var yPos = jsoSliderBanner.offsetTop + jsoSliderBanner.offsetHeight + 10;
    var jsoSliderButton = document.getElementsByClassName("green-button")[0];
    jsoSliderButton.style.top = yPos + 'px';
    jsoSliderButton.style.bottom = 'inherit';
}

// as the page loads, call these scripts
jQuery(document).ready(function ($) {


    $(".select_date_wrap .span9 ").hover(function () {
        $(this).toggleClass("result_hover");
    });


    if (screen.width < 480) {
        $('#text-6').clone().insertAfter('.woocommerce-ordering');
        // $('#sidebar1 .widget:first-child').clone().insertAfter('.woocommerce-ordering');
    }
    //page specific js
    if ($('body').hasClass('wpsc')) {
        $('.product_grid_display').each(function () {
            var $oEl = $(this),
                sHtml = '<div class="row-fluid"></div>';
            if (screen.width <= 480) {
                var perSlide = 1;
            } else {
                var perSlide = $oEl.attr('data-per-slide') || 2;
            }
            while ($oEl.children(".product_grid_item").length > 0) {
                $oEl.children(".product_grid_item").slice(0, perSlide).wrapAll(sHtml);
            }
        });
    }
    //Add header to Ship address in checkout
    //(Otherwise we would need to customize the woocommerce/checkout/form-shipping.php)
    $('body.woocommerce-checkout #ship-to-different-address').prepend('Delivery Address</br >');
    if ($('body').hasClass('woocommerce') && $('ul.products').length) {
        //WooCommerce purchase page add-to-cart with quantity and AJAX, by customising the add-to-cart template in the WooCommerce loop.
        //See blog post for details: http://snippets.webaware.com.au/snippets/woocommerce-add-to-cart-with-quantity-and-ajax/
        //https://gist.github.com/webaware/6260326
        /* when product quantity changes, update quantity attribute on add-to-cart button */
        $("form.cart").on("change", "input.qty", function () {
            //alert('change');
            if (this.value === "0")
                this.value = "1";
            $(this.form).find("button[data-quantity]").attr("data-quantity", this.value);
        });
        /* remove old "view cart" text, only need latest one thanks! */
        $(document.body).on("adding_to_cart", function () {
            $("a.added_to_cart").remove();
        });
    }
    if ($('body').hasClass('woocommerce-checkout')) {
        $('.woocommerce-billing-fields .address-field input[type=text], .woocommerce-shipping-fields .address-field input[type=text]').attr("autocomplete", "off");
    }
    $("ol.commentlist a.comment-reply-link").each(function () {
        $(this).addClass('btn btn-success btn-mini');
        return true;
    });
    $('#cancel-comment-reply-link').each(function () {
        $(this).addClass('btn btn-danger btn-mini');
        return true;
    });
    // Input placeholder text fix for IE
    $('[placeholder]').focus(function () {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
        }
    }).blur(function () {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
        }
    }).blur();
    // Prevent submission of empty form
    $('[placeholder]').parents('form').submit(function () {
        $(this).find('[placeholder]').each(function () {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
            }
        })
    });
    $("form.woocommerce-checkout input, form.woocommerce-checkout textarea").on('input', function () {
        //Replace any emojis found
        removeInvalidChars(this);
    });
    $('#s').focus(function () {
        if ($(window).width() < 940) {
            $(this).animate({width: '200px'});
        }
    });
    $('#s').blur(function () {
        if ($(window).width() < 940) {
            $(this).animate({width: '100px'});
        }
    });
    $('.alert-message').alert();
    $('.dropdown-toggle').dropdown();
    //Warn before clearing cart for wrong delivery date selection
    $('.wrong-delivery-date').click(function () {
        //Ok button pressed...
        if (confirm("Are you sure?\n\nChanging your delivery date will clear all items from your cart.\nIf you want to order for both delivery dates, please complete your order first before starting another.")) {
            window.location.href = $(this).attr('data-clear-cart-url');
        }
    });
    //Make sidebar mini cart buttons the light green that the other WooCommerce buttons are
    //Not working for some reason - commenting out for now (AGiles 6/4/2018)
    //$('.woocommerce-mini-cart__buttons a.button').addClass('green-button');
    //Deal with things that depend on the size of the browser, etc here
    //Note image related items happen later in resizedImages() which is triggered on load
    resized();
});
/* end of as page load scripts */
//Deal with things that depend on the size of the browser, etc here
//these adjustments require images to be loaded
jQuery(window).load(function () {
    resizedImages();
    if (jQuery('body').hasClass('home')) {
        if (document.getElementsByClassName("banner")[0]) {
            positionSliderButton();
            window.addEventListener('resize', function () {
                positionSliderButton();
            }, true);
        }
    }
});

function resizedImages() {
    var viewportWidth = jQuery(window).width() + getScrollBarWidth();
    var noDeliveryDate = jQuery('.no-delivery-date').length;
    var isGiftPage = jQuery('.page-template-page-gifts').length;
    //Size up product blocks on menu page to all be same height as their peers
    jQuery('.post-type-archive-product ul.products, .page-template-page-gifts ul.products').each(function () {
        //Reset product blocks to their normal height
        jQuery(this).find('li.product').css('min-height', '0');
        //Only resize on wider screens
        if (viewportWidth > 501) {
            var maxHeight = 0;
            var contentHeight = 0;
            jQuery(this).find('li.product').each(function () {
                //Grab height of product blocks (title, photo, and excerpt)
                contentHeight = jQuery(this).innerHeight();
                //If this is taller than the current max height, then capture it
                if (contentHeight > maxHeight)
                    maxHeight = contentHeight;
            });
            //Use max-height to size all blocks evenly to tallest height
            if (maxHeight > 0) {
                //Adjust height when Add to Cart is missing or when on Gift page
                if (noDeliveryDate > 0) {
                    maxHeight = maxHeight - 10;
                } else if (isGiftPage > 0) {
                    maxHeight = maxHeight - 50;
                }
                //Define set height for each block sop they line up
                jQuery(this).find('li.product').css('min-height', maxHeight + 'px');
            } else
                jQuery(this).find('li.product').css('min-height', '0');
        }
    });
}

function resized() {
    //var viewportWidth = jQuery(window).width() + getScrollBarWidth();
    //page specific js
    var hdrHeight = jQuery('.container.masthead').outerHeight();
    if (hdrHeight > 0) {
        if (jQuery('body').hasClass('home')) {
            jQuery('.home .hero-slider-soliloquy .row').css('padding-top', Math.ceil(hdrHeight) + 'px');
        } else {
            jQuery('header.homepage-v2-header + div').css('padding-top', Math.ceil(hdrHeight) + 'px');
        }
    }
}

//Resize relevant items on window resize
jQuery(window).resize(resized);
jQuery(window).resize(resizedImages);

//Calculate the width of the scroll bar so css media queries and js widow.width match
function getScrollBarWidth() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";
    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);
    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;
    document.body.removeChild(outer);
    return (w1 - w2);
};