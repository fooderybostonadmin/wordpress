<?php
/*
 * Functions related to Gravity Forms customization
 */
define('FOODERY_REGISTER_FORM', 4);
define('FOODERY_REFER_FRIEND_FORM_CUSTOMER', 6);

/**
 * Updating user meta after user registration successful 
 */
add_action( 'gform_user_registered', 'foodery_gforms_prepopulating_first_last_names', 10, 4 );
function foodery_gforms_prepopulating_first_last_names( $user_id, $feed, $entry, $user_pass ) {    
    $firstname = rgar( $entry, '1.3' );
    $lastname = rgar( $entry, '1.6' );
    
    update_user_meta($user_id, 'billing_first_name', $firstname);
    update_user_meta($user_id, 'shipping_first_name', $firstname);
    update_user_meta($user_id, 'billing_last_name', $lastname);
    update_user_meta($user_id, 'shipping_last_name', $lastname);    
}

/*
 * For Referral Form for Customers (logged in users), default in first & last name
 */
add_filter( 'gform_pre_render_' . FOODERY_REFER_FRIEND_FORM_CUSTOMER, 'foodery_gform_populate_customer_name' );
function foodery_gform_populate_customer_name( $form ) {

    global $current_user; 

    foreach ( $form['fields'] as &$field ) {
        if ( $field->id == 3 ) 
            $field->defaultValue = $current_user->user_firstname;
        
        if ( $field->id == 4 ) 
            $field->defaultValue = $current_user->user_lastname;
    }

    return $form;
}

/**
 * Fix Gravity Form Tabindex Conflicts
 * https://gravitywiz.com/fix-gravity-form-tabindex-conflicts/
 */
add_filter( 'gform_tabindex', 'gform_tabindexer', 10, 2 );
function gform_tabindexer( $tab_index, $form = false ) {
    $starting_index = 1000; // if you need a higher tabindex, update this number
    if( $form )
        add_filter( 'gform_tabindex_' . $form['id'], 'gform_tabindexer' );
    return GFCommon::$tab_index >= $starting_index ? GFCommon::$tab_index : $starting_index;
}

/*
  * Add Google AdWords tracking for new accounts created
 */
//Removed per John Bauer's email of 11/5/16
//add_filter( 'gform_confirmation_' . FOODERY_REGISTER_FORM, 'foodery_register_confirmation_add_google_tracking', 10, 4 );
function foodery_register_confirmation_add_google_tracking( $confirmation, $form, $entry, $ajax ) {

    $confirmation .= '
        <!-- Google Code for Customer Account Setups Conversion Page -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 991030295;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "EgqcCJnB_GoQl9jH2AM";
        var google_remarketing_only = false;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/991030295/?label=EgqcCJnB_GoQl9jH2AM&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript> 
    ';
    
    return $confirmation;
}


//This code no longer needed since User Activation via email is not being used - 9/7/2016
//add_filter( 'gform_notification_' . FOODERY_REGISTER_FORM, 'foodery_registration_notification', 10, 3 );
//function foodery_registration_notification( $notification, $form, $entry ) {
//    if ( $notification['name'] == 'User Notification' ) {
//        //Grab activation URL from notification email set up in Gravity Forms
//        $activationurl = trim(strip_tags($notification['message']));
//        
//        if ( !WC()->cart->is_empty() )
//            $activationurl .= '&cart=true';
//        
//        //Using activation URL, set up custom email.
//        $notification['message'] = '
//            <div><a href="http://fooderyboston.com/"><img class="alignleft size-full wp-image-295" src="http://fooderyboston.com//wp-content/themes/wordpress-bootstrap-master/images/foodery_logo.png" alt="foodery_logo" /></a></div>
//            <p>&nbsp;</p>
//            <h3>Thank you for registering with The Foodery!</h3>
//            <p>&nbsp;</p>            
//            <h3><a href="' . $activationurl . '">Click here to verify your account!</a></h3>
//            <p>&nbsp;</p>
//            <h5><em>If the above link is not working, then copy the below URL into your browser to activate your account:</em><br />
//            ' . $activationurl . '</h5>';  
//      
//    }
//    return $notification;    
//}


//This code no longer needed since User Activation via email is not being used - 9/7/2016
/**
* Gravity Forms Custom Activation Template
* http://gravitywiz.com/customizing-gravity-forms-user-registration-activation-page
*/
//add_action('wp', 'foodery_custom_maybe_activate_user', 9);
//function foodery_custom_maybe_activate_user() {
//
//    $template_path = STYLESHEETPATH . '/activate.php';
//    $is_activate_page = isset( $_GET['page'] ) && $_GET['page'] == 'gf_activation';
//    
//    if( ! file_exists( $template_path ) || ! $is_activate_page  )
//        return;
//    
//    require_once( $template_path );
//    
//    exit();
//}

