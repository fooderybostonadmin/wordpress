<?php
/*
 * Careers / Job Listings
 * 
 */

/********************************************************************************
 * Register the post type
 ********************************************************************************/

add_action( 'init', 'foodery_register_job_post_type' );
function foodery_register_job_post_type() {
    $labels = array(
        'name' => __('Careers', 'foodery'),
        'singular_name' => __('Career', 'foodery'),
        'edit_item' => __('Edit Career', 'foodery'),
        'new_item' => __('New Career', 'foodery'),
        'all_items' => __('All Careers', 'foodery'),
        'view_item' => __('View Career', 'foodery'),
        'menu_name' => __('Careers', 'foodery')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        '_builtin' => false,
        'exclude_from_search' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_icon' => 'dashicons-businessman',
        'rewrite' => array( 'slug' => 'job' ),
        'supports' => array('title', 'editor', 'page-attributes')
    );

    register_post_type('foodery-job', $args);
}


/********************************************************************************
 * Add 'Sort Order' to Wp Admin view and make column sortable
 ********************************************************************************/    

/* Adds new column headers to 'foodery-job' post type */
function foodery_job_add_new_columns($columns)  {
    unset($columns['date']);
    unset($columns['wpseo-score']);
    
    $columns['sortorder'] = 'Sort Order';
    $columns['date'] = 'Date';
    
    return $columns;
}
add_filter( 'manage_edit-foodery-job_columns', 'foodery_job_add_new_columns' );

/* Adds content to new columns for 'foodery_job' post type */
function foodery_job_manage_columns($column_name, $id) {
	global $post;
	switch ($column_name) {
		case 'sortorder':
			echo $post->menu_order;
			break;               
		default:
			break;
	} 
}
add_action( 'manage_foodery-job_posts_custom_column', 'foodery_job_manage_columns', 10, 2 );

/* Make new 'foodery-job' columns sortable */
function foodery_job_columns_sortable($columns) {
	$custom = array(
		'sortorder'	=> 'sortorder'
	);
	return wp_parse_args($custom, $columns);
}
add_filter( 'manage_edit-foodery-job_sortable_columns', 'foodery_job_columns_sortable' );

/* Handle ordering for "Order" column */
function foodery_columns_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) && 'sortorder' == $vars['orderby'] ) {
		$vars = array_merge( $vars, array(
			'orderby' => 'menu_order'
		) );
	}      
	return $vars;
}
add_filter( 'request', 'foodery_columns_orderby' );


/********************************************************************************
 * Create shortcode to pull all careers / job listings
 ********************************************************************************/    
function foodery_careers_shortcode( $atts, $content = NULL) {
    $output = '';
    
    extract( shortcode_atts( array(
    'class' => ''      //Optional additional class for outer <div>
    ), $atts ) );
        
    //Generate query arguments to pull all job listings 
    $args = array (
        'post_type' => 'foodery-job',
        'orderby'   => 'menu_order title',
        'order'     => 'ASC',
        'posts_per_page' => -1
    );

    //Pull the posts and generate the output
    $jobs = new WP_Query( $args );
    if ( $jobs->have_posts() ) : 
        
        $output = "<div class='careers $class'>"; 
        
        while ( $jobs->have_posts() ) : $jobs->the_post(); 

            $output .= "<div class='career'>";
            $output .= "<div class='career-title'>";           
            $output .= "<a class='career-link' href='" . get_the_permalink() . "'>" . get_the_title() . "</a>";
            $output .= "</div></div>";
            
        endwhile;
        
        $output .= "</div>";
    endif;

    //Reset the query
    wp_reset_postdata(); 
    
    //Return the job listing
    return $output;
    
}
add_shortcode( 'careers', 'foodery_careers_shortcode' );