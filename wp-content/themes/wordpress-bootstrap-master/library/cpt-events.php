<?php

// Register Custom Post Type for Special Events
function foodery_special_events_cpt() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'foodery' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'foodery' ),
		'menu_name'             => __( 'Events', 'foodery' ),
		'name_admin_bar'        => __( 'Events', 'foodery' ),
		'archives'              => __( 'Event Archives', 'foodery' ),
		'parent_item_colon'     => __( 'Parent Item:', 'foodery' ),
		'all_items'             => __( 'All Events', 'foodery' ),
		'add_new_item'          => __( 'Add New Event', 'foodery' ),
		'add_new'               => __( 'Add New', 'foodery' ),
		'new_item'              => __( 'New Event', 'foodery' ),
		'edit_item'             => __( 'Edit Event', 'foodery' ),
		'update_item'           => __( 'Update Event', 'foodery' ),
		'view_item'             => __( 'View Event', 'foodery' ),
		'search_items'          => __( 'Search Events', 'foodery' ),
		'not_found'             => __( 'Not found', 'foodery' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'foodery' ),
		'insert_into_item'      => __( 'Insert into item', 'foodery' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'foodery' ),
		'items_list'            => __( 'Events list', 'foodery' ),
		'items_list_navigation' => __( 'Events list navigation', 'foodery' ),
		'filter_items_list'     => __( 'Filter Events list', 'foodery' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'foodery' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5.1,
		'menu_icon'             => 'dashicons-star-filled',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
                'rewrite'               => array( 'slug' => 'event' ),
		'capability_type'       => 'post',
	);
	register_post_type( 'foodery-events', $args );

}
add_action( 'init', 'foodery_special_events_cpt', 0 );

/* Adds new column headers */
function foodery_events_add_new_columns($columns)  {
    //Optionally remove columns (can be re-added below if you want to control order)
    unset($columns['date']);
    unset($columns['wpseo-score']);

	$columns['evtdesc'] = 'Description';
	$columns['couponcode'] = 'Coupon Code';
	$columns['coupondec'] = 'Coupon Savings';
	$columns['date'] = 'Date';
	return $columns;
}  
//Sortable
add_filter('manage_edit-foodery-events_columns', 'foodery_events_add_new_columns'); 


/* Adds content to new columns */
add_action('manage_foodery-events_posts_custom_column', 'foodery_events_manage_columns', 10, 2);
function foodery_events_manage_columns($column_name, $post_id) {
	global $post;
	switch ($column_name) {
		case 'evtdesc':
			the_field('event_description');
			break;
		case 'couponcode':
			the_field('coupon_code');
			break;
		case 'coupondec':
			the_field('coupon_desc');
			break;
		default:
			break;
	} 
}

