module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        sass: {
            options: {
                sourceMaps: true,
                outputStyle: 'expanded',
            },
            dist: {
                files: {
                  'library/css/bootstrap.css': 'library/scss/bootstrap.scss',
                  'library/css/custom.css': 'library/scss/custom.scss',
                  'library/css/responsive.css': 'library/scss/responsive.scss',
                  'library/css/skeuocard.css': 'library/scss/skeuocard.scss',
                  'library/css/woocommerce.css': 'library/scss/woocommerce.scss',
                  'library/css/woocommerce-overrides.css': 'library/scss/woocommerce-overrides.scss',
                  'library/css/wpsc-default.css': 'library/scss/wpsc-default.scss',
                  'library/css/landing-page.css': 'library/scss/landing-page.scss',
                  'library/css/homepage-v2.css': 'library/scss/homepage-v2.scss',

                }
            },
        },


        copy: {
            dist: {
                files: [
                    {expand: true, cwd: 'styles', src: ['**/*.css'], dest: 'assets/css'}
                ]
            }
        },

        watch: {
            options: {
                livereload: true
            },
            sass: {
                files: ['library/scss//**/*.scss'],
                tasks: ['sass:dist']
            },
            php: {
                files: ['**/*.php']
            },
            copy: {
                files: ['library/scss/**/*.css'],
                tasks: ['copy']
            }
        },

        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'library/css/**/*.css',
                        //'library/scss/**/*.scss',
                        './**/*.php',
                        'js/**/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    proxy: "fdry2.dev",
                    port: 1337 // Change port?

                }
            }
        }

    });

    //grunt.loadNpmTasks('grunt-contrib-compass')
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('default', ['sass:dist', 'copy:dist',  'watch']);
//    grunt.registerTask('default', ['sass:dist', 'copy:dist', 'browserSync',  'watch']);
    //grunt.registerTask('release', ['sass:dist', 'copy:dist', 'watch']);
};
