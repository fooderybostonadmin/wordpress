<?php
/*
Template Name: Event Signup Page
*/

$fullurl = $_SERVER["REQUEST_URI"];
$coupondesc = (isset($_REQUEST['desc']) ? $_REQUEST['desc'] : '');

get_header('landing'); ?>

			<div id="content" class="clearfix row-fluid" style="background-image:url(<?php echo get_field('background') ?>);">
				<div id="main" class="span12 clearfix" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

						<section class="post_content">
							<a class="logo" href="<?php echo $fullurl; ?>"><img src="<?php echo get_field( 'logo'); ?>" /></a>
							<h1><?php echo $coupondesc; ?></h1>
							<div>
								<div class="form-container">
									<div class="form-background"></div>
									<div class="form-foreground"><?php the_content(); ?></div>
								</div>
							</div>

						</section> <!-- end article section -->

					</article> <!-- end article -->

					<?php endwhile; ?>
					<?php endif; ?>
				</div> <!-- end #main -->
			</div> <!-- end #content -->

<?php get_footer('landing'); ?>
