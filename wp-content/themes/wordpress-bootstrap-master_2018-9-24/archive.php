<?php get_header(); 

$paged = (get_query_var('paged'));
$count = 0;

foodery_display_blog_header(); ?>
			
	<div id="content" class="clearfix row-fluid">
                            
            <?php foodery_display_blog_search(); ?>                            
                            
            <div id="main" class="span12 clearfix" role="main">
                <div class="blog-posts-list">
                    <div class="container">

                        <div class="page-header">
                        <?php if (is_category()) { ?>
                                <h1 class="archive_title h2">
                                        <span><?php _e("Posts Categorized:", "bonestheme"); ?></span> <?php single_cat_title(); ?>
                                </h1>
                        <?php } elseif (is_tag()) { ?> 
                                <h1 class="archive_title h2">
                                        <span><?php _e("Posts Tagged:", "bonestheme"); ?></span> <?php single_tag_title(); ?>
                                </h1>
                        <?php } elseif (is_author()) { ?>
                                <h1 class="archive_title h2">
                                    <span><?php _e("Posts By:", "bonestheme"); ?></span> 
                                    <?php 
                                            // If google profile field is filled out on author profile, link the author's page to their google+ profile page
                                            $curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
                                            $google_profile = get_the_author_meta( 'google_profile', $curauth->ID );
                                            if ( $google_profile ) {
                                                    echo '<a href="' . esc_url( $google_profile ) . '" rel="me">' . $curauth->display_name . '</a>'; 
                                    ?>
                                    <?php 
                                            } else {
                                                    echo get_the_author_meta('display_name', $curauth->ID);
                                            }
                                    ?>
                                </h1>
                        <?php } elseif (is_day()) { ?>
                                <h1 class="archive_title h2">
                                        <span><?php _e("Daily Archives:", "bonestheme"); ?></span> <?php the_time('l, F j, Y'); ?>
                                </h1>
                        <?php } elseif (is_month()) { ?>
                            <h1 class="archive_title h2">
                                <span><?php _e("Monthly Archives:", "bonestheme"); ?>:</span> <?php the_time('F Y'); ?>
                            </h1>
                        <?php } elseif (is_year()) { ?>
                            <h1 class="archive_title h2">
                                <span><?php _e("Yearly Archives:", "bonestheme"); ?>:</span> <?php the_time('Y'); ?>
                            </h1>
                        <?php } ?>
                        </div>

                        <?php if (have_posts()) : while (have_posts()) : the_post(); 
                                $excerpt = get_the_excerpt();
                                $hasphoto = has_post_thumbnail(); ?>
                        
                                <article <?php post_class('blog-normal span10 offset1 clearfix'); ?>>
                                    <div class="row-fluid">
                                        <?php if ($hasphoto) : ?>
                                        <div class="span5">
                                            <a href="<?php  echo get_permalink(); ?>"><?php the_post_thumbnail( 'medium', array('class' => '') ); ?></a>
                                        </div>
                                        <?php endif; ?>
                                        <div class="<?php echo ($hasphoto ? 'span7' : 'span12'); ?>">
                                            <h1 class="blog-title"><a href="<?php  echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
                                            <div class="blog-meta"><?php _e("By", "foodery"); ?> <?php the_author_posts_link(); ?></div>
                                            <div class="blog-excerpt"><?php echo $excerpt; ?></div>					                            
                                        </div>
                                    </div>
                                </article>   

                        <?php endwhile; ?>	

                        <div class="blog-navigation span10 offset1">
                        <?php if (function_exists('page_navi')) { // if expirimental feature is active ?>

                                <?php page_navi(); // use the page navi function ?>

                        <?php } else { // if it is disabled, display regular wp prev & next links ?>
                                <nav class="wp-prev-next">
                                        <ul class="clearfix">
                                                <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
                                                <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
                                        </ul>
                                </nav>
                        <?php } ?>
                        </div>		

                        <?php else : ?>

                        <article id="post-not-found">
                            <header>
                                <h1><?php _e("Nothing Found!", "bonestheme"); ?></h1>
                            </header>
                            <section class="post_content">
                                <p><?php _e("Sorry, we couldn't find what you were looking for.", "bonestheme"); ?></p>
                            </section>
                            <footer>
                            </footer>
                        </article>

                        <?php endif; ?>

                        </div> <!-- end container --> 
                    </div><!-- .blog-posts-list -->                                     
    </div> <!-- end #main -->

    </div> <!-- end #content -->

<?php get_footer(); ?>