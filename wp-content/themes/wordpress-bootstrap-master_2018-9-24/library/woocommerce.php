<?php
/*
 * WooCommerce related Functions (some carried over from ossium-custom-functions.php for greater visbility)
 */
define( 'FOODERY_DELIVERY_URL', 'delivery' );
define( 'FOODERY_ORDER_NOW_URL', 'order-now' );

function enqueue_style_after_wc(){
//These styles are now rolled into custom.scss / custom.css	
//	$deps = class_exists( 'WooCommerce' ) ? array( 'woocommerce-layout', 'woocommerce-smallscreen', 'woocommerce-general' ) : array();
//	wp_enqueue_style( 'zocalo-woocommerce-overrides', get_template_directory_uri() . '/library/css/woocommerce-overrides.css', $deps, 
//				filemtime(get_template_directory()  . '/library/css/woocommerce-overrides.css'));
	
	// Adding back in the neat plus/minus quantity next to the add to cart button
	wp_enqueue_script( 'wcqi-number-polyfill' );
	
}
add_action( 'wp_enqueue_scripts', 'enqueue_style_after_wc' );


// Customizing woocommerce fields
function alter_woocommerce_checkout_fields( $fields ) {
     unset($fields['order']['order_comments']);
     return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'alter_woocommerce_checkout_fields' );


//woocommerce stop emails
function unhook_those_pesky_emails( $email_class ) {

		/**
		 * Hooks for sending emails during store events
		 **/
		remove_action( 'woocommerce_low_stock_notification', array( $email_class, 'low_stock' ) );
		remove_action( 'woocommerce_no_stock_notification', array( $email_class, 'no_stock' ) );
		remove_action( 'woocommerce_product_on_backorder_notification', array( $email_class, 'backorder' ) );

		// New order emails
		//remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_pending_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_failed_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_failed_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_failed_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );

		// Processing order emails
		remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );
		remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );

		// Completed order emails
		remove_action( 'woocommerce_order_status_completed_notification', array( $email_class->emails['WC_Email_Customer_Completed_Order'], 'trigger' ) );

		// Note emails
		remove_action( 'woocommerce_new_customer_note_notification', array( $email_class->emails['WC_Email_Customer_Note'], 'trigger' ) );
}
add_action( 'woocommerce_email', 'unhook_those_pesky_emails' );



//Remove related products
function woocommerce_remove_related_products(){
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
}
add_action('woocommerce_after_single_product_summary', 'woocommerce_remove_related_products');


// Removing stuff from woocomercer_single_product_summary
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

//Move price down closer to Add to Cart button
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price',10 );
add_action( 'woocommerce_before_add_to_cart_form', 'woocommerce_template_loop_price',10 );


// Getting rid of the tabs we don't need and adding news ones
function woo_remove_product_tabs( $tabs ) {

	unset( $tabs['description'] );      	// Remove the description tab
	//unset( $tabs['reviews'] ); 			// Remove the reviews tab
	unset( $tabs['additional_information'] );  	// Remove the additional information tab

	$tabs['nutrition'] = array(
		'title'   	=> __( 'Nutrition Information', 'woocommerce' ),
		'priority'	=> 50,
		'callback'	=> 'woo_nutrition_tab_content'
	);

	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );


function woo_nutrition_tab_content() {

	global $post;

	// The new tab content
	echo $post->post_excerpt;

}

function woocommerce_theme_support() {
	// add support for woocommerce
	add_theme_support( 'woocommerce' );
	define('WOOCOMMERCE_USE_CSS', false);
}
// launching this stuff after theme setup
add_action('after_setup_theme','woocommerce_theme_support');


// Change number or products per row to 3
// http://docs.woothemes.com/document/change-number-of-products-per-row/
function foodery_loop_columns() {
	return 3; // 3 products per row
}
add_filter('loop_shop_columns', 'foodery_loop_columns');


// let's get our cute little shopping cart icon in there
function woo_custom_cart_button_text() {
	return __( '<i class="icon-shopping-cart icon-white"></i> Add To Cart', 'woocommerce' );
}
add_filter( 'add_to_cart_text', 'woo_custom_cart_button_text' );

// add_filter('adding','woocommerce_single_product_summary')
// woocommerce_single_product_summary

// Display variation's price even if min and max prices are the same
add_filter('woocommerce_available_variation', 'foodery_woocommerce_available_variation', 10, 3);
function foodery_woocommerce_available_variation ($value, $object = null, $variation = null) {
	if ($value['price_html'] == '') {
		$value['price_html'] = '<span class="price">' . $variation->get_price_html() . '</span>';
	}
	return $value;
}

// Abbreviated Order Box
// @see woocommerce_variable_add_to_cart()
// @see woocommerce_template_single_meta()

add_action( 'woocommerce_single_product_summary_mini', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary_mini', 'woocommerce_template_single_meta', 10 );

	
/**
 * If we get sent to Checkout page but we aren't logged in, go to My Account page to log in.
 */
function foodery_redirect_to_cart() {

    if( is_page( 'checkout' ) && !is_user_logged_in() ) {
    	$checkout_url = wc_get_checkout_url();
        wp_redirect( home_url('/my-account/?redirect=' . esc_url($checkout_url)) );
        exit();
    }
}
add_action('template_redirect', 'foodery_redirect_to_cart');


/**
 * Validation registration form  after submission using the filter registration_errors
 *
 * @param WP_Error $reg_errors
 *
 * @return WP_Error
 */
function registration_errors_validation( $reg_errors ) {

	if ( empty( $_POST['firstname'] ) || empty( $_POST['lastname'] ) ) {
		$reg_errors->add( 'empty required fields', __( 'Please fill in all required fields. Required fields are marked with <span class="required">*</span>', 'woocommerce' ) );
	}

	return $reg_errors;
}
add_filter( 'woocommerce_registration_errors', 'registration_errors_validation' );


/*
 * Create 'Allergens' taxonomy (replaces use of Custom Post Type UI plugin)
 */
add_action( 'init', 'foodery_register_my_taxes_allergen' );
function foodery_register_my_taxes_allergen() {
	$labels = array(
		"name" => __( 'Allergens', '' ),
		"singular_name" => __( 'Allergen', '' ),
        );

	$args = array(
		"label" => __( 'Allergens', '' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Allergens",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'allergen', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "allergen", array( "product" ), $args );
}

/*
 * Create 'Portions' taxonomy (replaces use of Custom Post Type UI plugin)
 */
add_action( 'init', 'foodery_register_my_taxes_portion' );
function foodery_register_my_taxes_portion() {
	$labels = array(
		"name" => __( 'Portions', '' ),
		"singular_name" => __( 'Portion', '' ),
        );

	$args = array(
		"label" => __( 'Portions', '' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Portions",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'portion', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "portion", array( "product" ), $args );
}


/*
 * Add Delivery Phone & Delivery Email fields to WooCommerce Checkout
 */
add_filter( 'woocommerce_checkout_fields' , 'foodery_addtl_checkout_fields' );
function foodery_addtl_checkout_fields( $fields ) {

    $fields['shipping']['shipping_phone'] = array(
            'type' => 'tel',
            'label' => __('Delivery Phone <abbr class="required" title="required">*</abbr>', 'foodery'),
            'placeholder' => '', // _x('Delivery Phone', 'placeholder', 'foodery'),
            'required'  => false,
            'class'     => array(),
            'clear' => true
    );
    $fields['shipping']['shipping_email'] = array(
            'type' => 'text',
            'label' => __('Delivery Email <abbr class="required" title="required">*</abbr>', 'foodery'),
            'placeholder' => '', // _x('Delivery Email', 'placeholder', 'foodery'),
            'required'  => false,
            'class'     => array(),
            'clear' => true
    );

    return $fields;
}


/**
 * Add 'Delivery Instructions' field to the WooCommerce Checkout
 */
add_action( 'woocommerce_checkout_after_customer_details', 'foodery_add_custom_delivery_instructions' );
function foodery_add_custom_delivery_instructions( ) {
    $user_id = get_current_user_id();
    $instructions = get_user_meta( $user_id, 'shipping-notes-note', true );

    echo '<div id="my_custom_checkout_field"><h2>' . __('Delivery Instructions') . '</h2>';

    woocommerce_form_field( 'delivery-instructions',
        array(
            'type'          => 'textarea',
            'class'         => array('input-textarea delivery-instructions'),
            'label'         => __('Please let us know of any special delivery instructions'),
            'clear' => true
        ), $instructions );

    echo '</div>';
}


/**
 * Display additional checkout field values on the order edit page (in WP Admin)
 */
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'foodery_checkout_field_display_admin_order_meta', 10, 1 );
function foodery_checkout_field_display_admin_order_meta($order){
    $addtlflds = array(
        array( '_shipping_phone', 'Phone'),
        array( '_shipping_email', 'Email'),
        array( 'order-delivery-date', 'Delivery Date' ),
        array( 'Delivery Instructions', 'Instructions' )
    );

	echo '<h3>Delivery Details</h3>';
	echo '<p>';
	
    foreach ( $addtlflds as $fld) :
            echo '<strong>'.__( $fld[1] ).':</strong> ' . get_post_meta( $order->id, $fld[0], true ) . '<br/>';
    endforeach;
	
	echo '</p>';
}


/**
 * Update the order & user meta with field values
 */
add_action( 'woocommerce_checkout_update_order_meta', 'foodery_checkout_field_update_order_meta' );
function foodery_checkout_field_update_order_meta( $order_id ) {
    //Get current user ID
    $user_id = get_current_user_id();


    //Update 'Delivery Phone' and 'Delivery Email' User Meta fields
    update_user_meta($user_id, '_shipping_phone', (!empty( $_POST['shipping_phone'] ) ? sanitize_text_field( $_POST['shipping_phone']) : '') );
    update_user_meta($user_id, '_shipping_email', (!empty( $_POST['shipping_email'] ) ? sanitize_text_field( $_POST['shipping_email']) : '') );

    //Update both 'Order' record & User Meta with 'Delivery Instructions'
    $instructions = ( ! empty( $_POST['delivery-instructions'] ) ? sanitize_text_field( $_POST['delivery-instructions'] ) : '' );
    //Save Order meta value
    update_post_meta( $order_id, 'Delivery Instructions', $instructions );
    //Save for User meta as well
    update_user_meta( $user_id,   'shipping-notes-note', $instructions );

}

/*
 * Display the extra field on order recieved page and my-account order review
 */
function foodery_display_order_delivery_date( $order_id ){  
	if ( $order_id != '' ) {
		$delivery_date = get_post_meta( $order_id, 'order-delivery-date', true );
	} else {
		$delivery_date = foodery_get_order_delivery_date();
	}
	
	if ( $delivery_date != '' ) : 
		$d = DateTime::createFromFormat("Y-m-d", $delivery_date); ?>
		<div class="delivery-date-check">
			<div class="delivery-date-selection-title">Delivery Date: <?php echo $d->format("D M j"); ?></div>
		</div>
	<?php endif;
}
add_action( 'woocommerce_thankyou', 'foodery_display_order_delivery_date', 5 );
add_action( 'woocommerce_view_order', 'foodery_display_order_delivery_date', 5 );

/*
 * Display the new Delivery Date field on order recieved page and my-account order review
 */
function foodery_display_extra_order_data( $order_id ){  
	$instructions = get_post_meta( $order_id, 'Delivery Instructions', true );
	?>
    <div class="order-extra-fields">
        <h3><?php _e( 'Delivery Instructions' ); ?></h3>
        <div class="order-field-content"><?php echo ($instructions ? $instructions : '<em>No special delivery instructions were given.</em>'); ?></div>
    </div>
<?php }
add_action( 'woocommerce_thankyou', 'foodery_display_extra_order_data', 20 );
add_action( 'woocommerce_view_order', 'foodery_display_extra_order_data', 20 );

/*
 * Display the new Delivery Date field on order recieved page and my-account order review
 */
function foodery_finish_order( $order_id ){
	//Track last order ID so we can retrieve Delivery Date as needed
	if ( $order_id != '' ) {
		$_SESSION['last_order_id'] = $order_id;	
	}
}
add_action( 'woocommerce_thankyou', 'foodery_finish_order', 1 );

/*
 * Add Additional WooCommerce Checkout validation (for new fields)
 */
add_action('woocommerce_checkout_process', 'foodery_validate_checkout_field_process');
function foodery_validate_checkout_field_process() {
	global $woocommerce;
	
	//Check for missing ship phone and email
    $diffshipaddress = isset( $_POST['ship_to_different_address'] ) ? true : false;

    if ($diffshipaddress) :
        if ( ! isset($_POST['shipping_phone']))
            wc_add_notice( __( 'Please enter a Delivery Phone Number.' ), 'error' );
        elseif ( $_POST['shipping_phone'] == '')
            wc_add_notice( __( 'Please enter a Delivery Phone Number.' ), 'error' );

        if ( ! isset($_POST['shipping_email']))
            wc_add_notice( __( 'Please enter a Delivery Email Address.' ), 'error' );
        elseif ( $_POST['shipping_email'] == '')
            wc_add_notice( __( 'Please enter a Delivery Email Address.' ), 'error' );

    endif;
	
	//Check for conflicting delivery dates
	$error_msg = foodery_check_order_delivery_dates_old();
	if ( $error_msg != '' ) {
		wc_add_notice( $error_msg, 'error' );
	}
}

function foodery_check_order_delivery_dates() {
	global $woocommerce, $wpdb;
	
	//Get current date and time
	$current_time = current_time( 'timestamp', 0 );
	$today = date('Y-m-d', $current_time);
	$time = date('H:i:s', $current_time);
	
	//Get Delivery date chosen for the order
	$order_delivery_date = foodery_get_order_delivery_date();
	
	//Initialize delivery date to be blank
	$delivery_date = '';
	//Used to track how many delivery dates on each product match the order delivery date selected 
	//(only 1 has to match, or fine if there are no delivery dates on the product)
	$dates_matched = 0;
	$dates_found = 0;
	
	$delivery_cut_off_time = get_field( 'delivery_cut_off_time', 'option' ); //Returns time in the format hh:mm:ss	
	$buffer_minutes = get_field('order_buffer_minutes', 'option');
		
	$product_id_list = '';
	
	//Loop through cart items 
	foreach ( WC()->cart as $cart_item_key => $values ) {
		if ( $cart_item_key == 'cart_contents') {
			foreach ( $values as $product ) {
				//Get product ID
				$product_id = $product['product_id'];
				
				//Add this product ID to the list to check
				if ( $product_id != '' ) {
					$product_id_list .= $product_id . ',';
				}				
			}
			
			//Remove last comma off product ID list
			if ( $product_id_list != '' ) {

				$sql = "select posts.ID, posts.post_title, terms.term_id, 
						COALESCE(ddate1.meta_value,'') Delivery1, COALESCE(ddate2.meta_value,'') Delivery2,
						COALESCE(startdate1.meta_value, '') Start1, COALESCE(startdate2.meta_value, '') Start2,
						COALESCE(enddate1.meta_value, '') End1, COALESCE(enddate2.meta_value, '') End2
						from $wpdb->posts posts 
						INNER JOIN $wpdb->term_relationships ON posts.ID = wp_term_relationships.object_id 
						INNER JOIN $wpdb->term_taxonomy term_taxonomy USING (term_taxonomy_id)
						INNER JOIN $wpdb->terms terms USING (term_id)
						LEFT OUTER JOIN $wpdb->termmeta ddate1 ON terms.term_id = ddate1.term_id and ddate1.meta_key = 'product-cat-delivery-date-1' 
						LEFT OUTER JOIN $wpdb->termmeta ddate2 ON terms.term_id = ddate2.term_id and ddate2.meta_key = 'product-cat-delivery-date-2'
						LEFT OUTER JOIN $wpdb->termmeta startdate1 ON terms.term_id = startdate1.term_id and startdate1.meta_key = 'product-cat-start-date-1' 
						LEFT OUTER JOIN $wpdb->termmeta startdate2 ON terms.term_id = startdate2.term_id and startdate2.meta_key = 'product-cat-start-date-2'
						LEFT OUTER JOIN $wpdb->termmeta enddate1 ON terms.term_id = enddate1.term_id and enddate1.meta_key = 'product-cat-end-date-1' 
						LEFT OUTER JOIN $wpdb->termmeta enddate2 ON terms.term_id = enddate2.term_id and enddate2.meta_key = 'product-cat-end-date-2'
						WHERE posts.ID in (" . $product_id_list . ") 
							and term_taxonomy.taxonomy = 'product_cat' and terms.term_id <> 347 
							and ( COALESCE(ddate1.meta_value,'') != '' or COALESCE(ddate2.meta_value,'') != '' )
						ORDER BY FIND_IN_SET(posts.ID, '" . $product_id_list . "'), ddate1.meta_value DESC, ddate2.meta_value DESC
					";

				$products = $wpdb->get_results( $sql, ARRAY_N );	
				$current_row = 0;


				if ( count($products) > 1 ) :
					
					//If Order Delivery Date cannot be found (and at least 1 produc has delivery dates)
					//prompt user to select date again
					if ( $order_delivery_date == '' ) {
						return "Oops, we weren't to locate your delivery date. Please 
							<a href='" . home_url() . "/menu/?order-now'>select your delivery date</a> again
							or <a href='" . wc_get_cart_url() . "?empty-cart'>clear your cart</a>
							to begin again. If you continue to run into this problem, please <a href='" . home_url() . "/faqs/'>contact us</a>.";	
					}

					//Loop through cart products again...
					foreach ( $values as $product ) {
						if ( $product_id != '' ) {
							//Used to track how many delivery dates on each product match 
							// the order delivery date selected (only 1 has to match, or 
							// find if there are no delivery dates on the product)
							$dates_matched = 0;
							$dates_found = 0;

							error_log('Product ID ' . $product_id);
							for ( $i = $current_row; $products[$i]['ID'] == $product_id; $i++ ) {
								error_log('Checking row ' . $i . ' (Product ID ' . $product_id . ')');

								//Get Delivery dates for this period (1 or 2)
								$delivery_date = get_term_meta($term_id, 'product-cat-delivery-date-' . $i, true);
								//Get Start and End dates for this period (1 or 2)
								$start_date = get_term_meta($term_id, 'product-cat-start-date-' . $i, true);
								$end_date = get_term_meta($term_id, 'product-cat-end-date-' . $i, true);											

								if ( $delivery_date == '' || $start_date == '' || $end_date == '' ) {
									//If there is no delivery date for this product category, do nothing
								} else {
									//Increment dates found count (since delivery date was found) - we'll comapre this to matched date count
									$dates_found++;

									error_log( 'Checking Delivery Date ' . $i . ' Delivery: ' . $delivery_date . ' ~~~ Start: ' . $start_date . ' ~~ End: ' . $end_date);

									//Is Order date within the order date period for this product category / menu
									if (    (($today > $start_date && $today < $end_date ) ||
											( $today == $start_date && $time >= $delivery_cut_off_time ) ||
											( $today == $end_date && $time <= $delivery_cut_off_time )) 
											&& $order_delivery_date == $delivery_date ) {	

										//All good - increment count on good dates
										$dates_matched++;
										error_log( "Order date/time matched normal timeframe. dates_matched++");
									} else {
										//Add buffer time to delivery cut off time and re-compare
										$bln_next_day = false;
										//Get time parts
										$hour = substr( $delivery_cut_off_time, 0, 2);
										$min = substr( $delivery_cut_off_time, 3, 2);
										$sec = substr( $delivery_cut_off_time, 6, 2);

										//Add buffer in minutes (bumping hour/day if need be)
										if ( (intval($min) + $buffer_minutes) >= 60 ) {
											//Does bumping to next hour bump us to the next day?
											if ( (intval($hour) + 1) > 23 ) { //Yes
												//Bump to next day 
												$bln_next_day = true;

												//Add buffer time, and bump hour to next hour and day
												$buffer_cut_off_time = '00:' . strval(intval($min) + $buffer_minutes - 60) . ':' . $sec;														
												error_log( "Checking against next day");
											} else { //No, not bumped to next day
												//Add buffer time and bump hour to next hour
												$buffer_cut_off_time = strval($hour + 1) . ':' . strval(intval($min) + $buffer_minutes - 60) . ':' . $sec;														
												error_log( "Checking against next hour");
											}
										} else {
											//Add buffer time (same hour)
											$buffer_cut_off_time = $hour . ':' . strval(intval($min) + $buffer_minutes) . ':' . $sec;
											error_log( "Checking against buffer time (same hour)");
										}

										//If buffer bumps us to next day, bump end date out 1 day
										if ( $bln_next_day ) {
											$end_date = date('Y-m-d', strtotime($end_date . ' +1 day'));
										}	

										//Do same check as before but using buffer cut off time & possibly new end date
										if (    (($today > $start_date && $today < $end_date ) ||
												( $today == $start_date && $time >= $delivery_cut_off_time ) ||
												( $today == $end_date && $time <= $buffer_cut_off_time ) )
												&& $order_delivery_date == $delivery_date ) {

											//All good - increment count on good dates
											$dates_matched++;
											error_log( "Order date/time matched buffer timeframe. dates_matched++ = " . $dates_matched);

										}

									}								
								} //if ( $delivery_date == '' || $start_date == '' || $end_date == '' ) 
								$current_row = $i;
							} // for ( $i = $current_row; $products[$i]['ID'] == $product_id; $i++ ) 			
						}// if ( $product_id != '' ) {			
					} // foreach ( $values as $product ) 
				
				endif;
			}
		}
		
	}	
}
function foodery_check_order_delivery_dates_old() {	
	global $woocommerce;
	
	//Get current date and time
	$current_time = current_time( 'timestamp', 0 );
	$today = date('Y-m-d', $current_time);
	$time = date('H:i:s', $current_time);
	
	//Get Delivery date chosen for the order
	$order_delivery_date = foodery_get_order_delivery_date();
	
	//Initialize delivery date to be blank
	$delivery_date = '';
	//Used to track how many delivery dates on each product match the order delivery date selected 
	//(only 1 has to match, or fine if there are no delivery dates on the product)
	$dates_matched = 0;
	$dates_found = 0;
	
	$delivery_cut_off_time = get_field( 'delivery_cut_off_time', 'option' ); //Returns time in the format hh:mm:ss	
	$buffer_minutes = get_field('order_buffer_minutes', 'option');
	
	//Loop through cart items 
	foreach ( WC()->cart as $cart_item_key => $values ) {
		if ( $cart_item_key == 'cart_contents') {
			foreach ( $values as $product ) {
				//Get product ID
				$product_id = $product['product_id'];
				
				if ( $product_id != '' ) {
					
					//Default to no match for this product
					$product_matched = false;
					$dates_matched = 0;
					$dates_found = 0;

					//Pull Product Categories / Menus associated with this product
					$product_terms = wp_get_post_terms( $product_id, 'product_cat' );
					
					if ( ! empty( $product_terms ) ) {
						if ( ! is_wp_error( $product_terms ) ) {

							//Loop through each associated Product Category / Menu
							foreach( $product_terms as $term ) {
								
								//Don't worry about gift cards since they don't have delivery dates
								if ( $term->slug != 'gift-card' ) {
								
									error_log('Checking product ' . $product_id . ' of term ' . $term->slug );
									
									//If Order Delivery Date cannot be found, prompt user to select date again
									if ( $order_delivery_date == '' ) {
										global $woocommerce;
										return "Oops, we weren't to locate your delivery date. Please 
											<a href='" . home_url() . "/menu/?order-now'>select your delivery date</a> again
											or <a href='" . wc_get_cart_url() . "?empty-cart'>clear your cart</a>
											to begin again. If you continue to run into this problem, please <a href='" . home_url() . "/faqs/'>contact us</a>.";	
									}
									
									$term_id = $term->term_id;

									//Loop through both order period custom date fields (1 & 2)
									for ($i = 1; $i <= 2; $i++) {	
										
										//Get Delivery dates for this period (1 or 2)
										$delivery_date = get_term_meta($term_id, 'product-cat-delivery-date-' . $i, true);
										//Get Start and End dates for this period (1 or 2)
										$start_date = get_term_meta($term_id, 'product-cat-start-date-' . $i, true);
										$end_date = get_term_meta($term_id, 'product-cat-end-date-' . $i, true);											
										
										if ( $product_matched || $delivery_date == '' || $start_date == '' || $end_date == '' ) {
											//If there is no delivery date for this product category, do nothing
										} elseif ( $dates_matched == 0 ) {
											//Increment dates found count (since delivery date was found) - we'll comapre this to matched date count
											$dates_found++;
										
											error_log( 'Checking Delivery Date ' . $i . ' Delivery: ' . $delivery_date . ' ~~~ Start: ' . $start_date . ' ~~ End: ' . $end_date);
											
											//Is Order date within the order date period for this product category / menu
											if (    (($today > $start_date && $today < $end_date ) ||
													( $today == $start_date && $time >= $delivery_cut_off_time ) ||
													( $today == $end_date && $time <= $delivery_cut_off_time )) 
													&& $order_delivery_date == $delivery_date ) {	

												//All good - increment count on good dates
												$dates_matched++;
												//error_log( "Order date/time matched normal timeframe. dates_matched++");
											} else {
												//Add buffer time to delivery cut off time and re-compare
												$bln_next_day = false;
												//Get time parts
												$hour = substr( $delivery_cut_off_time, 0, 2);
												$min = substr( $delivery_cut_off_time, 3, 2);
												$sec = substr( $delivery_cut_off_time, 6, 2);

												//Add buffer in minutes (bumping hour/day if need be)
												if ( (intval($min) + $buffer_minutes) >= 60 ) {
													//Does bumping to next hour bump us to the next day?
													if ( (intval($hour) + 1) > 23 ) { //Yes
														//Bump to next day 
														$bln_next_day = true;

														//Add buffer time, and bump hour to next hour and day
														$buffer_cut_off_time = '00:' . strval(intval($min) + $buffer_minutes - 60) . ':' . $sec;														
														//error_log( "Checking against next day");
													} else { //No, not bumped to next day
														//Add buffer time and bump hour to next hour
														$buffer_cut_off_time = strval($hour + 1) . ':' . strval(intval($min) + $buffer_minutes - 60) . ':' . $sec;														
														//error_log( "Checking against next hour");
													}
												} else {
													//Add buffer time (same hour)
													$buffer_cut_off_time = $hour . ':' . strval(intval($min) + $buffer_minutes) . ':' . $sec;
													//error_log( "Checking against buffer time (same hour)");
												}

												//If buffer bumps us to next day, bump end date out 1 day
												if ( $bln_next_day ) {
													$end_date = date('Y-m-d', strtotime($end_date . ' +1 day'));
												}	

												//Do same check as before but using buffer cut off time & possibly new end date
												if (    (($today > $start_date && $today < $end_date ) ||
														( $today == $start_date && $time >= $delivery_cut_off_time ) ||
														( $today == $end_date && $time <= $buffer_cut_off_time ) )
														&& $order_delivery_date == $delivery_date ) {

													//All good - increment count on good dates
													$dates_matched++;
													error_log( "Order date/time matched buffer timeframe. dates_matched++ = " . $dates_matched);

												}

											}
										
										}
										
									} //for ($i = 1; $i <= 2; $i++) 
									
								} //if ( $term->slug != 'gift-card' ) 
								
							} //foreach( $product_terms as $term )

							error_log( 'Dates Found: ' . $dates_found );
							error_log( 'Dates Matched: ' . $dates_matched );

							//If we found dates and we haven't already matched this product's delivery dates
							if ( $dates_found > 0 && $dates_matched > 0 ) {
								//Totally fine if only 1 date matched, but if no 
								//dates match then we have a problem
									error_log("Mark this product's delivery dates as matched");
									$product_matched = true;
							} elseif ( $dates_found > 0 ) {
									return "Oops, something happened and we weren't able to complete your request due to differing 
										or expired delivery dates in your cart. To try and fix the issue, please 
										<a href='" . wc_get_cart_url() . "'>review your cart</a> or 
										<a href='" . wc_get_cart_url() . "?empty-cart'>clear your cart</a>. 
										If you continue to run into this problem, please <a href='" . home_url() . "/faqs/'>contact us</a>.";
					
							}
														
						}
					}				
				} //if ( $product_id != '' )
									
			} //foreach ( $values as $product )
		} //if ( $cart_item_key == 'cart_contents')
				
	} //foreach ( WC()->cart as $cart_item_key => $values )
	
	//If we didn't encounter any conflicting delivery dates, then validate successfully
	return '';
}

// check for empty-cart get param to clear the cart
function foodery_woocommerce_clear_cart_url() {
  global $woocommerce;
	
	if ( isset( $_GET['empty-cart'] ) ) {
		$woocommerce->cart->empty_cart(); 
	}
}
add_action( 'init', 'foodery_woocommerce_clear_cart_url' );

/*
 * Add new fields to User Profile screen
 */
add_action( 'show_user_profile', 'foodery_display_user_custom_fields' );
add_action( 'edit_user_profile', 'foodery_display_user_custom_fields' );
function foodery_display_user_custom_fields( $user ) { ?>
    <h3>WooCommerce Additional Fields</h3>
    <table class="form-table">
        <tr>
            <th><label>Delivery Phone</label></th>
            <td><input type="text" name="shipping_phone" id="shipping_phone" 
                       value="<?php echo get_user_meta( $user->ID, '_shipping_phone', true ); ?>" /></td>
        </tr>
        <tr>
            <th><label>Delivery Email</label></th>
            <td><input type="text"  name="shipping_email" id="shipping_email" 
                       value="<?php echo get_user_meta( $user->ID, '_shipping_email', true ); ?>" /></td>
        </tr>
        <tr>
            <th><label>Delivery Instructions</label></th>
            <td><textarea rows="2" cols="100" name="shipping_note" id="shipping_note" /><?php echo esc_attr(get_user_meta( $user->ID, 'shipping-notes-note', true )); ?></textarea></td>
        </tr>
    </table>
    <?php
}

function foodery_save_user_custom_fields( $user_id ) {
	
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	
	update_user_meta( $user_id, '_shipping_phone', $_POST['shipping_phone'] );
	update_user_meta( $user_id, '_shipping_email', $_POST['shipping_email'] );
	update_user_meta( $user_id, 'shipping-notes-note', $_POST['shipping_note'] );
}
add_action( 'personal_options_update', 'foodery_save_user_custom_fields' );
add_action( 'edit_user_profile_update', 'foodery_save_user_custom_fields' );


/**
 * Redirect users to Menu page after add to cart.
 */
function foodery_add_to_cart_redirect( $url ) {
	
    return home_url('/menu/');
}
add_filter( 'woocommerce_add_to_cart_redirect', 'foodery_add_to_cart_redirect' );


/*
 * Modify default WooCommerce inventory text
 */
function foodery_woocommerce_get_availability_text( $availability, $this ) {
    if ($availability == 'In stock') {
        //When product is in stock, no need to show this text
        $availability = '';
    } elseif ($availability == 'Out of stock') {
        //When product is out of stock, show "Sold Out"
        $availability = 'Sold Out';
    } elseif (substr(strtolower ($availability), -13) == 'left in stock') {
        //When product is almost gone, tweak the text
        $availability = str_replace( 'left in stock', 'left', $availability);
    } 
    
    return $availability;
}
add_filter( 'woocommerce_get_availability_text', 'foodery_woocommerce_get_availability_text', 10, 2);


/***********************************************************************
 * WooCommerce - Add Start/End/Delivery Dates to Product Categories
 * 
 * From https://stackoverflow.com/questions/23469841/adding-custom-field-to-product-category-in-woocommerce
 ***********************************************************************/
//Product Cat Create page
function foodery_product_cat_add_new_meta_field() {
	
	//Loop through both order periods
	for ($i = 1; $i <= 2; $i++) {	
    ?>

		<div class="form-field">
			<label><?php _e('Order Date (Period ' . $i . ')', 'foodery'); ?></label>
			<input type="date" name="product-cat-start-date-<?php echo $i; ?>" id="product-cat-start-date-<?php echo $i; ?>"> - 
			<input type="date" name="product-cat-end-date-<?php echo $i; ?>" id="product-cat-end-date-<?php echo $i; ?>">
			<p class="description"><?php _e('Enter the start and end date for the order period, in the format YYYY-MM-DD', 'foodery'); ?></p>
		</div>
		<div class="form-field">
			<label for="product-cat-delivery-date-1"><?php _e('Delivery Date (Period ' . $i . ')', 'foodery'); ?></label>
			<input type="date" name="product-cat-delivery-date-<?php echo $i; ?>" id="product-cat-delivery-date-<?php echo $i; ?>">
			<p class="description"><?php _e('Enter the delivery date for the order period, in the format YYYY-MM-DD', 'foodery'); ?></p>
		</div>
	

    <?php
	}
}
add_action('product_cat_add_form_fields', 'foodery_product_cat_add_new_meta_field', 10, 1);

/*
 * Add Start, End, and Delivery dates (2 sets) to the Product Category taxonomy
 */
function foodery_product_cat_edit_meta_field($term) {
	$start_date = '';
	$end_date = '';
	$delivery_date = '';
		
    //getting term ID
    $term_id = $term->term_id;
	
	//Loop through both order periods
	for ($i = 1; $i <= 2; $i++) {
		// retrieve the existing values for the order period
		$start_date = get_term_meta($term_id, 'product-cat-start-date-' . $i, true);
		$end_date = get_term_meta($term_id, 'product-cat-end-date-' . $i, true);
		$delivery_date = get_term_meta($term_id, 'product-cat-delivery-date-' . $i, true);
		
		?>
    <tr class="form-field">
		<th scope="row" valign="top">
			<label><?php _e('Order Date (Period ' . $i . ')', 'foodery'); ?></label>
		</th>
		<td>
        <input type="date" name="product-cat-start-date-<?php echo $i; ?>" id="product-cat-start-date-<?php echo $i; ?>"
			   value="<?php echo esc_attr($start_date) ? esc_attr($start_date) : ''; ?>" > - 
		<input type="date" name="product-cat-end-date-<?php echo $i; ?>" id="product-cat-end-date-<?php echo $i; ?>"
			   value="<?php echo esc_attr($end_date) ? esc_attr($end_date) : ''; ?>">
        <p class="description"><?php _e('Enter the start and end date for the order period, in the format YYYY-MM-DD', 'foodery'); ?></p>
		</td>
    </tr>
    <tr class="form-field">
		<th scope="row" valign="top">
			<label for="product-cat-delivery-date-1"><?php _e('Delivery Date (Period ' . $i . ')', 'foodery'); ?></label>
		</th>
		<td>
        <input type="date" name="product-cat-delivery-date-<?php echo $i; ?>" id="product-cat-delivery-date-<?php echo $i; ?>"
			   value="<?php echo esc_attr($delivery_date) ? esc_attr($delivery_date) : ''; ?>" >
        <p class="description"><?php _e('Enter the delivery date for the order period, in the format YYYY-MM-DD', 'foodery'); ?></p>
		</td>
    </tr>
	
	<?php		
	}
}
add_action('product_cat_edit_form_fields', 'foodery_product_cat_edit_meta_field', 10, 1);

/*
 * Save the Product Category meta fields for Start, End, and Delivery dates (2 sets)
 */
function foodery_product_save_meta_values($term_id) {

	//Loop through both order periods
	for ($i = 1; $i <= 2; $i++) {
		//Retrieve the inputted values
		$start_date = filter_input(INPUT_POST, 'product-cat-start-date-' . $i);
		$end_date = filter_input(INPUT_POST, 'product-cat-end-date-' . $i);
		$delivery_date = filter_input(INPUT_POST, 'product-cat-delivery-date-' . $i);

		//Save values to term meta table
		update_term_meta($term_id, 'product-cat-start-date-' . $i, $start_date);
		update_term_meta($term_id, 'product-cat-end-date-' . $i, $end_date);		
		update_term_meta($term_id, 'product-cat-delivery-date-' . $i, $delivery_date);		
	}
}
add_action('edited_product_cat', 'foodery_product_save_meta_values', 10, 1);
add_action('create_product_cat', 'foodery_product_save_meta_values', 10, 1);

/***********************************************************************
 * END WooCommerce - Add Start/End/Delivery Dates to Product Categories
/***********************************************************************


/*
 * When an Order is saved, also save the Delivery Date if oen exists
 */
function foodery_woocommerce_checkout_order_processed( $order_id, $posted_data, $order ) {	
	//If we have a delivery date, then store it on the order
	$delivery_date = foodery_get_order_delivery_date();
	if ($delivery_date != '') {
		update_post_meta( $order_id, 'order-delivery-date', $delivery_date );
	} 
	
}
add_action( 'woocommerce_checkout_order_processed', 'foodery_woocommerce_checkout_order_processed', 10, 3 );

/*
 * Get current order end and delivery dates for display on the Menu page
 */
function foodery_get_product_end_delivery_dates( $product_id, $output = "both", $nextdaybuffer = true ) {
	$current_time = current_time( 'timestamp', 0 );
	//$current_time = new DateTime("now");
	$today = date('Y-m-d', $current_time);
	$time = date('H:i:s', $current_time);
	$delivery_dates = array();
	$count = 0;
	
	//Determine how many delivery dates to include
	$dates_available = get_field( 'num_delivery_dates', 'option');
	//Determine current cut-off time for orders
	$delivery_cut_off_time = get_field( 'delivery_cut_off_time', 'option' ); //Returns time in the format hh:mm:ss
	
	if ( $product_id != '' ) {
		$product_terms = wp_get_post_terms( $product_id, 'product_cat' );

		if ( ! empty( $product_terms ) ) {
			if ( ! is_wp_error( $product_terms ) ) {

				foreach( $product_terms as $term ) {

					if ( $term->slug != 'gift-card' ) {
						$term_id = $term->term_id;

						//Loop through both order periods
						for ($i = 1; $i <= 2; $i++) {									
							$start_date = get_term_meta($term_id, 'product-cat-start-date-' . $i, true);
							$end_date = get_term_meta($term_id, 'product-cat-end-date-' . $i, true);
							$delivery_date = '';

							//Is current date /time within the order date period for this product category / menu
							if ( $start_date != '' && $end_date != '' ) {
								if (	($today > $start_date && $today < $end_date ) ||
										( $today == $start_date && $time >= $delivery_cut_off_time ) ||
										( $today == $end_date && $time <= $delivery_cut_off_time ) ) {
								
									$delivery_date = get_term_meta($term_id, 'product-cat-delivery-date-' . $i, true);
									//error_log("Delivery Date match: " . $delivery_date);
								} 

								//Save this delivery date along with associated end date
								if ( $delivery_date != '' && $count < $dates_available ) {
									//Return the desired output
									switch ( $output ) {
									case "delivery" :
										$delivery_dates[] = $delivery_date;
										break;
									case "end" :
										$delivery_dates[] = $end_date;
										break;
									default :
										$delivery_dates[] = $end_date . "," . $delivery_date;
										break;
									}
									$count++;
								} 											
							}
						} //for ($i = 1; $i <= 2; $i++) 
					} //if ( $term->slug != 'gift-card' ) 

				} //foreach( $product_terms as $term )

			}
		}				
	} //if ( $product_id != '' )

	if ( !empty($delivery_dates)) {
		return implode("|", $delivery_dates);
	}
}


/*
 * Used when retrieving Menu items for Menu page to ensure we only
 * retreive items from active menus.
 * 
 * Returns an array of the Product Category / Menu Term IDs
 */
function foodery_get_current_menus( $num_menus = 0 ) {
	//TODO - Store in transient?
	
	//Create an empty array
	$menu_term_ids = array();
	
	//Grab todays date formatted just likes menu dates
	$current_time = current_time( 'timestamp', 0 );	
	$today = date('Y-m-d', $current_time);
	$time = date( 'H:i:s', $current_time);

	//Determine current cut-off time for orders
	$delivery_cut_off_time = get_field( 'delivery_cut_off_time', 'option' ); //Returns time in the format hh:mm:ss
	
	//Determine how many delivery dates to include
	if ( $num_menus == 0 ) {
		$num_menus = get_field( 'num_delivery_dates', 'option');
	} 
	
	//Loop through both order periods
	for ($i = 1; $i <= 2; $i++) {	
		//Query for product categories in order of delivery dates
		$args = array(
			'taxonomy'   => 'product_cat',
			'hide_empty' => true,
			'meta_query' => array(
				'relation' => 'AND',
				 array(
					'key'       => 'product-cat-start-date-' . $i,
					'value'     => $today,
					'compare'   => '<=',
					'type'		=> 'DATE'
				 ),
				 array(
					'key'       => 'product-cat-end-date-' . $i,
					'value'     => $today,
					'compare'   => '>=',
					'type'		=> 'DATE'
				 )			
			),
			'orderby'       =>  'product-cat-delivery-date-' . $i,
			'order'         =>  'ASC'
		);	
		$terms = get_terms( $args );	
		
		//Loop through retrieved terms and create array with delivery dates
		if ( ! empty( $terms ) ) {
			if ( ! is_wp_error( $terms ) ) {

				foreach( $terms as $term ) {
					$start_date = get_term_meta($term->term_id, 'product-cat-start-date-' . $i, true);
					$end_date = get_term_meta($term->term_id, 'product-cat-end-date-' . $i, true);
					
					//error_log( $term->name . ' : Start - End : ' . $start_date . ' - ' . $end_date);
					//error_log( $term->name . ' : Now - Cutoff : ' . $time . ' - ' . $delivery_cut_off_time);
					
					//Is current date /time within the order date period for this product category / menu
					if (	($today > $start_date && $today < $end_date ) ||
							( $today == $start_date && $time >= $delivery_cut_off_time ) ||
							( $today == $end_date && $time <= $delivery_cut_off_time ) ) {
						
								$delivery_date = get_term_meta($term->term_id, 'product-cat-delivery-date-' . $i, true);
								$menu_term_ids[$delivery_date] = $term->term_id;
								//error_log("Delivery Date match: " . $delivery_date);

					}								

				}
			}
		}
	}
	//error_log( print_r($menu_term_ids, true) );
	//Sort by Delivery Date
	ksort($menu_term_ids);
	
	//error_log( print_r(array_slice($menu_term_ids, 0, $num_menus), true) );
	
	//Return the term IDs for the desired # of delivery dates
	return array_slice($menu_term_ids, 0, $num_menus);
//	$menus = array_slice($menu_term_ids, 0, $num_menus);
//	error_log( print_r($menus, true));
//	$menu_ids = array();
//	foreach ($menus as $menu_id ) :
//		$menu_ids[] = $menu_id;
//	endforeach;
//	return $menu_ids;
}

	/**
	 * Output WooCommerce content.
	 *
	 * This function is only used in the optional 'woocommerce.php' template.
	 * which people can add to their themes to add basic woocommerce support.
	 * without hooks or modifying core templates.
	 * 
	 * Customization here are to add a Delivery Date header and an order Now button beneath each Delivery Date section
	 *
	 */
	function woocommerce_content() {
		global $post, $woocommerce, $wpdb;
		
		if ( is_singular( 'product' ) ) {

			while ( have_posts() ) : the_post();

				wc_get_template_part( 'content', 'single-product' );

			endwhile;

		} else { 
			//Determine how many delivery dates to include
			$num_menus = get_field( 'num_delivery_dates', 'option');	
			$current_menu_id = '';
			//Determine cut off time to display below
			$cut_off_time = foodery_delivery_cut_off_time_shortcode();
			//Get Order Delivery Date
			$order_delivery_date = foodery_get_order_delivery_date();
			
			//Query for menu items
			$menu_id_list = '';
			$delivery_date_list = '';

			//Grab the term IDs for the menus for which we should be displaying products
			//Return format >>> $menu_ids[delivery_date] = term_id;
			$menu_ids = foodery_get_current_menus( $num_menus );	
			//error_log( print_r($menu_ids, true) );

			//If menus found, then query for products associated with those menus
			if ( !empty( $menu_ids )) { 

				//Loop through and parse out delivery dates and menu IDs for use in the query
				foreach ( $menu_ids as $delivery_date => $menu_id ) :
					if ( $order_delivery_date == '' || $order_delivery_date == $delivery_date ) {
						$menu_id_list .= $menu_id . ',';
						$delivery_date_list .= "'" . $delivery_date . "',";
					}
				endforeach;

				//Strip the trailing comma from the lists
				$menu_id_list = substr( $menu_id_list, 0, strlen($menu_id_list) - 1);
				$delivery_date_list = substr( $delivery_date_list, 0, strlen($delivery_date_list) - 1);

				$sql = "select posts.*, terms.term_id, 
						(case when ddate1.meta_value in ( " . $delivery_date_list . " ) then ddate1.meta_value else '' end) Delivery1, 
						(case when ddate2.meta_value in ( " . $delivery_date_list . " ) then ddate2.meta_value else '' end) Delivery2,
						(case when ddate1.meta_value in ( " . $delivery_date_list . " ) then enddate1.meta_value else '' end) End1, 
						(case when ddate2.meta_value in ( " . $delivery_date_list . " ) then enddate2.meta_value else '' end) End2
					from $wpdb->terms terms
					INNER JOIN $wpdb->term_taxonomy USING (term_id)
					INNER JOIN $wpdb->term_relationships  USING (term_taxonomy_id)
					INNER JOIN $wpdb->posts posts ON $wpdb->term_relationships.object_id = posts.ID
					LEFT OUTER JOIN $wpdb->termmeta ddate1 ON terms.term_id = ddate1.term_id and ddate1.meta_key = 'product-cat-delivery-date-1' 
					LEFT OUTER JOIN $wpdb->termmeta ddate2 ON terms.term_id = ddate2.term_id and ddate2.meta_key = 'product-cat-delivery-date-2'
					LEFT OUTER JOIN $wpdb->termmeta enddate1 ON terms.term_id = enddate1.term_id and enddate1.meta_key = 'product-cat-end-date-1' 
					LEFT OUTER JOIN $wpdb->termmeta enddate2 ON terms.term_id = enddate2.term_id and enddate2.meta_key = 'product-cat-end-date-2'
					WHERE posts.post_type = 'product' AND posts.post_status = 'publish' 
						AND terms.term_id in (" . $menu_id_list . ")
						AND ( ddate1.meta_value in ( " . $delivery_date_list . " ) OR ddate2.meta_value in ( " . $delivery_date_list . " ) ) 
					ORDER BY FIND_IN_SET(terms.term_id, '" . $menu_id_list . "'), posts.post_title
					";

				$products = $wpdb->get_results( $sql );		
			} //if ( !empty( $menu_ids )) 
			?>

			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

				<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

			<?php endif; ?>

			<?php do_action( 'woocommerce_archive_description' ); ?>

			<?php if ( !empty($products) ) : ?>

				<?php do_action( 'woocommerce_before_shop_loop' ); ?>
				
				<?php $delivery_date_count = 0; ?>

				<?php woocommerce_product_loop_start(); ?>

					<?php woocommerce_product_subcategories(); ?>

					<?php foreach ( $products as $post ) :
						setup_postdata($post); 
					
						//Grab 2 possible delivery datesfor current menu
						$date1 = $post->Delivery1;
						$date2 = $post->Delivery2;
            
						//echo "<input type='hidden' name='currentDate' id='currentDate' value={$date1}>";
						echo "<script>
								if(!jQuery('a[data-date=\"{$date1}\"]').hasClass('active'))
								{

								jQuery('a[data-date=\"{$date1}\"]').addClass('active');

								var prevBtn = jQuery('a[data-date=\"{$date1}\"]').prev();
								var nextBtn = jQuery('a[data-date=\"{$date1}\"]').next();

								if(prevBtn.prop('tagName') != 'A')
								{
									jQuery('span[data-direction=\"prev\"]').addClass('disabledBtn');
								}
								if(nextBtn.prop('tagName') != 'A')
								{
									jQuery('span[data-direction=\"next\"]').addClass('disabledBtn');
								}

								jQuery('a.day-unit').on('click',function(){
									var length = jQuery('a.day-unit.active').length;
									console.log(length);
									if(length >= 2)
									{
										jQuery('html, body').animate({
											scrollTop: jQuery('#'+jQuery(this).data('date')).offset().top-300
										}, 300);
										return false;
									}
								});
								

								jQuery('span.dateslider').on('click', function(){
									if(jQuery(this).data('direction') == 'next')
									{
										jQuery('a[data-date=\"{$date1}\"]').next().find('div').click();
									}
									if(jQuery(this).data('direction') == 'prev')
									{
										jQuery('a[data-date=\"{$date1}\"]').prev().find('div').click();
									}
									
								});
							}
							</script>";

						
					
						//Show Order Now button before Delivery Dates section (except if it's the first section)
						if ( $current_menu_id != $post->term_id ) {
							//if ( $current_menu_id != '' ) {
								echo foodery_order_now_button($date1);
							//} 
					
							$count = 0;
							$delivery_notes = '';
							$first_date_index = 1;
							$second_date_index = 0;

							//Only show the relevant number of order / delivery date ranges
							$ed1 = ( $post->End1 == '' ? '' : DateTime::createFromFormat("Y-m-d", $post->End1));
							$ed2 = ( $post->End2 == '' ? '' : DateTime::createFromFormat("Y-m-d", $post->End2));
							$dd1 = ( $post->Delivery1 == '' ? '' : DateTime::createFromFormat("Y-m-d", $post->Delivery1));
							$dd2 = ( $post->Delivery2 == '' ? '' : DateTime::createFromFormat("Y-m-d", $post->Delivery2));
							
							if ( ($order_delivery_date == '' || 
									$order_delivery_date == $post->Delivery1 || 
									$order_delivery_date == $post->Delivery2 ) && $delivery_date_count < $num_menus ) {
									
								//show header for Delivery Dates
								echo '<div class="hero-unit"><h2>For Delivery on ';
								
								if ( $order_delivery_date == '' ) {
									//If no order delivery date has been selected...
									//We should show either delivery date - whichever one is earlier
									if ( $post->Delivery1 != '' && $post->Delivery2 != '' ) {
										if ($post->Delivery1 < $post->Delivery2 ) {
											$first_date_index= 1;
											$second_date_index = 2;
										} else {
											$first_date_index= 2;
											$second_date_index = 1;
										}
									} elseif ( $post->Delivery1 != '' ) {
										//Only 1st date is populated so show that one
										$first_date_index= 1;
									} else {
										//Only 2nd date is populated so show that one
										$first_date_index= 2;
									}
								} else {
									//Order Delivery Date has already been selected, so show only that date
									if ( $order_delivery_date == $dd2 ) {
										$first_date_index= 2;
									} 
								}
								
								if ( $first_date_index == 1 ) {
									echo $dd1->format("D, M jS"); 
								} else {
									echo $dd2->format("D, M jS"); 
								}
								
								$delivery_notes .= 'Orders must be placed by <strong>';
								$delivery_notes .= ( $first_date_index == 1 ? $ed1->format("D, M jS") :  $ed2->format("D, M jS") ) . ' at '; 
								$delivery_notes .= $cut_off_time . '</strong> for delivery on <strong>';
								$delivery_notes .= ( $first_date_index == 1 ? $dd1->format("D, M jS") :  $dd2->format("D, M jS") ) . '</strong>.'; 
								
								//Increment total count of delivery dates shown
								$delivery_date_count++;
								
								//If a second valid delivery date is available for this menu
								//then show its order cut off info as well
								if ( $second_date_index > 0 && $delivery_date_count < $num_menus ) {
									echo " and " . ( $second_date_index == 1 ? $dd1->format("D, M jS") :  $dd2->format("D, M jS") ); 
									$delivery_notes .= "<br />";
									$delivery_notes .= 'Orders must be placed by <strong>';
									$delivery_notes .= ( $second_date_index == 1 ? $ed1->format("D, M jS") :  $ed2->format("D, M jS") ) . ' at '; 
									$delivery_notes .= $cut_off_time . '</strong> for delivery on <strong>';
									$delivery_notes .= ( $second_date_index == 1 ? $dd1->format("D, M jS") :  $dd2->format("D, M jS") ) . '</strong>.'; 
									
									//Increment total count of delivery dates shown
									$delivery_date_count++;
								}

								echo "</h2><p class='text-success'>" . $delivery_notes . "</p></div>";
							}

						} 		
						
						//Store current menu ID						
						$current_menu_id = $post->term_id;
						?>

						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endforeach; // end of looping through products.  ?>

				<?php woocommerce_product_loop_end(); ?>

				<?php do_action( 'woocommerce_after_shop_loop' ); ?>

			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

				<?php do_action( 'woocommerce_no_products_found' ); ?>

			<?php endif;

			wp_reset_postdata();
		}
	}


/*
 * Determine whether Add to Cart buttons should show
 */
function foodery_show_qty_add_to_cart() {
	
	//If ?cheat! is passed, then allow Add to Cart to show
	if ( isset($_REQUEST['cheat!']) ) {
		return true;
		
	//If Order Delivery Date has been saved, then allow items to be added to the Cart
	} elseif ( foodery_get_order_delivery_date() != '' ) {
		return true;
	
	//Allow Gift Cards to always be added to the cart
	} else {
		return ( is_page_template('page-gifts.php') or has_term( 'gift-card', 'product_cat' ) );
	}
}	

/*
 * Only show the Add to Cart link if we're allowed to show it (hidden before Delivery Date is chosen)
 */
function foodery_woocommerce_loop_add_to_cart_link( $cart_link ) {
	if ( foodery_show_qty_add_to_cart() ) {
		return $cart_link;
	} 
}
add_filter( 'woocommerce_loop_add_to_cart_link', 'foodery_woocommerce_loop_add_to_cart_link' );

/*
 * If Delivery Date is in URL then save this
 */
function foodery_set_order_delivery_date($delivery_date = '') {
	session_start();
    if($delivery_date == '')
    {
        $delivery_date = (isset($_REQUEST[FOODERY_DELIVERY_URL]) ? $_REQUEST[FOODERY_DELIVERY_URL] : '');
    }
	if ( $delivery_date != '' ) {
		$_SESSION['foodery_delivery_date'] = $delivery_date;	
	}
}
add_action( 'wp', foodery_set_order_delivery_date() );

/*
 * Clear Order Delivery Date when cart is cleared
 */
function foodery_clear_delivery_date() {

	if ( isset( $_SESSION['foodery_delivery_date'] )) {
		$_SESSION['foodery_delivery_date'] = '';
		$_SESSION['last_order_id'] = '';
	}
}
add_action( 'woocommerce_cart_emptied', 'foodery_clear_delivery_date' );
add_action( 'woocommerce_thankyou', 'foodery_clear_delivery_date' );

/*
 * Check if Order Delivery date has been set (True/False)
 */
function foodery_is_order_delivery_date_set() {
	return (isset($_SESSION['foodery_delivery_date']) && $_SESSION['foodery_delivery_date'] != '');	
}

/*
 * Get current Order Delivery Date value
 */
function foodery_get_order_delivery_date() {
	global $order;
	global $wp;
	$delivery_date = (isset($_REQUEST[FOODERY_DELIVERY_URL]) ? $_REQUEST[FOODERY_DELIVERY_URL] : '');
	
	if ( $delivery_date != '' ) {
		//Pull Delivery Date from URL if present
		$delivery_date = $_REQUEST[FOODERY_DELIVERY_URL];
		//error_log( "Pulled Delivery Date from URL");
	} elseif ( foodery_is_order_delivery_date_set() ) {
		//Pull from Session
		$delivery_date = $_SESSION['foodery_delivery_date'];
		//error_log( "Pulled Delivery Date from SESSION");
	} 

	//If still blank, pull from just completed order
	if ( $delivery_date == '' && isset($wp->query_vars['order-pay'] ) ) {
		if ($wp->query_vars['order-pay'] != '' ) {
			//error_log( "Delivery Date coming from global wp order-pay");
			$delivery_date = get_post_meta( $wp->query_vars['order-pay'], 'order-delivery-date', true );	
		}
	} 
	return $delivery_date;
}

/*
 * Retrieve "Order Now" button
 */
function foodery_order_now_button($date = "") {
	$shop_url = get_permalink( wc_get_page_id( 'shop' ) );
	return '<div class="show-order-now"><a href="' . $shop_url . '?delivery='.$date . '" class="green-button" id="'.$date.'">Start My Order</a></div>';	
}

/*
 * Display Delivery Date on Cart and Checkout pages
 */
function foodery_show_order_delivery_date() {
	//Grab delivery date for the order
	$delivery_date = foodery_get_order_delivery_date();
	if ( $delivery_date != '' ) {
		$d = DateTime::createFromFormat("Y-m-d", $delivery_date);
		$delivery_date = $d->format("D M j"); 
		echo '<p class="cart-delivery-date">Order Delivery Date: <strong>' . $delivery_date . '</strong></p>';
	}
	
}
add_action( 'woocommerce_before_cart', 'foodery_show_order_delivery_date' );
add_action( 'woocommerce_before_checkout_form', 'foodery_show_order_delivery_date' );

/*
 * Show the Cash on Delivery option on Staging (when turned on)
 */
function foodery_staging_styling() {
	if ( WP_SITEURL == 'http://thefoodery.staging.wpengine.com' ) { ?>
		<style>
			.payment_method_cod {
				display: list-item !important;
			}
		</style>			
<?php }
}
add_action( 'wp_head', 'foodery_staging_styling' );

function foodery_reminders_shortcode( $atts, $content = '' ) {
	//Get Current Delivery Date
	$delivery_date = foodery_get_order_delivery_date();
	
	//If Delivery Date is missing, pull from just completed order
	if ( $delivery_date == '' ) {
		$delivery_date = foodery_get_last_order_delivery_date();	
	}
	//Bail if delivery date still not found	
	if ( $delivery_date == '' ) {
		return;
	} else {
		$reminder_message = get_field( 'order_reminders_text', 'option' );
		return do_shortcode($reminder_message);
	}
}
add_shortcode( 'reminders', 'foodery_reminders_shortcode' );

/*
 * Retrieves Delivery Date from last completed order
 */
function foodery_get_last_order_delivery_date() {
	//Get Delivery Date
	$delivery_date = foodery_get_order_delivery_date();
	
	//If Delivery Date is missing, pull from just completed order
	if ( $delivery_date == '' ) {	
		//Get Last Order ID
		$order_id = (isset($_SESSION['last_order_id']) ? $_SESSION['last_order_id'] : '');	

		//If found, then pull its delivery date
		if ( $order_id != '' ) {
			$delivery_date = get_post_meta( $order_id, 'order-delivery-date', true );	
		}
	}
	
	return $delivery_date;
}

function foodery_delivery_date_shortcode( $atts, $content ) {
	//Get Delivery Date
	$delivery_date = foodery_get_last_order_delivery_date();
		
	//Reformat into format: Monday, January 1st
	if ( $delivery_date != '' ) {
		$d = DateTime::createFromFormat("Y-m-d", $delivery_date);
		return $d->format("l, F jS");
	}
	
}
add_shortcode( 'delivery-date', 'foodery_delivery_date_shortcode' );

/*
 * Get Order End Date (based on delivery date)
 */
function foodery_order_end_date_shortcode( $atts, $content = '' ) {
	$order_day = '';
	
	//Get Delivery Date
	$delivery_date = foodery_get_last_order_delivery_date();	
	
	//Bail if delivery date still not found
	if ( $delivery_date == '' ) {
		return;
	}
	
	//Convert into proper date format
	$d = DateTime::createFromFormat("Y-m-d", $delivery_date);
	//Get Delivery Day of Week
	$delivery_day = $d->format("N");	
	
	//Loop through Delivery Time Frames set in Options
	if( have_rows('delivery_time_frames', 'option' ) ): 
		while( have_rows('delivery_time_frames', 'option') ): the_row(); 

			//Check if delivery day of week matches
			if ( get_sub_field('deliver_on') == $delivery_day ) :
				//Get order day of week
				$order_day = get_sub_field( 'order_by' );
				break;
			endif; 

		endwhile; 
	endif; 
	reset_rows();
	
	//If our Order Day was found, then count back from devliery date
	if ( $order_day != '' ) {
		$daysback = 0;
		if ( $order_day > $delivery_day ) {
			$daysback = (7 - $order_day) + $delivery_day;
		} else {
			$daysback = $delivery_day - $order_day;
		}
		$date = date_create($delivery_date);
		date_sub($date, date_interval_create_from_date_string( $daysback . ' days'));		
		return date_format($date, 'l, F jS');
	}
		
}
add_shortcode( 'order-end-date', 'foodery_order_end_date_shortcode' );

/*
 * Get Delivery Time frame (based on delivery date)
 */
function foodery_delivery_time_frame_shortcode( $atts, $content = '' ) {
	$time_frame = '';
	
	//Get Delivery Date
	$delivery_date = foodery_get_last_order_delivery_date();	
	
	//Bail if delivery date still not found
	if ( $delivery_date == '' ) {
		return;
	}
	
	//Convert into proper date format
	$d = DateTime::createFromFormat("Y-m-d", $delivery_date);
	//Get Delivery Day of Week
	$delivery_day = $d->format("N");	
	
	//Loop through Delivery Time Frames set in Options
	if( have_rows('delivery_time_frames', 'option' ) ): 
		while( have_rows('delivery_time_frames', 'option') ): the_row(); 
		
		//Check if delivery day of week matches
		if ( get_sub_field('deliver_on') == $delivery_day ) :
			$time_frame = get_sub_field( 'delivery_time_frame' );
			break;
		endif; 
		
	endwhile; 
endif; 	
	reset_rows();
	return $time_frame;

}
add_shortcode( 'delivery-time-frame', 'foodery_delivery_time_frame_shortcode' );

function foodery_delivery_cut_off_time_shortcode( $atts = NULL, $content = '' ) {
	//Determine current cut-off time for orders
	$delivery_cut_off_time = get_field_object( 'delivery_cut_off_time', 'option' );	
	return $delivery_cut_off_time['choices'][ get_field('delivery_cut_off_time', 'option') ];
}
add_shortcode( 'cut-off-time', 'foodery_delivery_cut_off_time_shortcode' );

/**
 * Add body classes for WC pages.
 *
 * @param  array $classes
 * @return array
 */
function foodery_woocommerce_body_class( $classes ) {
	$classes = (array) $classes;

	$delivery_date = foodery_get_last_order_delivery_date();
	
	if ( $delivery_date != '' ) {

		$classes[] = 'has-delivery-date';

	} else {

		$classes[] = 'no-delivery-date';

	} 
	
	return array_unique( $classes );
}
add_filter( 'body_class', 'foodery_woocommerce_body_class' );

function foodery_woocommerce_order_details_after_order_table( $order ) {
	$has_refund = false;
			
	if ( $total_refunded = $order->get_total_refunded() ) {
		$has_refund = true;
	}
	if ( $has_refund ) { ?>
		<table>
			<tr>
				<th scope="row"><?php _e( 'Refunded:', 'woocommerce' ); ?></th>
				<td>-<?php echo wc_price( $total_refunded, array( 'currency' => $order->get_order_currency() ) ); ?></td>
			</tr>
		</table>
	<?php
	}
}
add_filter( 'woocommerce_order_details_after_order_table', 'foodery_woocommerce_order_details_after_order_table' );

function foodery_woocommerce_my_account_edit_address_title( $page_title, $load_address ) {
	if ( $page_title == 'Shipping Address' ) {
		$page_title = 'Delivery Address';
	}
	return $page_title;
}
add_filter( 'woocommerce_my_account_edit_address_title', 'foodery_woocommerce_my_account_edit_address_title', 10, 2 );

//	/**
//	 * My Account content output.
//	 */
//	function woocommerce_account_content() {
//		global $wp;
//
//		foreach ( $wp->query_vars as $key => $value ) {
//			// Ignore pagename param.
//			if ( 'pagename' === $key ) {
//				continue;
//			}
//error_log( 'woocommerce_account_' . $key . '_endpoint' );
//			if ( has_action( 'woocommerce_account_' . $key . '_endpoint' ) ) {
//				do_action( 'woocommerce_account_' . $key . '_endpoint', $value );
//				return;
//			}
//		}
//
//		// No endpoint found? Default to dashboard.
//		wc_get_template( 'myaccount/dashboard.php', array(
//			'current_user' => get_user_by( 'id', get_current_user_id() ),
//		) );
//	}
	
	
function foodery_woocommerce_account_content() { 
//	$order = wc_get_order( $order_id );
//	if ( empty( $order )) {
//		return;
//	}
		global $wp;
		$isOrdersPage = false;
		foreach ( $wp->query_vars as $key => $value ) {
			if ( 'orders' === $key || 'view-order' == $key) {
				$isOrdersPage = true;
				break;
			}			
		}	
		
		if ( !$isOrdersPage ) : ?>

			<?php 
			if ( ! wc_ship_to_billing_address_only() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) {
				$address_title = __( 'My Addresses', 'woocommerce' );
			} else {
				$address_title =  __( 'My Address', 'woocommerce' ) ;
			} ?>
			<h2><?php echo $address_title; ?></h2>
			<?php
			wc_get_template( 'myaccount/my-address.php' ); 
			wc_get_template( 'myaccount/my-shipping-notes.php' );

			wc_get_template( 'myaccount/my-downloads.php' ); 

			//$order_count = wc_get_customer_order_count( get_current_user_id() );
			$order_count = 10;
			wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); 

	endif; //if ( !$isOrdersPage ) 
	
}
add_action( 'woocommerce_account_content', 'foodery_woocommerce_account_content');


function foodery_woocommerce_my_account_my_orders_title( $title ) {
	return '<span class="my-account-my-orders-title">' . $title . '</span>';
}
add_filter('woocommerce_my_account_my_orders_title', 'foodery_woocommerce_my_account_my_orders_title');

//Remove the My Account left navigation
remove_action( 'woocommerce_account_navigation', 'woocommerce_account_navigation' );

function foodery_woocommerce_my_account_get_addresses( $address_labels ) {
	if ( isset($address_labels['shipping'] )) {
		$address_labels['shipping'] = 'Delivery Address';
	}
	return $address_labels;
	
}
add_filter( 'woocommerce_my_account_get_addresses', 'foodery_woocommerce_my_account_get_addresses');


/**
 * Adjust the Addressy field mapping so that the unit number populates in the
 * line 2 address field.
 *
 * @param array $params associative array of localized script parameters
 * @return array
 */
function sv_wc_address_validation_addressy_line_2_unit_number( $params ) {
	$params['billing'] = array(
		array( 'element' => 'billing_company',   'field' => 'Company',      'mode' => 'DEFAULT|PRESERVE' ),
		array( 'element' => 'billing_address_1', 'field' => '{StreetAddress}' ),
		array( 'element' => 'billing_address_2', 'field' => 'SubBuilding',  'mode' => 'POPULATE' ),
		array( 'element' => 'billing_city',      'field' => 'City',         'mode' => 'POPULATE' ),
		array( 'element' => 'billing_state',     'field' => 'ProvinceCode', 'mode' => 'POPULATE' ),
		array( 'element' => 'billing_postcode',  'field' => 'PostalCode',   'mode' => 'POPULATE' ),
	);
	return $params;
}
add_filter( 'wc_address_validation_addressy_addresses', 'sv_wc_address_validation_addressy_line_2_unit_number' );


