<?php

// Register Custom Post Type for Quotes
function foodery_quotes_cpt() {

	$labels = array(
		'name'                  => _x( 'Quotes', 'Post Type General Name', 'foodery' ),
		'singular_name'         => _x( 'Quote', 'Post Type Singular Name', 'foodery' ),
		'menu_name'             => __( 'Quotes', 'foodery' ),
		'name_admin_bar'        => __( 'Quotes', 'foodery' ),
		'archives'              => __( 'Quote Archives', 'foodery' ),
		'parent_item_colon'     => __( 'Parent Item:', 'foodery' ),
		'all_items'             => __( 'All Items', 'foodery' ),
		'add_new_item'          => __( 'Add New Quote', 'foodery' ),
		'add_new'               => __( 'Add New', 'foodery' ),
		'new_item'              => __( 'New Item', 'foodery' ),
		'edit_item'             => __( 'Edit Item', 'foodery' ),
		'update_item'           => __( 'Update Item', 'foodery' ),
		'view_item'             => __( 'View Item', 'foodery' ),
		'search_items'          => __( 'Search Item', 'foodery' ),
		'not_found'             => __( 'Not found', 'foodery' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'foodery' ),
		'featured_image'        => __( 'Featured Image', 'foodery' ),
		'set_featured_image'    => __( 'Set featured image', 'foodery' ),
		'remove_featured_image' => __( 'Remove featured image', 'foodery' ),
		'use_featured_image'    => __( 'Use as featured image', 'foodery' ),
		'insert_into_item'      => __( 'Insert into item', 'foodery' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'foodery' ),
		'items_list'            => __( 'Items list', 'foodery' ),
		'items_list_navigation' => __( 'Items list navigation', 'foodery' ),
		'filter_items_list'     => __( 'Filter items list', 'foodery' ),
	);
	$args = array(
		'label'                 => __( 'Quote', 'foodery' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'page-attributes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-quote',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'quotes', $args );

}
add_action( 'init', 'foodery_quotes_cpt', 0 );

/* Adds new column headers */
function foodery_quotes_add_new_columns($columns)  {
    //Optionally remove columns (can be re-added below if you want to control order)
    //unset($columns['date']);
    //unset($columns['wpseo-score']);

	$columns['sortorder'] = 'Sort Order';
	return $columns;
}  
//Sortable
add_filter('manage_edit-quotes_columns', 'foodery_quotes_add_new_columns'); 


/* Adds content to new columns */
add_action('manage_quotes_posts_custom_column', 'foodery_quotes_manage_columns', 10, 2);
function foodery_quotes_manage_columns($column_name, $post_id) {
	global $post;
	switch ($column_name) {
		case 'sortorder':
			echo $post->menu_order;
			break;
		default:
			break;
	} 
}


/* Make new columns sortable */
add_filter("manage_edit-quotes_sortable_columns", 'foodery_quotes_columns_sortable');
function foodery_quotes_columns_sortable($columns) {
	$custom = array(
		'sortorder' 		=> 'sortorder',
	);
	return wp_parse_args($custom, $columns);
}

/* Handle ordering for new columns */
add_filter( 'request', 'foodery_quotes_columns_orderby' );
function foodery_quotes_columns_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) && 'sortorder' == $vars['orderby'] ) {
		$vars = array_merge( $vars, array(
			'orderby' => 'menu_order'
		) );
	}      

	return $vars;
}