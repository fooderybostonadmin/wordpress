<?php

// shortcodes

// Gallery shortcode

// remove the standard shortcode
remove_shortcode('gallery', 'gallery_shortcode');
add_shortcode('gallery', 'gallery_shortcode_tbs');

function gallery_shortcode_tbs($attr) {
	global $post, $wp_locale;

	$output = "";

	$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID ); 
	$attachments = get_posts($args);
	if ($attachments) {
		$output = '<div class="row-fluid"><ul class="thumbnails">';
		foreach ( $attachments as $attachment ) {
			$output .= '<li class="span2">';
			$att_title = apply_filters( 'the_title' , $attachment->post_title );
			$output .= wp_get_attachment_link( $attachment->ID , 'thumbnail', true );
			$output .= '</li>';
		}
		$output .= '</ul></div>';
	}

	return $output;
}



// Buttons
function buttons( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => 'default', /* primary, default, info, success, danger, warning, inverse */
	'size' => 'default', /* mini, small, default, large */
	'url'  => '',
	'text' => '', 
	), $atts ) );
	
	if($type == "default"){
		$type = "";
	}
	else{ 
		$type = "btn-" . $type;
	}
	
	if($size == "default"){
		$size = "";
	}
	else{
		$size = "btn-" . $size;
	}
	
	$output = '<a href="' . $url . '" class="btn '. $type . ' ' . $size . '">';
	$output .= $text;
	$output .= '</a>';
	
	return $output;
}

add_shortcode('button', 'buttons'); 

// Alerts
function alerts( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => 'alert-info', /* alert-info, alert-success, alert-error */
	'close' => 'false', /* display close link */
	'text' => '', 
	), $atts ) );
	
	$output = '<div class="fade in alert alert-'. $type . '">';
	if($close == 'true') {
		$output .= '<a class="close" data-dismiss="alert">×</a>';
	}
	$output .= $text . '</div>';
	
	return $output;
}

add_shortcode('alert', 'alerts');

// Block Messages
function block_messages( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => 'alert-info', /* alert-info, alert-success, alert-error */
	'close' => 'false', /* display close link */
	'text' => '', 
	), $atts ) );
	
	$output = '<div class="fade in alert alert-block alert-'. $type . '">';
	if($close == 'true') {
		$output .= '<a class="close" data-dismiss="alert">×</a>';
	}
	$output .= '<p>' . $text . '</p></div>';
	
	return $output;
}

add_shortcode('block-message', 'block_messages'); 

// Block Messages
function blockquotes( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'float' => '', /* left, right */
	'cite' => '', /* text for cite */
	), $atts ) );
	
	$output = '<blockquote';
	if($float == 'left') {
		$output .= ' class="pull-left"';
	}
	elseif($float == 'right'){
		$output .= ' class="pull-right"';
	}
	$output .= '><p>' . $content . '</p>';
	
	if($cite){
		$output .= '<small>' . $cite . '</small>';
	}
	
	$output .= '</blockquote>';
	
	return $output;
}

add_shortcode('blockquote', 'blockquotes'); 
 


/*
 * Create [zip-lookup] shortcode to display the Zip Code lookup widget
 */
function foodery_widget_zip_code_shortcode( $atts = NULL, $content = '') {
    global $zipformcount;
    
    //Count the # of times we're showing this shortcode so that we only include the JS once
    $zipformcount++;
    ob_start();
    include(FOODERY_PARTIALS_PATH . 'widget-zip-code.php');
    $output = ob_get_clean();
    return '<div class="zip-shortcode">' . $output . '</div>';
}
add_shortcode('zip-lookup', 'foodery_widget_zip_code_shortcode');


/*
 * Shortcode to generate the title for the Newsletter section
 */
function foodery_sign_up_for_newsletter_title( $atts, $content) {
    return '<div class="sign-up-title-block"><div class="sign-up-title"></div></div>';
}
add_shortcode( 'sign-up-title', 'foodery_sign_up_for_newsletter_title');


// EMAIL ENCODE SHORTCODE
function email_encode_function( $atts, $content ){
	return '<a href="'.antispambot("mailto:".$content).'">'.antispambot($content).'</a>';
}
add_shortcode( 'email', 'email_encode_function' );


function foodery_green_button_shortcode( $atts, $content = NULL) {
    extract( shortcode_atts( array(
    'text' => '', /* button text */
    'link' => '', /* button URL/link */
    'target' => '', /* button URL/link */
    ), $atts ) );

    if ( $text != '' && $link != '' ) :
        return '<a class="green-button" href="' . trim($link) . '" ' . ($target != '' ? 'target="' . $target . '"' : '') . '>' . trim($text) . '</a>';
    endif;
}
add_shortcode( 'green-button', 'foodery_green_button_shortcode');