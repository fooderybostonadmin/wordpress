<?php
/**
 * WooCommerce File Support
 */
?>
<?php
$dates = foodery_get_current_menus();
$i     = 0;
foreach ( $dates as $date => $term_id ) :
	if ( foodery_get_order_delivery_date() == '' ) {
		if ( $i == 0 ) {
			foodery_set_order_delivery_date( $date );
			//header("Refresh:0");
		}
	}
endforeach;
?>
<?php get_header(); ?>
<div class="row-fluid">
    <div class="container">
        <div id="content" class="clearfix row-fluid site-content" role="main">

            <div id="main" class="span9 clearfix" role="main">
                <article>
					<?php
					$order_offline         = get_field( 'order_offline', 'option' );
					$at_capacity           = get_field( 'at_capacity', 'option' );
					$show_order_now_button = false;
					$order_now             = false;

					if ( is_shop() ) :

						$show_order_now_button = ( ! foodery_show_qty_add_to_cart() );
						$order_now             = isset( $_REQUEST[ FOODERY_ORDER_NOW_URL ] );
						$order_now_button      = foodery_order_now_button();
						if ( $show_order_now_button && ! $order_now ) : //&& !foodery_is_order_delivery_date_set()
							//	echo $order_now_button;
						endif; ?>

					<?php endif; ?>

                    <section class="post_content clearfix" itemprop="articleBody">
						<?php
						if ( $order_offline === true ) {
							echo 'Ordering Offline';
						} elseif ( $at_capacity === true ) {
							echo 'At Capacity';
						} else { //if ( $order_now ) {
							//Display the available Delivery Dates so user can select one
							$shop_url = get_permalink( wc_get_page_id( 'shop' ) );
							echo '<div class="delivery-date-selection select_date_wrap ">';
							echo '<div class="container">';
							echo '<div class="span9 clearfix">';
							echo '<span data-direction="prev" class="dateslider"><i class="fa fa-angle-left" aria-hidden="true"></i></span>';
							echo '<div class="delivery-date-selection-title">Choose Delivery Date</div>';
							$dates = foodery_get_current_menus();
							$i     = 0;
							foreach ( $dates as $date => $term_id ) :
								/*if(foodery_get_order_delivery_date() == ''){
									if($i == 0)
									{
										foodery_set_order_delivery_date($date);
										header("Refresh:0");
									}
								}*/
								echo '<a  href="' . $shop_url . '?' . FOODERY_DELIVERY_URL . '=' . $date . '" data-date="' . $date . '" class="day-unit">';
								$d = DateTime::createFromFormat( "Y-m-d", $date );
								echo "<div class='day'> For Delivery: ";
								echo $d->format( "l F " );
								echo '</div>' . $d->format( "jS" ) . ' ';
								//								echo '<a href="' . $shop_url . '?' . FOODERY_DELIVERY_URL . '=' . $date . '" class="green-button larger-text">Select</a>';
								echo '</a>';
								$i ++;
							endforeach;
							echo '<span data-direction="next" class="dateslider"><i class="fa fa-angle-right" aria-hidden="true"></i></span>';
							echo '</div>';
							echo '</div>';
							echo '</div>'; 
							if(!is_single()):
							?>
                            <div class="product_headline">
                                <div class="line">

                                    <div class="h1">Lunches & Dinners</div>
                                    <div class="h2">Made from Scratch In Malden, MA</div>
                                </div>
                            </div>
							<?php endif; ?>
							<?php woocommerce_content();
							//} else {
							//Delivery Date must have already been selected, so show available products
							/*if ( foodery_is_order_delivery_date_set() ) {
								//foodery_show_order_delivery_date();
								echo "<div class='delivery-date-check'>Wrong Delivery Date? <a href='#' class='wrong-delivery-date' data-clear-cart-url='" . wc_get_cart_url() . "?empty-cart'>Click here to change</a>.</div>";
							}
							woocommerce_content();*/
						}
						?>
                    </section> <!-- end article section -->

					<?php
					if ( $show_order_now_button && ! $order_now ) :  //&& !foodery_is_order_delivery_date_set()
						//echo $order_now_button;
					endif; ?>

                </article> <!-- end article -->

            </div> <!-- end #main -->

			<?php get_sidebar(); // sidebar 1 ?>

        </div> <!-- end #content -->


    </div>
</div>

<?php get_footer(); ?>
