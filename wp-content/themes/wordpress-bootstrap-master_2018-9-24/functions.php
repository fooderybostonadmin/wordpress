<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/*********************
INCLUDE FILES
*********************/
define('FOODERY_PARTIALS_PATH', dirname(__FILE__).'/partials/');
define('FOODERY_LIBRARY_PATH', dirname(__FILE__).'/library/'); 
require_once(FOODERY_LIBRARY_PATH.'gravity-forms.php');
require_once(FOODERY_LIBRARY_PATH.'cpt-quotes.php');
require_once(FOODERY_LIBRARY_PATH.'cpt-careers.php');
require_once(FOODERY_LIBRARY_PATH.'cpt-events.php');
require_once 'include/foodery.php';

/**
 *Reduce the strength requirement on the woocommerce password.
 *
 * Strength Settings
 * 3 = Strong (default)
 * 2 = Medium
 * 1 = Weak
 * 0 = Very Weak / Anything
 */
function reduce_woocommerce_min_strength_requirement( $strength ) {
    return 1;
}
add_filter( 'woocommerce_min_password_strength', 'reduce_woocommerce_min_strength_requirement' );


//allow for shortcodes in sidebar
add_filter('widget_text', 'do_shortcode');


//filtering products for product archive page so gift cards do not appear there
add_filter( 'pre_get_posts', 'foodery_pre_get_posts_query' );

function foodery_pre_get_posts_query( $query ) {

//AGiles 2/20/18 - Don't need this code as we're going to pull products in a different manner
	if ( ! is_admin() && is_shop() && $query->is_main_query() ) {
//		//Grab the term IDs for the upcoming menus
//		$menu_ids = foodery_get_current_menus();
//		//error_log(print_r($menu_ids, true));
//
//		//Grab next N menus (# menus configurable in Options)
//		$query->set( 'tax_query', array(
//			'relation' => 'AND',
//			array(
//				'taxonomy' => 'product_cat',
//				'field' => 'slug',
//				'terms' => array( 'gift-card' ), // Don't display gift cards on the main shop page
//				'operator' => 'NOT IN'
//			),
//			array(
//				'taxonomy' => 'product_cat',
//				'field'    => 'term_id',
//				'terms'    => $menu_ids,
//			),
//		));
//
//		$query->set( 'posts_per_page', -1 ) ;
//		$query->set( 'orderby', 'delivery-dates' ) ;
//		$query->set( 'order', 'ASC' ) ;
//

	}

	//Modify RSS Feed results to show 2 featured meals from homepage
	if ($query->is_feed() && $query->is_post_type_archive( 'product')  ) {
		$posts = array();

		//Determine current menu to display
		$current_menu = foodery_get_current_menus(1); //Comes back in the format $current_menu[delivery_date] = term_id;
		foreach ($current_menu as $id => $value ) :
			$current_menu_term_id = $value;
		endforeach;

		//Pull 2 meals featured on home page
		$frontpage_id = get_option( 'page_on_front' );
		if ( have_rows('featured_meals', $frontpage_id ) ):

			while( have_rows('featured_meals', $frontpage_id) ): the_row();

				$valid_delivery_date = false;

				if( $post_obj = get_sub_field('meal')):

					//Check for valid term ID
					if ( has_term( $current_menu_term_id, 'product_cat', $post_obj ) ) {
						$valid_delivery_date = true;
					}

					//If it's the valid delivery date and published, then include it in the feed
					if ($valid_delivery_date && $post_obj->post_status == 'publish') {
						$posts[] = $post_obj->ID;
					}
				endif;
			endwhile;
		endif;

		$query->set( 'post_type', 'product' ) ;
		$query->set( 'post__in', $posts);
		$query->set( 'orderby', 'post__in' );
		$query->set( 'order', 'ASC' );
	}

}


/*
 * Modify query for Menu page to order by delivery date field on the Product Category (menu)
 */
//AGiles 2/20/18 - Don't need this code as we're going to pull products in a different manner
//function foodery_orderby_tax_clauses( $clauses, $wp_query ) {
//	global $wpdb;
//
//	$orderby_arg = $wp_query->get('orderby');
//	$delivery_date = foodery_get_order_delivery_date();
//
//	if ( is_main_query() && $orderby_arg == 'delivery-dates' ) {
//
//			$clauses['join'] = "
//INNER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
//INNER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
//INNER JOIN {$wpdb->terms} USING (term_id)
//INNER JOIN {$wpdb->termmeta} ON {$wpdb->terms}.term_id = {$wpdb->termmeta}.term_id and {$wpdb->termmeta}.meta_key in ('product-cat-delivery-date-1', 'product-cat-delivery-date-2')
//			";
//			//$clauses['orderby'] = "GROUP_CONCAT({$wpdb->termmeta}.meta_value ORDER BY meta_value ASC) ASC";
//			$clauses['orderby'] = "{$wpdb->termmeta}.meta_value ASC";
//
//			if ( $delivery_date != '' ) {
//				$clauses['where'] .= " AND {$wpdb->termmeta}.meta_value = '" . $delivery_date . "' ";
//			}
//
//	}
//
//	error_log( print_r($clauses, true) );
//    return $clauses;
//}
//add_filter('posts_clauses', 'foodery_orderby_tax_clauses', 10, 2 );


// Get Bones Core Up & Running!
require_once('library/bones.php');            // core functions (don't remove)
require_once('library/plugins.php');          // plugins & extra functions (optional)

// Options panel
require_once('library/options-panel.php');

// Shortcodes
require_once('library/shortcodes.php');

// WooCommerce & Related Order Queries
require_once('library/woocommerce.php');
require_once(FOODERY_LIBRARY_PATH.'foodery-queries.php');


// Admin Functions
require_once('library/admin.php');         // custom admin functions


// Set content width
if ( ! isset( $content_width ) ) $content_width = 580;


/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 *
 * @return string Filtered title.
 *
 * @note may be called from http://example.com/wp-activate.php?key=xxx where the plugins are not loaded.
 */
function bones_filter_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'bonestheme' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'bones_filter_title', 10, 2 );


/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'wpbs-featured', 638, 300, true );  // Used in archive.php, author.php, content.php
add_image_size( 'featurette-large', 400, 400, true);
add_image_size( 'page-featured-image', 870, 410, true);  //Page & Blog header images
add_image_size( 'home-meals', 500, 500, false);   //New homepage featured meals
add_image_size( 'blog-thumbnail', 335, 335, false);   //Blog thumbnails
update_option( 'medium_size_crop', 1 );


/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/
//
// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
    register_sidebar(array(
    	'id' => 'sidebar',
    	'name' => 'Sidebar',
    	'description' => 'Used on every page BUT the homepage page template.',
    	'before_widget' => '<div id="%1$s" class="widget well %2$s">',
    	'after_widget' => '</div>',
    	'before_title' => '<h4 class="widgettitle">',
    	'after_title' => '</h4>',
    ));

	register_sidebar(array(
		'id' => 'ref',
		'name' => 'Ref',
		'description' => 'Used on every page BUT the homepage page template.',
		'before_widget' => '<div id="%1$s" class="widget well %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

    /*
    to add more sidebars or widgetized areas, just copy
    and edit the above sidebar code. In order to call
    your new sidebar just use the following code:

    Just change the name to whatever your new
    sidebar's id is, for example:

    To call the sidebar in your template, you can just copy
    the sidebar.php file and rename it to your sidebar's name.
    So using the above example, it would be:
    sidebar-sidebar2.php

    */
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<div class="comment-author vcard row-fluid clearfix">
				<div class="avatar span3">
					<?php echo get_avatar( $comment, $size='75' ); ?>
				</div>
				<div class="span9 comment-text">
					<?php printf('<h4>%s</h4>', get_comment_author_link()) ?>
					<?php edit_comment_link(__('Edit','bonestheme'),'<span class="edit-comment btn btn-small btn-info"><i class="icon-white icon-pencil"></i>','</span>') ?>

                    <?php if ($comment->comment_approved == '0') : ?>
       					<div class="alert-message success">
          				<p><?php _e('Your comment is awaiting moderation.','bonestheme') ?></p>
          				</div>
					<?php endif; ?>

                    <?php comment_text() ?>

                    <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time('F jS, Y'); ?> </a></time>

					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
			</div>
		</article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

// Display trackbacks/pings callback function
function list_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
?>
        <li id="comment-<?php comment_ID(); ?>"><i class="icon icon-share-alt"></i>&nbsp;<?php comment_author_link(); ?>
<?php

}

// Only display comments in comment count (which isn't currently displayed in wp-bootstrap, but i'm putting this in now so i don't forget to later)
add_filter('get_comments_number', 'comment_count', 0);
function comment_count( $count ) {
	if ( ! is_admin() ) {
            global $id;
            $comments = get_comments('status=approve&post_id=' . $id);
	    $comments_by_type = separate_comments( $comments );
	    return count($comments_by_type['comment']);
	} else {
	    return $count;
	}
}

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch( $form ) {
  $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
  <input type="hidden" name="post_type" value="post">
  <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search keyword, or phrase" />
  <input type="submit" id="searchsubmit" value="'. esc_attr__('Search','bonestheme') .'" />
  </form>';
  return $form;
} // don't remove this bracket!

/****************** password protected post form *****/

add_filter( 'the_password_form', 'custom_password_form' );

function custom_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<div class="clearfix"><form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
	' . '<p>' . __( "This post is password protected. To view it please enter your password below:" ,'bonestheme') . '</p>' . '
	<label for="' . $label . '">' . __( "Password:" ,'bonestheme') . ' </label><div class="input-append"><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" class="btn btn-primary" value="' . esc_attr__( "Submit",'bonestheme' ) . '" /></div>
	</form></div>
	';
	return $o;
}

/*********** update standard wp tag cloud widget so it looks better ************/

add_filter( 'widget_tag_cloud_args', 'my_widget_tag_cloud_args' );

function my_widget_tag_cloud_args( $args ) {
	$args['number'] = 20; // show less tags
	$args['largest'] = 9.75; // make largest and smallest the same - i don't like the varying font-size look
	$args['smallest'] = 9.75;
	$args['unit'] = 'px';
	return $args;
}

// filter tag cloud output so that it can be styled by CSS
function add_tag_class( $taglinks ) {
    $tags = explode('</a>', $taglinks);
    $regex = "#(.*tag-link[-])(.*)(' title.*)#e";
    $term_slug = "(get_tag($2) ? get_tag($2)->slug : get_category($2)->slug)";

        foreach( $tags as $tag ) {
        	$tagn[] = preg_replace($regex, "('$1$2 label tag-'.$term_slug.'$3')", $tag );
        }

    $taglinks = implode('</a>', $tagn);

    return $taglinks;
}

add_action( 'wp_tag_cloud', 'add_tag_class' );

add_filter( 'wp_tag_cloud','wp_tag_cloud_filter', 10, 2) ;

function wp_tag_cloud_filter( $return, $args )
{
  return '<div id="tag-cloud">' . $return . '</div>';
}

// Enable shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Disable jump in 'read more' link
function remove_more_jump_link( $link ) {
	$offset = strpos($link, '#more-');
	if ( $offset ) {
		$end = strpos( $link, '"',$offset );
	}
	if ( $end ) {
		$link = substr_replace( $link, '', $offset, $end-$offset );
	}
	return $link;
}
add_filter( 'the_content_more_link', 'remove_more_jump_link' );

// Remove height/width attributes on images so they can be responsive
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

//// Add the Meta Box to the homepage template
//function add_homepage_meta_box() {
//	global $post;
//
//	// Only add homepage meta box if template being used is the homepage template
//	// $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : "");
//	$post_id = $post->ID;
//	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
//
//	if ( $template_file == 'page-homepage.php' ){
//	    add_meta_box(
//	        'homepage_meta_box', // $id
//	        'Optional Homepage Tagline', // $title
//	        'show_homepage_meta_box', // $callback
//	        'page', // $page
//	        'normal', // $context
//	        'high'); // $priority
//    }
//}
//
//add_action( 'add_meta_boxes', 'add_homepage_meta_box' );
//
//// Field Array
//$prefix = 'custom_';
//$custom_meta_fields = array(
//    array(
//        'label'=> 'Homepage tagline area',
//        'desc'  => 'Displayed underneath page title. Only used on homepage template. HTML can be used.',
//        'id'    => $prefix.'tagline',
//        'type'  => 'textarea'
//    )
//);
//
//// The Homepage Meta Box Callback
//function show_homepage_meta_box() {
//  global $custom_meta_fields, $post;
//
//  // Use nonce for verification
//  wp_nonce_field( basename( __FILE__ ), 'wpbs_nonce' );
//
//  // Begin the field table and loop
//  echo '<table class="form-table">';
//
//  foreach ( $custom_meta_fields as $field ) {
//      // get value of this field if it exists for this post
//      $meta = get_post_meta($post->ID, $field['id'], true);
//      // begin a table row with
//      echo '<tr>
//              <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
//              <td>';
//              switch($field['type']) {
//                  // text
//                  case 'text':
//                      echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="60" />
//                          <br /><span class="description">'.$field['desc'].'</span>';
//                  break;
//
//                  // textarea
//                  case 'textarea':
//                      echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="80" rows="4">'.$meta.'</textarea>
//                          <br /><span class="description">'.$field['desc'].'</span>';
//                  break;
//              } //end switch
//      echo '</td></tr>';
//  } // end foreach
//  echo '</table>'; // end table
//}
//
//// Save the Data
//function save_homepage_meta( $post_id ) {
//
//    global $custom_meta_fields;
//
//    // verify nonce
//    if ( !isset( $_POST['wpbs_nonce'] ) || !wp_verify_nonce($_POST['wpbs_nonce'], basename(__FILE__)) )
//        return $post_id;
//
//    // check autosave
//    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
//        return $post_id;
//
//    // check permissions
//    if ( 'page' == $_POST['post_type'] ) {
//        if ( !current_user_can( 'edit_page', $post_id ) )
//            return $post_id;
//        } elseif ( !current_user_can( 'edit_post', $post_id ) ) {
//            return $post_id;
//    }
//
//    // loop through fields and save the data
//    foreach ( $custom_meta_fields as $field ) {
//        $old = get_post_meta( $post_id, $field['id'], true );
//        $new = $_POST[$field['id']];
//
//        if ($new && $new != $old) {
//            update_post_meta( $post_id, $field['id'], $new );
//        } elseif ( '' == $new && $old ) {
//            delete_post_meta( $post_id, $field['id'], $old );
//        }
//    } // end foreach
//}
//add_action( 'save_post', 'save_homepage_meta' );

// Add thumbnail class to thumbnail links
function add_class_attachment_link( $html ) {
    $postid = get_the_ID();
    $html = str_replace( '<a','<a class="thumbnail"',$html );
    return $html;
}
add_filter( 'wp_get_attachment_link', 'add_class_attachment_link', 10, 1 );


// Add lead class to first paragraph
function foodery_rss_products_desc( $excerpt ){
    global $post;

    //if ( is_feed() && is_post_type_archive( 'product' ) ) {
        $productdesc = get_post_meta($post->ID, 'product_desc', true);

        //Default to regular content if a short product description isn't specified
        if ($productdesc != '')
            $excerpt = $productdesc;
    //}

    return $excerpt;
}
add_filter( 'the_excerpt_rss', 'foodery_rss_products_desc' );

// Menu output mods
/* Bootstrap_Walker for Wordpress
     * Author: George Huger, Illuminati Karate, Inc
     * More Info: http://illuminatikarate.com/blog/bootstrap-walker-for-wordpress
     *
     * Formats a Wordpress menu to be used as a Bootstrap dropdown menu (http://getbootstrap.com).
     *
     * Specifically, it makes these changes to the normal Wordpress menu output to support Bootstrap:
     *
     *        - adds a 'dropdown' class to level-0 <li>'s which contain a dropdown
     *         - adds a 'dropdown-submenu' class to level-1 <li>'s which contain a dropdown
     *         - adds the 'dropdown-menu' class to level-1 and level-2 <ul>'s
     *
     * Supports menus up to 3 levels deep.
     *
     */
    class Bootstrap_Walker extends Walker_Nav_Menu
    {

        /* Start of the <ul>
         *
         * Note on $depth: Counterintuitively, $depth here means the "depth right before we start this menu".
         *                   So basically add one to what you'd expect it to be
         */
        function start_lvl(&$output, $depth = 0, $args = array())
        {
            $tabs = str_repeat("\t", $depth);
            // If we are about to start the first submenu, we need to give it a dropdown-menu class
            if ($depth == 0 || $depth == 1) { //really, level-1 or level-2, because $depth is misleading here (see note above)
                $output .= "\n{$tabs}<ul class=\"dropdown-menu\">\n";
            } else {
                $output .= "\n{$tabs}<ul>\n";
            }
            return;
        }

        /* End of the <ul>
         *
         * Note on $depth: Counterintuitively, $depth here means the "depth right before we start this menu".
         *                   So basically add one to what you'd expect it to be
         */
        function end_lvl(&$output, $depth = 0, $args = array())
        {
            if ($depth == 0) { // This is actually the end of the level-1 submenu ($depth is misleading here too!)

                // we don't have anything special for Bootstrap, so we'll just leave an HTML comment for now
                $output .= '<!--.dropdown-->';
            }
            $tabs = str_repeat("\t", $depth);
            $output .= "\n{$tabs}</ul>\n";
            return;
        }

        /* Output the <li> and the containing <a>
         * Note: $depth is "correct" at this level
         */
        function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
        {
            global $wp_query;
            $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
            $class_names = $value = '';
            $classes = empty( $item->classes ) ? array() : (array) $item->classes;

            /* If this item has a dropdown menu, add the 'dropdown' class for Bootstrap */
            if ($item->hasChildren) {
                $classes[] = 'dropdown';
                // level-1 menus also need the 'dropdown-submenu' class
                if($depth == 1) {
                    $classes[] = 'dropdown-submenu';
                }
            }

            /* This is the stock Wordpress code that builds the <li> with all of its attributes */
            $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
            $class_names = ' class="' . esc_attr( $class_names ) . '"';
            $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
            $item_output = $args->before;

            /* If this item has a dropdown menu, make clicking on this link toggle it */
            if ($item->hasChildren && $depth == 0) {
                $item_output .= '<a'. $attributes .' class="dropdown-toggle" data-toggle="dropdown">';
            } else {
                $item_output .= '<a'. $attributes .'>';
            }

            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

            /* Output the actual caret for the user to click on to toggle the menu */
            if ($item->hasChildren && $depth == 0) {
                $item_output .= '<b class="caret"></b></a>';
            } else {
                $item_output .= '</a>';
            }

            $item_output .= $args->after;
            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            return;
        }

        /* Close the <li>
         * Note: the <a> is already closed
         * Note 2: $depth is "correct" at this level
         */
        function end_el (&$output, $item, $depth = 0, $args = array())
        {
            $output .= '</li>';
            return;
        }

        /* Add a 'hasChildren' property to the item
         * Code from: http://wordpress.org/support/topic/how-do-i-know-if-a-menu-item-has-children-or-is-a-leaf#post-3139633
         */
        function display_element ($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
        {
            // check whether this item has children, and set $item->hasChildren accordingly
            $element->hasChildren = isset($children_elements[$element->ID]) && !empty($children_elements[$element->ID]);

            // continue with normal behavior
            return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
        }
    }
add_editor_style('editor-style.css');

// Add Twitter Bootstrap's standard 'active' class name to the active nav link item
add_filter('nav_menu_css_class', 'add_active_class', 10, 2 );

function add_active_class($classes, $item) {
	if( $item->menu_item_parent == 0 && in_array('current-menu-item', $classes) ) {
    $classes[] = "active";
	}

  return $classes;
}

// enqueue styles
if( !function_exists("theme_styles") ) {
    function theme_styles() {

		//Include multiple Google Fonts with a single call this way (https://premium.wpmudev.org/blog/custom-google-fonts/)
		$query_args = array(
			'family' => 'Lato:100,300,400,700|Roboto+Slab:300,700',
			'subset' => 'latin,latin-ext',
		);
		wp_register_style( 'google_fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );

		wp_enqueue_style( 'nexa_script_font', get_template_directory_uri() . '/library/fonts/MyFontsWebfontsKit.css', array(), null );

        // This is the compiled css file from LESS - this means you compile the LESS file locally and put it in the appropriate directory if you want to make any changes to the master bootstrap.css.
        wp_register_style( 'bootstrap', get_template_directory_uri() . '/library/css/bootstrap.css', array( 'google_fonts' ),
                filemtime(get_template_directory() . '/library/css/bootstrap.css'), 'all' );
        wp_register_style( 'bootstrap-responsive', get_template_directory_uri() . '/library/css/responsive.css', array( 'bootstrap','custom' ),
                filemtime(get_template_directory() . '/library/css/responsive.css'), 'all' );
        //There's nothing in the style.css so don't enqueue it
        //wp_register_style( 'wp-bootstrap', get_stylesheet_uri(), array(), '1.0', 'all' );
		$deps = class_exists( 'WooCommerce' ) ? array( 'bootstrap', 'woocommerce-layout', 'woocommerce-smallscreen', 'woocommerce-general' ) : array( 'bootstrap' );
        wp_register_style( 'custom', get_template_directory_uri() . '/library/css/custom.css', $deps,
                filemtime(get_template_directory() . '/library/css/custom.css'), 'all' );
    	wp_register_style( 'shame', get_template_directory_uri() . '/library/css/shame.css', array( 'custom', 'bootstrap', 'bootstrap-responsive' ),
                filemtime(get_template_directory() . '/library/css/shame.css'), 'all' );

        wp_enqueue_style( 'google_fonts' );
        wp_enqueue_style( 'bootstrap' );
        wp_enqueue_style( 'custom');


        //Landing Page
        if( is_page_template( 'page-landing-bpm.php' ))   {
            wp_register_style( 'landing-page', get_template_directory_uri() . '/library/css/landing-page.css', array( 'google_fonts' ),
                filemtime(get_template_directory() . '/library/css/landing-page.css'), 'all' );
            wp_enqueue_style( 'landing-page');
        }

        //Homepage
        if( is_page_template( 'page-homepage-v2.php' ))   {
          wp_register_style( 'homepage-v2-page', get_template_directory_uri() . '/library/css/homepage-v2.css', array('custom'),
                  filemtime(get_template_directory() . '/library/css/homepage-v2.css'), 'all' );
          wp_enqueue_style( 'homepage-v2-page');
        }

        //Blog pages
        if ( fooder_is_blog_page() ) {
            wp_register_style( 'blog', get_template_directory_uri() . '/library/css/blog.css', array( 'custom' ),
                    filemtime(get_template_directory() . '/library/css/blog.css'), 'all' );
            wp_enqueue_style( 'blog');
        }

        wp_enqueue_style( 'bootstrap-responsive' );
        wp_enqueue_style( 'shame');

        wp_enqueue_style( 'font-awesome', 'http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css', array(), null);

    }

}
add_action( 'wp_enqueue_scripts', 'theme_styles' );

function fooder_is_blog_page() {
    return ( is_home() || is_category() || is_tag() || is_singular( 'post' ) || is_search() || is_author() );
}

// acf pro upgrade to v5
if(function_exists('acf_add_options_page')) {

	acf_add_options_page();
}

// enqueue javascript
if( !function_exists( "theme_js" ) ) {
    function theme_js(){

        wp_register_script( 'bootstrap',
        get_template_directory_uri() . '/library/js/bootstrap.min.js',
        array('jquery'),
        filemtime(get_template_directory() . '/library/js/bootstrap.min.js') );

        wp_register_script( 'wpbs-scripts',
        get_template_directory_uri() . '/library/js/scripts.js',
        array('jquery'),
        filemtime(get_template_directory() . '/library/js/scripts.js') );

        wp_register_script(  'modernizr',
        get_template_directory_uri() . '/library/js/modernizr.full.min.js',
        array('jquery'),
        filemtime(get_template_directory() . '/library/js/modernizr.full.min.js') );

        wp_enqueue_script('bootstrap');
        wp_enqueue_script('wpbs-scripts');
        wp_enqueue_script('modernizr');
    }
}
add_action( 'wp_enqueue_scripts', 'theme_js' );

/*
 * Include Nexa Rustic Script font using MyFonts web kit
 */
function foodery_add_myfontswebfonts_kit() { ?>
    <script type="text/javascript">
    //uncomment and change this to false if you're having trouble with WOFFs
    var woffEnabled = false;
    //to place your webfonts in a custom directory
    //uncomment this and set it to where your webfonts are.
    var customPath = "<?php echo get_template_directory_uri(); ?>/library/fonts/webfonts/";
    </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/fonts/MyFontsWebfontsKit.js"></script>

<?php
}
//add_action( 'wp_head', 'foodery_add_myfontswebfonts_kit');

// Get theme options
function get_wpbs_theme_options(){
  $theme_options_styles = '';

      $heading_typography = of_get_option( 'heading_typography' );
      if ( $heading_typography['face'] != 'Default' ) {
        $theme_options_styles .= '
        h1, h2, h3, h4, h5, h6{
          font-family: ' . $heading_typography['face'] . ';
          font-weight: ' . $heading_typography['style'] . ';
          color: ' . $heading_typography['color'] . ';
        }';
      }

      $main_body_typography = of_get_option( 'main_body_typography' );
      if ( $main_body_typography['face'] != 'Default' ) {
        $theme_options_styles .= '
        body{
          font-family: ' . $main_body_typography['face'] . ';
          font-weight: ' . $main_body_typography['style'] . ';
          color: ' . $main_body_typography['color'] . ';
        }';
      }

      $link_color = of_get_option( 'link_color' );
      if ($link_color) {
        $theme_options_styles .= '
        a{
          color: ' . $link_color . ';
        }';
      }

      $link_hover_color = of_get_option( 'link_hover_color' );
      if ($link_hover_color) {
        $theme_options_styles .= '
        a:hover{
          color: ' . $link_hover_color . ';
        }';
      }

      $link_active_color = of_get_option( 'link_active_color' );
      if ($link_active_color) {
        $theme_options_styles .= '
        a:active{
          color: ' . $link_active_color . ';
        }';
      }

      $topbar_position = of_get_option( 'nav_position' );
      if ($topbar_position == 'scroll') {
        $theme_options_styles .= '
        .navbar{
          position: static;
        }
        body{
          padding-top: 0;
        }
        #content {
          padding-top: 27px;
        }
        '
        ;
      }

      $topbar_bg_color = of_get_option( 'top_nav_bg_color' );
      $use_gradient = of_get_option( 'showhidden_gradient' );

      if ( $topbar_bg_color && !$use_gradient ) {
        $theme_options_styles .= '
        .navbar-inner, .navbar .fill {
          background-color: '. $topbar_bg_color . ';
          background-image: none;
        }' . $topbar_bg_color;
      }

      if ( $use_gradient ) {
        $topbar_bottom_gradient_color = of_get_option( 'top_nav_bottom_gradient_color' );

        $theme_options_styles .= '
        .navbar-inner, .navbar .fill {
          background-image: -khtml-gradient(linear, left top, left bottom, from(' . $topbar_bg_color . '), to('. $topbar_bottom_gradient_color . '));
          background-image: -moz-linear-gradient(top, ' . $topbar_bg_color . ', '. $topbar_bottom_gradient_color . ');
          background-image: -ms-linear-gradient(top, ' . $topbar_bg_color . ', '. $topbar_bottom_gradient_color . ');
          background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, ' . $topbar_bg_color . '), color-stop(100%, '. $topbar_bottom_gradient_color . '));
          background-image: -webkit-linear-gradient(top, ' . $topbar_bg_color . ', '. $topbar_bottom_gradient_color . '2);
          background-image: -o-linear-gradient(top, ' . $topbar_bg_color . ', '. $topbar_bottom_gradient_color . ');
          background-image: linear-gradient(top, ' . $topbar_bg_color . ', '. $topbar_bottom_gradient_color . ');
          filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'' . $topbar_bg_color . '\', endColorstr=\''. $topbar_bottom_gradient_color . '2\', GradientType=0);
        }';
      }
      else{
      }

      $topbar_link_color = of_get_option( 'top_nav_link_color' );
      if ( $topbar_link_color ) {
        $theme_options_styles .= '
        .navbar .nav li a {
          color: '. $topbar_link_color . ';
        }';
      }

      $topbar_link_hover_color = of_get_option( 'top_nav_link_hover_color' );
      if ( $topbar_link_hover_color ) {
        $theme_options_styles .= '
        .navbar .nav li a:hover {
          color: '. $topbar_link_hover_color . ';
        }';
      }

      $topbar_dropdown_hover_bg_color = of_get_option( 'top_nav_dropdown_hover_bg' );
      if ( $topbar_dropdown_hover_bg_color ) {
        $theme_options_styles .= '
          .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover {
            background-color: ' . $topbar_dropdown_hover_bg_color . ';
          }
        ';
      }

      $topbar_dropdown_item_color = of_get_option( 'top_nav_dropdown_item' );
      if ( $topbar_dropdown_item_color ){
        $theme_options_styles .= '
          .dropdown-menu a{
            color: ' . $topbar_dropdown_item_color . ' !important;
          }
        ';
      }

      $hero_unit_bg_color = of_get_option( 'hero_unit_bg_color' );
      if ( $hero_unit_bg_color ) {
        $theme_options_styles .= '
        .hero-unit {
          background-color: '. $hero_unit_bg_color . ';
        }';
      }

      $suppress_comments_message = of_get_option( 'suppress_comments_message' );
      if ( $suppress_comments_message ){
        $theme_options_styles .= '
        #main article {
          border-bottom: none;
        }';
      }

      $additional_css = of_get_option( 'wpbs_css' );
      if( $additional_css ){
        $theme_options_styles .= $additional_css;
      }

      if( $theme_options_styles ){
        echo '<style>'
        . $theme_options_styles . '
        </style>';
      }

      $bootstrap_theme = of_get_option( 'wpbs_theme' );
      $use_theme = of_get_option( 'showhidden_themes' );

      if( $bootstrap_theme && $use_theme ){
        if( $bootstrap_theme == 'default' ){}
        else {
          echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/admin/themes/' . $bootstrap_theme . '.css">';
        }
      }
} // end get_wpbs_theme_options function


/*-----------------------------------------------------------------------------------*/
/* Remove Unwanted Admin Menu Items */
/*-----------------------------------------------------------------------------------*/

function remove_admin_menu_items() {
	$remove_menu_items = array(__('Links'),__('Comments'));
	global $menu;
	end ($menu);
	while (prev($menu)){
		$item = explode(' ',$menu[key($menu)][0]);
		if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
		unset($menu[key($menu)]);}
	}
}
// Save for last, remove what we don't need!
add_action('admin_menu', 'remove_admin_menu_items');

/**
 * Hide ACF menu item from the admin menu
 */

function hide_admin_menu()
{
    global $current_user;
    //get_currentuserinfo(); Deprecated. If needed, should be replaced by wp_get_current_user()

    $permitted_users = array('sparkdev','jeffwu','thefoodery','johnbauer','mikespeights');
    if(!in_array($current_user->user_login, $permitted_users))
    {
        echo '<style type="text/css">#toplevel_page_edit-post_type-acf,#menu-plugins,#menu-tools,#menu-plugins,#toplevel_page_cpt_main_menu,#toplevel_page_wpengine-common{display:none;}</style>';
    }
}
// Save for last, remove what we don't need!
add_action('admin_head', 'hide_admin_menu');


/**
* Preview WooCommerce Emails.
* @author WordImpress.com
* @url https://github.com/WordImpress/woocommerce-preview-emails
* If you are using a child-theme, then use get_stylesheet_directory() instead
*/

$preview = get_stylesheet_directory() . '/woocommerce/emails/woo-preview-emails.php';

if(file_exists($preview)) {
    require $preview;
}


function foodery_display_blog_header() { ?>
    <div class="row-fluid brownbg blog-header">
        <h1 class="text-center">
            <a href="<?php echo get_permalink( get_option('page_for_posts' ) ); ?>"><img src="<?php echo get_template_directory_uri();?>/library/images/simple-origins.png" alt="Simple Origins" /></a>
        </h1>
    </div>
<?php }

function foodery_display_blog_search() {
    $paged = (get_query_var('paged')); ?>

                        <div class="blog-search">
                            <div class="container">
                                <?php if ( $paged >= 2 ) : ?>
                                    <div class="span10 offset1">
                                <?php endif;

                                get_search_form();

                                if ($paged >= 2 ) : ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
    <?php
}

function foodery_custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'foodery_custom_excerpt_length', 999 );

/**
 * Setting a new cache time for feeds in WordPress
 */
function foodery_set_feed_cache_time( $seconds ) {
	return 1;
}
//add_filter( 'wp_feed_cache_transient_lifetime' , 'foodery_set_feed_cache_time' );


add_action('rss2_item', 'foodery_add_my_image_product_rss');

function foodery_add_my_image_product_rss() {
	global $post;
	if(has_post_thumbnail($post->ID)):
                echo '<media:content url="';
                the_post_thumbnail_url( 'featurette-large' );
                echo '" medium="image" width="400" height="400" />';

            //Get thumbnail ID
            $thumbnailid = get_post_thumbnail_id( $post->ID );
            //Get resized image object at 400x400
            $image_attributes = wp_get_attachment_image_src( $thumbnailid, 'featurette-large' );
            //Get full URL for this image
            $filename = $image_attributes[0];
            //Get header info for the image (to grab file size)
            $img = get_headers($image_attributes[0], 1);
            //Grab file type info (to include MIME type)
            $filetype = wp_check_filetype( $filename );

            echo '<enclosure url="' . $filename . '" type="' . $filetype['type'] . '" length="' . $img["Content-Length"] . '"  />';
	endif;

}

/*
 * Display the MailChimp Goal Tracking JS code
 */
function foodery_display_mailchimp_goal_tracking(){
    echo "\n<!-- MailChimp Automation Code -->\n";
    the_field('mailchimp_goal_tracking_code', 'option');
}

/*
 * Determine if this was a new order containing something other than Gift Certificates
 * and, if so, show the MailChimp goal tracking to trigger a New Customer "Automation" email
 */
function foodery_woocommerce_new_customer_tracking( $order_id ){
    $currentordervalid = false;
    $validordercount = 0;

    $customer_orders = get_posts( array(
        'posts_per_page' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_order_statuses() ),
        'order'       => 'ASC'
    ) );
    foreach ( $customer_orders as $post ) {
       $order = new WC_Order($post->ID);
       $hasmeals = false;

       //Loop through items on this order until we reach a non-GC order
        foreach ($order->get_items() as $key => $lineItem) {

            if ( !has_term( 'gift-card', 'product_cat', $lineItem['product_id']) ) {
                $hasmeals = true;
                break;
            }
        }
        if ($hasmeals) {
            $validordercount++;
        }

        if ( $post->ID == $order_id && $hasmeals) {
            $currentordervalid = true;
        }
    }
    if ($currentordervalid && $validordercount == 1) {
        foodery_display_mailchimp_goal_tracking();
    }
}
//add_action( 'woocommerce_thankyou', 'foodery_woocommerce_new_customer_tracking', 15 );


/*
 * Add Google AdWords tracking for orders placed
 */
function foodery_woocommerce_google_adwords_tracking( $order_id ){

    $order = new WC_Order( $order_id );
    if ( !$order->has_status( 'failed' ) ) : ?>

    <!-- Google Code for Foodery order placed Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 991030295;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "klUPCJPagWsQl9jH2AM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/991030295/?label=klUPCJPagWsQl9jH2AM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

<?php
    endif;
}
//Removed per John Bauer's email of 11/5/16
//add_action( 'woocommerce_thankyou', 'foodery_woocommerce_google_adwords_tracking', 20 );



function foodery_gtm_add() {

    // We're checking for this URL indicating this is the Order Complete page
    // ["REQUEST_URI"]=> "/checkout/order-received/1689/?key=wc_order_55c9ee479b4c9"
    $fullurl = $_SERVER["REQUEST_URI"];
    $url = explode('/',$fullurl);

    if ( $url[1] == 'checkout' && $url[2] == 'order-received') :
        $orderid = $url[3];// order id
        $order = new WC_Order( $orderid );

        if ( !$order->has_status( 'failed' ) ) :

            //Loop through to get list of coupons used
            $coupons_list = '';
            foreach( $order->get_used_coupons() as $coupon) {
                $coupons_list .=  $coupon . ', ';
            }
            //Strip last comma off list
            if ($coupons_list != '')
                $coupons_list = substr($coupons_list, 0, strlen($coupons_list) - 2);

$block = "
<script>
// Send transaction data with a pageview if available
// when the page loads. Otherwise, use an event when the transaction
// data becomes available.
dataLayer = [{
  'ecommerce': {
    'purchase': {
      'actionField': {
        'id': '" . $orderid . "',                         // Transaction ID. Required for purchases and refunds.
        'affiliation': 'Online Store',
        'revenue': '" . $order->get_total() . "',         // Total transaction value (incl. tax and shipping)
        'tax':'" . $order->get_total_tax() . "',
        'shipping': '" . $order->order_shipping . "',
        'coupon': '" . $coupons_list . "'
      },
    'products': [";

       //Loop through items on this order
        $products = '';
        foreach ($order->get_items() as $key => $lineItem) {

            // Get the product category(ies)
            $terms = get_the_terms( $lineItem['product_id'], 'product_cat' );
            $termslist = '';
            if ( ! empty( $terms ) ) {
                foreach ( $terms as $term ) {
                    $termslist .= $term->name . ', ';
                }
                //Strip last comma off list
                $termslist = substr($termslist, 0, strlen($termslist) - 2);
            }

            $products .= "
                {   // List of productFieldObjects.
                    'name': '" . $lineItem['name'] . "',     // Name or ID is required.
                    'id': '" . $lineItem['product_id'] . "',
                    'price': '" . $lineItem['line_total'] . "',
                    'category': '" . $termslist . "',
                    'quantity': " . $lineItem['qty'] . ",
               },";
        }

//Strip last comma off Products list & add to output
$block .= substr($products, 0, strlen($products) - 1);

//Close out order text
$block .= "]
    }
  }
}];
</script>
";

$block .= "
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PFSSD2');</script>
<!-- End Google Tag Manager -->
";

            //Display this code
            echo  $block;
        endif;
    endif;

}
add_action( 'wp_head', 'foodery_gtm_add', 1 );

/**
 * When we "view" an event, we want to redirect to the landing page and pass the right info
 * to populate the signup form and pass to MailChimp
 */
function foodery_special_events_redirect() {

    if( is_singular( 'foodery-events' ) && !is_admin() ) {
        $eventurl = 'code=' . urlencode( get_field('coupon_code') );
        $eventurl .= '&desc=' . urlencode( get_field('coupon_desc') );
        $eventurl .= '&source=' . urlencode( get_field('event_description') );
        $eventurl .= '&evtname=' . urlencode( get_the_title() );
        $eventurl .= '&tracking=' . urlencode( get_field('google_tracking_url') );
        wp_redirect( home_url('/event-signup/?' . $eventurl) );
        exit();
    }
}
add_action('template_redirect', 'foodery_special_events_redirect');

function foodery_add_google_remarketing_tag() { ?>

		<!-- Google Code for Remarketing Tag -->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 991030295;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
		<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/991030295/?guid=ON&amp;script=0"/>
			</div>
		</noscript>

<?php }
add_action( 'wp_footer', 'foodery_add_google_remarketing_tag', 99 );

/*
 * Automatically append the URL from which the Gravity Forms form was submitted
 */
add_filter( 'gform_notification', 'gforms_notification_add_entry_url', 10, 3 );
function gforms_notification_add_entry_url( $notification, $form, $entry ) {

    if ($notification['message_format'] == 'text')
        $notification['message'] .= "Submitted from {embed_url} on {date_mdy}";
    else
        $notification['message'] .= "<br /><br />Submitted from <a href='{embed_url}'>{embed_url}</a> on {date_mdy}";

    return $notification;
}


/**
 * Adds a new column to the "My Orders" table in the account.
 *
 * @param string[] $columns the columns in the orders table
 * @return string[] updated columns
 */
function sv_wc_add_my_account_orders_column( $columns ) {

    $new_columns = array();

    $columns['order-date'] = __( 'Order Date', 'textdomain' );
    $columns['order-actions'] = __( 'Order Details', 'textdomain' );

    foreach ( $columns as $key => $name ) {

        $new_columns[ $key ] = $name;

        // add delivery-date after order status column
        if ( 'order-date' === $key ) {
            $new_columns['order-delivery-date'] = __( 'Delivery Date', 'textdomain' );
        }
    }

    return $new_columns;
}
add_filter( 'woocommerce_my_account_my_orders_columns', 'sv_wc_add_my_account_orders_column' );

/**
 * Adds data to the custom "Delivery Date" column in "My Account > Orders".
 *
 * @param \WC_Order $order the order object for the row
 */
function sv_wc_my_orders_delivery_date_column( $order ) {
    $delivery_date = get_field('order-delivery-date', $order->id);
    echo !empty( $delivery_date ) ? date("l F d, o", strtotime($delivery_date)) : '–';
}
add_action( 'woocommerce_my_account_my_orders_column_order-delivery-date', 'sv_wc_my_orders_delivery_date_column' );

/**
 * Adds a notification to checkout and cart pages about the authorize => stripe switch
 */
function authorize_to_stripe_notification() {
    $customer_orders = get_posts( array(
        'numberposts' => 1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_order_statuses() ),
        'orderby'     => 'date'
    ) );
        if(isset($customer_orders[0]) && !empty($customer_orders[0]))
        {
            $dateTime = new DateTime($customer_orders[0]->post_date);
            if($dateTime->diff(new DateTime("2018-06-21 00:00:00"))->format('%R') == '+')
            {
                echo '<div class="alert alert-danger" role="alert">The Foodery has switched payment providers from Authorize.net to Stripe. Please re-enter your payment information. Thank you!</div>';
            }
        }
}
add_action ('woocommerce_before_checkout_form', 'authorize_to_stripe_notification');
add_action ('woocommerce_before_cart', 'authorize_to_stripe_notification');
