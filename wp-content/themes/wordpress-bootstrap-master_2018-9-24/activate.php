<?php
/*
 * Gravity Forms "User Activation" page (copied from plugins/gravityformsuserregistration/includes/activate.php
 * 
 * Customized to have the same structure as page-no-sidebar.php
 */
define( 'WP_INSTALLING', true );

global $current_site;

// include GF User Registration functionality
require_once( gf_user_registration()->get_base_path() . '/includes/signups.php' );

GFUserSignups::prep_signups_functionality();

do_action( 'activate_header' );

function do_activate_header() {
    do_action( 'activate_wp_head' );
}
add_action( 'wp_head', 'do_activate_header' );

function wpmu_activate_stylesheet() {
    ?>
    <style type="text/css">
        form { margin-top: 2em; }
        #submit, #key { width: 90%; font-size: 24px; }
        #language { margin-top: .5em; }
        .error { background: #f66; }
        span.h3 { padding: 0 8px; font-size: 1.3em; font-family: "Lucida Grande", Verdana, Arial, "Bitstream Vera Sans", sans-serif; font-weight: bold; color: #333; }
    </style>
    <?php
}
add_action( 'wp_head', 'wpmu_activate_stylesheet' );

get_header(); ?>

<div class="row-fluid">
    
    <div class="container">
        
        <div id="content" class="clearfix row-fluid">
            <div id="main" class="span12 clearfix" role="main"> 

                                                    
    <?php if ( empty($_GET['key']) && empty($_POST['key']) ) { ?>

        <h2><?php _e('Activation Key Required') ?></h2>
        <form name="activateform" id="activateform" method="post" action="<?php echo network_site_url('?page=gf_activation'); ?>">
            <p>
                <label for="key"><?php _e('Activation Key:') ?></label>
                <br /><input type="text" name="key" id="key" value="" size="50" />
            </p>
            <p class="submit">
                <input id="submit" type="submit" name="Submit" class="submit" value="<?php esc_attr_e('Activate') ?>" />
            </p>
        </form>

    <?php } else {

        $url = is_multisite() ? get_blogaddress_by_id( (int) $blog_id) : home_url('', 'http');
        if ( $url != network_home_url('', 'http') ) : 
            $loginurl = $url . 'wp-login.php';
        else:                 
            if ( empty($_GET['cart']) && empty($_POST['cart']) ) : 
                $loginurl = get_permalink( get_option('woocommerce_myaccount_page_id') );
            else : 
                //Send to checkout page which will force user to log in anyway 
                $checkout_url = wc_get_checkout_url();
                $loginurl = home_url('/my-account/?redirect=' . esc_url($checkout_url));                    
            endif;
        endif;          
                
        $key = !empty($_GET['key']) ? $_GET['key'] : $_POST['key'];
        $result = GFUserSignups::activate_signup($key);
        if ( is_wp_error($result) ) {
            if ( 'already_active' == $result->get_error_code() || 'blog_taken' == $result->get_error_code() ) {
                $signup = $result->get_error_data();             
                ?>
                <h2><?php _e('Your account is now active!'); ?></h2>
                <?php
                echo '<p class="lead-in">';
                if ( $signup->domain . $signup->path == '' ) {
                    printf( __('Your account has been activated. You may now <a href="%1$s">log in</a> to the site using your email address <strong>%2$s</strong>. Please check your email inbox for %3$s for your password and login instructions. If you do not receive an email, please check your junk or spam folder. If you still do not receive an email within an hour, you can <a href="%4$s">reset your password</a>.'), $loginurl, $signup->user_email, $signup->user_email, network_site_url( 'wp-login.php?action=lostpassword', 'login' ) );
                } else {
                    printf( __('Your site at <a href="%1$s">%2$s</a> is active. You may now log in to your site using your email address <strong>%2$s</strong>. Please check your email inbox for %4$s for your password and login instructions. If you do not receive an email, please check your junk or spam folder. If you still do not receive an email within an hour, you can <a href="%5$s">reset your password</a>.'), 'http://' . $signup->domain, $signup->domain, $signup->user_login, $signup->user_email, network_site_url( 'wp-login.php?action=lostpassword' ) );
                }
                echo '</p>';
            } else {
                ?>
                <h2><?php _e('An error occurred during the activation'); ?></h2>
                <?php
                echo '<p>'.$result->get_error_message().'</p>';
            }
        } else {
            extract($result);
            $url = is_multisite() ? get_blogaddress_by_id( (int) $blog_id) : home_url('', 'http');
            $user = new WP_User( (int) $user_id);
            ?>
            <h2><?php _e('Your account is now active!'); ?></h2>

            <div id="signup-welcome">
                <p><span class="h3"><?php _e('Username:'); ?></span> <?php echo $user->user_email ?></p>
                <p><span class="h3"><?php _e('Password:'); ?></span> <?php echo $password; ?></p>
            </div>
            
            <?php if ( $url != network_home_url('', 'http') ) : ?>
                <h4><?php printf( __('<a href="%1$s">View your site</a> or <a class="btn btn-inverse" href="%2$s">Log in</a>'), $url, $loginurl ); ?></h4>
            <?php else:                 
                if ( empty($_GET['cart']) && empty($_POST['cart']) ) : ?>
                    <h4><?php printf( __('<a class="btn btn-inverse" href="%1$s">Log in</a> or check out <a href="%2$s">this week&apos;s menu</a>.' ), $loginurl, home_url('/menu/') ); ?></h4>
                <?php else :                   
                    //Send to checkout page which will force user to log in anyway ?>
                    <h4><?php printf( __('<a class="btn btn-inverse" href="%1$s">Log in</a> to continue ordering your meals.' ), $loginurl ); ?></h4>
                <?php endif;
            endif;
        }
    }
    ?>
                
            </div> <!-- end #main -->
        </div> <!-- end #content -->  

    </div>
</div>                        
    
<script type="text/javascript">
    var key_input = document.getElementById('key');
    key_input && key_input.focus();
</script>
<?php get_footer(); ?>