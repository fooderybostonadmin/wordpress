<?php
$bkgdimg = get_field('footer_background_image', 'option');

?>
<div class="row-fluid newsletter" style="background-image:url(<?php echo $bkgdimg; ?>)">
        <div class="container">
                <div class="row">
                        <?php if ( $n = get_field('newsletter_signup_form', 'option')): ?>
                                <?php echo $n; ?>
                        <?php endif; ?>
                </div>
        </div>
</div>


<footer class="footer">

	<div class="row-fluid">
		<div class="container">
				<div class="row">
				<!-- menu -->
				<?php
					wp_nav_menu(
			    	array(
			    		'menu' => 'footer-links-homepage-v2', /* menu name */
			    		'menu_class' => 'nav inline nav-footer span12',
			    		//'theme_location' => 'footer_links', /* where in the theme it's assigned */
			    		'container' => 'false', /* container class */
			    		'fallback_cb' => 'bones_footer_links_fallback' /* menu fallback */
			    	)
				);
				?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="container">
			<div class="row">
				<div class="span12 text-center info"><a href="tel:6172074080">617-207-4080</a> <span class="pipe">|</span> <?php echo email_encode_function(NULL, 'info@fooderyboston.com'); ?><br/></div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="container">
			<div class="row">
		    <div class="span12 text-center social">
				  <div class="social-buttons" style="float:none;">
            <!--
						<a href="https://twitter.com/theFoodery"><img src="<?php echo bloginfo('template_directory');?>/library/images/twitter-v2.png" /></a>
					  <a href="https://www.facebook.com/theFoodery"><img src="<?php echo bloginfo('template_directory');?>/library/images/facebook-v2.png" /></a>
					  <a href="https://pinterest.com/thefoodery/"><img src="<?php echo bloginfo('template_directory');?>/library/images/pinterest-v2.png" /></a>
            -->
            <a href="https://twitter.com/theFoodery"><i class="fa fa-twitter"></i></a>
					  <a href="https://www.facebook.com/theFoodery"><i class="fa fa-facebook-square"></i></a>
            <a href="https://instagram.com/thefoodery/"><i class="fa fa-instagram"></i></a>
            <a href="https://pinterest.com/thefoodery/"><i class="fa fa-pinterest"></i></a>
				  </div>
				</div>
			</div>
		</div>
	</div>



	<div class="row-fluid">
		<div class="container">
			<div class="row">
				<div class="text-center copyright">Commercial kitchen: 342 Pearl Street, Malden, MA 02148</div>
			</div>
		</div>
	</div>
  <div class="row-fluid">
		<div class="container">
			<div class="row">
				<div class="text-center copyright">&copy; 2013-<?php echo date("Y") ?> The Foodery &middot; <a href="<?php echo site_url('terms-and-conditions');?>">Terms and Conditions</a></div>
			</div>
		</div>
	</div>

</footer>


		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>
