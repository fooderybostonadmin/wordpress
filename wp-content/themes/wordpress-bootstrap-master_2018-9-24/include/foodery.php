<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

function file_dump() {
    ob_start();
    foreach (func_get_args() as $arg) {
        var_dump($arg);
        echo "\n";
    }
    $str = ob_get_clean();
    $file = get_stylesheet_directory() . "/logs/logs.txt";
    $fp = fopen($file, "a+");
    fputs($fp, $str);
    fclose($fp);
}

function pre_dump() {
    echo '<pre style="background:#eee; border: solid 1px #ccc; color:#444; border-radius: 3px; padding:20px;">';
    foreach (func_get_args() as $arg) {
        var_dump($arg);
        echo "<br>";
    }
    echo '</pre>';
}

//////// manipulations on order actions ////////
add_action('woocommerce_order_status_changed', 'action_woocommerce_order_status_changed', 10, 4);
function action_woocommerce_order_status_changed($post_id, $order_before_status, $order_after_status, $instance) {
    global $wpdb;

    if ($order_after_status == 'processing') {
        $cond_1 = !get_post_meta($post_id, 'messaged_after_stripe', 1);
        $cond_2 = get_post_meta($post_id, '_payment_method', 1) == 'stripe' && get_post_meta($post_id, '_date_paid', 1) > 0;
        $cond_3 = get_post_meta($post_id, '_payment_method', 1) == '' && floatval(get_post_meta($post_id, '_order_total', 1)) == 0;

        if ( $cond_1 && ($cond_2 || $cond_3) ) {
            $wc_emails = WC()->mailer()->get_emails();
            $wc_mail = $wc_emails['WC_Email_Customer_Processing_Order'];

            $mail = $wc_mail->trigger( $post_id );
            update_post_meta($post_id, 'messaged_after_stripe', 1);
        }
    }
}