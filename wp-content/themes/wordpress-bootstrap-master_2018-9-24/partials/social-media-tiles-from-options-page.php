<?php
/*
	Partial: social media tiles
*/
?>
<?php if( have_rows('tile', 'option') ): ?>
	<div class="row-fluid">
		<div class="containerx social-media-tiles">
			<div class="row">
				<div class="span1 pad1">&nbsp;</div>

				<?php $inColumn = false; ?>
	    	<?php while( have_rows('tile', 'option') ): the_row(); ?>
					<?php $s = get_sub_field('size'); ?>
					<?php //echo 'size:'.$s; ?>
					<?php if ( $s == '1 Column'): ?>
								<div class="span2">
					<?php elseif ( $s == 'Half height'): ?>
						<?php if ( $inColumn == false): ?>
							<div class="span2 half-height">
							<?php $inColumn = true; ?>
						<?php else: ?>
							<?php $inColumn = false; ?>
						<?php endif; ?>
					<?php elseif ( $s == '2 Column'): ?>
						<div class="span4">
					<?php  endif; ?>

						<?php if( $l = get_sub_field('link')): ?>
							<a href="<?php echo $l; ?>" target="_blank">
					  <?php endif; ?>
								<div class="img-container">
		        			<img src="<?php echo get_sub_field('image'); ?>" />
									<?php if( $f = get_sub_field('title')): ?>
										<div class="title"><?php echo $f; ?></div>
									<?php endif; ?>
								</div>
						<?php if( $l): ?>
							</a>
					  <?php endif; ?>

					<?php if ( $inColumn == false): ?>
						</div>
					<?php endif; ?>
    		<?php endwhile; ?>

				<div class="span1">&nbsp;</div>
			</div>
		</div>
	</div>
<?php endif; ?>
