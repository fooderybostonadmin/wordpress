<?php
//This code requires that it be inside of the loop. It will check to see if the corresponding custom field(s) are available and will fail gracefully if they aren't there.

echo 	'<div class="row-fluid marketing">
			<div class="container">';

if(get_field('main_title')){
	echo '<div class="headline span12"><h1 class="text-center">'. get_field('main_title') .'</h1></div>';
}

echo '<div class="row">';
$rows = get_field('tiles');
if($rows)
{


	foreach($rows as $row): ?>

     <div class="span4">
        <img class=" <?php echo $row['image_style'];?>" src="<?php echo $row['image']['sizes']['thumbnail'];?>">
        <h2><?php echo $row['title'];?></h2>
        <p class="tiles-paragraph"><?php echo $row['content'];?></p>


       <?php
        if($row['button_title'] != ''){
	    $link = ($row['button_link_manual'] != '') ? $row['button_link_manual'] : $row['button_link'];
	    echo '<a href="'. $link .'" class="btn btn-success">'. $row['button_title'] .'</a>';

        }
        ?>

      </div>
	<?php endforeach;

}
echo '</div>';

if(get_field('main_button_title')){
	$tease = get_field('main_button_tease');
	$link = (get_field('main_button_manual_link') != '') ? get_field('main_button_manual_link') : get_field('main_button_link');
	echo '<div class="row call-to-action">
		<a href="'. $link .'" class="pull-right btn btn-success btn-large">'. get_field('main_button_title') .'</a>
    <span class="pull-right tease">'.$tease.'</span>
		</div>';
}

    echo '</div></div>';

/*

      <div class="row">
        <div class="span4">
          <img class="img-polaroid" data-src="holder.js/140x140">
          <h2>Food with Integrity</h2>
          <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
          <a href="#food-with-integrity">Learn more</a>
        </div><!-- /.span4 -->
        <div class="span4">
          <img class="img-polaroid" data-src="holder.js/140x140">
          <h2>Deliverd to your Door</h2>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <a href="#enjoy-convienence">Learn more</a>
        </div><!-- /.span4 -->
        <div class="span4">
          <img class="img-polaroid" data-src="holder.js/140x140">
          <h2>Good for the Earth</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <a href="#good-for-the-environment">Learn more</a>
        </div><!-- /.span4 -->
      </div><!-- /.row -->
*/
?>