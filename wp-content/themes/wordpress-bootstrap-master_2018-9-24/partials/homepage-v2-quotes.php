<?php
/*
	Partial: quotes / publications
*/
?>
	<div class="row-fluid quotes">
		<div class="container">
			
			<div class="row arrow">

			<?php if( have_rows('publications') ): ?>
				<div class="row" >
				  <div class="logos">
				  	<?php while( have_rows('publications') ): the_row(); ?>
			      	<div class="span4 logo hidden-phone">
								<img src="<?php echo get_sub_field('publication_image'); ?>" />
							</div>
							<div class="span12 logo-phone visible-phone">
								<img src="<?php echo get_sub_field('publication_image'); ?>" />
							</div>
			    	<?php endwhile; ?>
					</div>
				</div>
			<?php endif; ?>

			</div>
		</div>
	</div>
