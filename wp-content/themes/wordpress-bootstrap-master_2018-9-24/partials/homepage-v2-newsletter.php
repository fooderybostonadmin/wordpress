<?php
/*
	Partial: newsletter from sign up
*/

$bkgdimg = get_field('quotes_background');

if( have_rows('steps') ): ?>
	<div class="row-fluid newsletter" style="background-image:url(<?php echo $bkgdimg; ?>)">
		<div class="container">
			<div class="row">
				<?php if ( $n = get_field('newsletter')): ?>
					<?php echo $n; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
