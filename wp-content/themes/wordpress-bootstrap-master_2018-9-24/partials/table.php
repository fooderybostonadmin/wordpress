<?php
//This code requires that it be inside of the loop. It will check to see if the corresponding custom field(s) are available and will fail gracefully if they aren't there.

echo '<div class="row-fluid">';
echo '<div class="container">';
echo '<div class="span12">';
echo '<h1 class="text-center">'.get_field('title').'</h1>';

$rows = get_field('row');
if($rows)
{
	echo '<table class="table table-striped">';
	echo '<thead>';
	echo '<tr>';
	echo '<th>'.get_field('left_column_title').'</th>';
	echo '<th>'.get_field('right_column_title').'</th>';
	echo '</tr>';
	echo '</thead>';
	foreach($rows as $row): ?>
		<tr>
			<td><?php echo $row['left_column'];?></td>
			<td><?php echo $row['right_column'];?></td>
		</tr>
	<?php endforeach;
echo '</table>';
echo '</div> <!-- end span12 -->';
echo '</div> <!-- end container -->';
echo '</div> <!-- end row -->';
}
/*

The markup the table is based on is below
     <div class="row">
        <div class="span12">
          <h1 class="text-center">The Foodery is an Inventment Worth Making</h1>
        <table class="table table-striped">
            <thead>
              <tr>
                <th>The Foodery</th>
                <th>Eating Out and Cooking at Home</th>
              </tr>
            </thead>
            <tr>
              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat, at.</td>
              <td>Adipisci labore veniam unde eum dolore magnam ad cum accusamus!</td>
            </tr>
            <tr>
              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, facilis.</td>
              <td>Sint suscipit omnis totam perspiciatis aliquam odio iusto necessitatibus. Totam.</td>
            </tr>
            <tr>
              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse, quisquam.</td>
              <td>At cum molestias expedita voluptates delectus velit ab rerum tempore.</td>
            </tr>
            <tr>
              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, quaerat.</td>
              <td>Neque, modi ea repudiandae eveniet est? Pariatur, voluptatem excepturi saepe.</td>
            </tr>
            <tr>
              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, facere.</td>
              <td>Sapiente, numquam, atque sunt ratione neque ea omnis aperiam harum.</td>
            </tr>
         </table>
        </div>
      </div>

*/
