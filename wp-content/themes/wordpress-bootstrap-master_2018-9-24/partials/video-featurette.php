<div class="row-fluid video-featurette">
    <div class="container">
        <div class="col-md-8">
        	<?php if(get_field('video_feature_use_video')):?>
            <iframe width="560" height="315" src="//www.youtube.com/embed/<?php echo the_field('video_feature_video_id');?>" frameborder="0" allowfullscreen></iframe>
            <?php else:?>
            	<a href="<?php echo the_field('video_feature_image_link');?>" target="_blank"><img src="<?php echo the_field('video_feature_image');?>"></a>
            <?php endif;?>
        </div>
        <div class="col-md-4 video-feature-content">
        	<?php if(get_field('video_feature_content_image')):?>
            <div class="col-md-12 col-sm-6">
            <?php if(get_field('video_feature_content_image_link')):?>
            	<a href="<?php echo the_field('video_feature_content_image_link');?>" target="_blank">
            <?php endif;?>
                <img src="<?php echo the_field('video_feature_content_image');?>" class="video-feature-badge">
            <?php if(get_field('video_feature_content_image_link')):?>
            	</a>
            <?php endif;?>
            </div>
            <?php endif;?>
            <div class="col-md-12 col-sm-6">
                <h2><?php echo the_field('video_feature_content_title');?></h2>
                <p class="tiles-paragraph"><?php echo the_field('video_feature_content_content');?></p>
                <a class="btn btn-success" href="<?php echo the_field('video_feature_button_link');?>" target="_blank"><?php echo the_field('video_feature_button_title');?></a>
            </div>
        </div>
    </div>
</div>
<?php if(get_field('video_feature_use_video')):?>
<script>
	//responsive video

(function ( window, document, undefined ) {

// Grab all iframes on the page or return
var iframes = document.getElementsByTagName( 'iframe' );

// Loop through the iframes array

for ( var i = 0; i < iframes.length; i++ ) {

var iframe = iframes[i],

/*
   * RegExp, extend this if you need more players
   */
players = /www.youtube.com|player.vimeo.com/;

/*
 * If the RegExp pattern exists within the current iframe
 */
if ( iframe.src.search( players ) > 0 ) {

  /*
   * Calculate the video ratio based on the iframe's w/h dimensions
   */
  var videoRatio        = ( iframe.height / iframe.width ) * 100;

  /*
   * Replace the iframe's dimensions and position
   * the iframe absolute, this is the trick to emulate
   * the video ratio
   */
  iframe.style.position = 'absolute';
  iframe.style.top      = '0';
  iframe.style.left     = '0';
  iframe.width          = '100%';
  iframe.height         = '100%';

  /*
   * Wrap the iframe in a new <div> which uses a
   * dynamically fetched padding-top property based
   * on the video's w/h dimensions
   */
  var wrap              = document.createElement( 'div' );
  wrap.className        = 'fluid-vids';
  wrap.style.width      = '100%';
  wrap.style.position   = 'relative';
  wrap.style.paddingTop = videoRatio + '%';

  /*
   * Add the iframe inside our newly created <div>
   */
  var iframeParent      = iframe.parentNode;
  iframeParent.insertBefore( wrap, iframe );
  wrap.appendChild( iframe );

}
}

})( window, document );

//end responsive video
</script>
<?php endif;?>