<?php get_header(); 

foodery_display_blog_header(); ?>

        <div id="content" class="clearfix row-fluid">

            <?php foodery_display_blog_search(); ?>

                <div id="main" class="span12 clearfix" role="main">

                    <div class="blog-posts-list">
                        <div class="container">

                            <div class="page-header"><h1><span><?php _e("Search Results for","bonestheme"); ?>:</span> <?php echo esc_attr(get_search_query()); ?></h1></div>

                            <?php if (have_posts()) : while (have_posts()) : the_post(); 
                                $excerpt = get_the_excerpt();
                                $hasphoto = has_post_thumbnail(); ?>
                        
                                <article <?php post_class('blog-normal span10 offset1 clearfix'); ?>>
                                    <div class="row-fluid">
                                        <?php if ($hasphoto) : ?>
                                        <div class="span5">
                                            <?php the_post_thumbnail( 'medium', array('class' => '') ); ?> 
                                        </div>
                                        <?php endif; ?>
                                        <div class="<?php echo ($hasphoto ? 'span7' : 'span12'); ?>">
                                            <h1 class="blog-title"><a href="<?php  echo get_permalink(); ?>"><?php the_title(); ?></a></h1>
                                            <div class="blog-meta"><?php _e("By", "foodery"); ?> <?php the_author_posts_link(); ?></div>
                                            <div class="blog-excerpt"><?php echo $excerpt; ?></div>					                            
                                        </div>
                                    </div>
                                </article>                               

                            <?php endwhile; ?>

                            <div class="blog-navigation span10 offset1">
                            <?php if (function_exists('page_navi')) { // if expirimental feature is active ?>

                                    <?php page_navi(); // use the page navi function ?>

                            <?php } else { // if it is disabled, display regular wp prev & next links ?>
                                    <nav class="wp-prev-next">
                                            <ul class="clearfix">
                                                    <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
                                                    <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
                                            </ul>
                                    </nav>
                            <?php } ?>
                            </div>

                            <?php else : ?>

                            <!-- this area shows up if there are no results -->

                            <article id="post-not-found">
                                <header>
                                    <h1><?php _e("Not Found", "bonestheme"); ?></h1>
                                </header>
                                <section class="post_content">
                                    <p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
                                </section>
                                <footer>
                                </footer>
                            </article>

                            <?php endif; ?>

                    </div> <!-- end #main -->

                    </div>

            </div> <!-- end #content -->

<?php get_footer(); ?>
