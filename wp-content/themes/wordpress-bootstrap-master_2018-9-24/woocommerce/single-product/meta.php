<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; 
}

global $post, $product;
?>
<div class="product_meta">

        <?php do_action( 'woocommerce_product_meta_start' ); ?>

        <div class="product-meta-individual">
        <?php 
            $size = sizeof( get_the_terms( $post->ID, 'portion' ) );
            $terms = get_the_terms( $post->ID, 'portion' );

                if ( $terms && ! is_wp_error( $terms ) ) {

                        $portion_links = array();

                        foreach ( $terms as $term ) {
                                $portion_links[] = $term->name;
                        }

                        $portion = join( ", ", $portion_links );

                        echo '<span>'.$portion.'</span>';
                };?>	

        </div>	

	<div class="product-meta-individual">
            <?php 
                $size = sizeof( get_the_terms( $post->ID, 'allergen' ) );
                $terms = get_the_terms( $post->ID, 'allergen' );

                if ( $terms && ! is_wp_error( $terms ) ) {

                        $allergen_links = array();

                        foreach ( $terms as $term ) {
                                $allergen_links[] = $term->name;
                        }

                        $allergen = join( ", ", $allergen_links );

                        echo 'Allergens: <span>'.$allergen.'</span>';
                };?>	

        </div>

        <?php do_action( 'woocommerce_product_meta_end' ); ?>
</div>
