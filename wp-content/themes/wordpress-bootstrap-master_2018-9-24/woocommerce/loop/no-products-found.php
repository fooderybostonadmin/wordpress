<?php
/**
 * Displayed when no products are found matching the current query.
 *
 * Override this template by copying it to yourtheme/woocommerce/loop/no-products-found.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $woocommerce;
?>
<p class="woocommerce-info">
	<?php
	
	//Get Delivery Date
	$delivery_date = foodery_get_order_delivery_date();
	
	//If Delivery Date is present but menu items are missing, prompt to choose a new delivery date
	if ( $delivery_date != '' ) : ?>	
		Oops, we weren't able to locate any menu items for your current delivery date. Please 
		<a href='<?php echo home_url(); ?>/menu/?order-now'>select a new delivery date</a>. 
		If you continue to run into this problem, please <a href='<?php echo home_url(); ?>/faqs/'>contact us</a>.
	<?php else : ?>								
		No menu items were found. Don't Worry! We may be in the process of changing over our menu for the next week. 
		Please check back shortly.
	<?php endif; ?>
</p>
