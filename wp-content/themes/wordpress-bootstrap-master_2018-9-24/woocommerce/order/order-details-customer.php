<?php
/**
 * Order Customer Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-customer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$show_shipping = ! wc_ship_to_billing_address_only() && $order->needs_shipping_address();
?>
<section class="woocommerce-customer-details">

	<h2><?php _e( 'Customer Details', 'woocommerce' ); ?></h2>

	<table class="woocommerce-table woocommerce-table--customer-details shop_table customer_details">

		<?php if ( $order->get_customer_note() ) : ?>
		<tr>
			<th><?php _e( 'Note:', 'woocommerce' ); ?></th>
				<td><?php echo wptexturize( $order->get_customer_note() ); ?></td>
		</tr>
	<?php endif; ?>

		<?php if ( $order->get_billing_email() ) : ?>
		<tr>
			<th><?php _e( 'Email:', 'woocommerce' ); ?></th>
				<td><?php echo esc_html( $order->get_billing_email() ); ?></td>
		</tr>
	<?php endif; ?>

		<?php if ( $order->get_billing_phone() ) : ?>
		<tr>
				<th><?php _e( 'Phone:', 'woocommerce' ); ?></th>
				<td><?php echo esc_html( $order->get_billing_phone() ); ?></td>
		</tr>
	<?php endif; ?>

	<?php do_action( 'woocommerce_order_details_after_customer_details', $order ); ?>

</table>

<?php if ( $show_shipping ) : ?>

	<section class="woocommerce-columns woocommerce-columns--2 woocommerce-columns--addresses col2-set addresses">

		<div class="woocommerce-column woocommerce-column--1 woocommerce-column--billing-address col-1">

<?php endif; ?>

			<h3 class="woocommerce-column__title"><?php _e( 'Billing address', 'woocommerce' ); ?></h3>

<address>
	<?php echo ( $address = $order->get_formatted_billing_address() ) ? $address : __( 'N/A', 'woocommerce' ); ?>
	<?php 
				 /*echo "<br>";
				 echo $order->get_billing_first_name() != "" ? "</br><strong>First Name: </strong>".$order->get_billing_first_name()  : "";
				 echo $order->get_billing_last_name() != "" ? "</br><strong>Last Name: </strong>".$order->get_billing_last_name()  : "";
				 echo $order->get_billing_company() != "" ? "</br><strong>Company: </strong>".$order->get_billing_company()  : "";
				 echo $order->get_billing_address_1() != "" ? "</br><strong>Address 1: </strong>".$order->get_billing_address_1()  : "";
				 echo $order->get_billing_address_2() != "" ? "</br><strong>Address 2: </strong>".$order->get_billing_address_2()  : "";
				 echo $order->get_billing_city() != "" ? "</br><strong>City: </strong>".$order->get_billing_city()  : "";
				 echo $order->get_billing_state() != "" ? "</br><strong>State: </strong>".$order->get_billing_state()  : "";
				 echo $order->get_billing_postcode() != "" ? "</br><strong>PostCode: </strong>".$order->get_billing_postcode()  : "";
				 echo $order->get_billing_country() != "" ? "</br><strong>Country: </strong>".$order->get_shipping_country()  : "";*/
			?>
</address>

<?php if ( $show_shipping ) : ?>

	</div><!-- /.col-1 -->

		<div class="woocommerce-column woocommerce-column--2 woocommerce-column--shipping-address col-2">

			<h3 class="woocommerce-column__title"><?php _e( 'Delivery address', 'woocommerce' ); ?></h3>

		<address>
			<?php echo $order->get_formatted_shipping_address(); ?>
			<?php //echo $shipping_address; ?>
			<?php //echo '<br />' . esc_html( $order->get_meta( '_shipping_phone' ) ); ?>
			<?php //echo '<br />' . esc_html( $order->get_meta( '_shipping_email' ) ); ?>
			<?php 
				/* echo "<br>";
				 echo $order->get_shipping_first_name() != "" ? "</br><strong>First Name: </strong>".$order->get_shipping_first_name()  : "";
				 echo $order->get_shipping_last_name() != "" ? "</br><strong>Last Name: </strong>".$order->get_shipping_last_name()  : "";
				 echo $order->get_shipping_company() != "" ? "</br><strong>Company: </strong>".$order->get_shipping_company()  : "";
				 echo $order->get_shipping_address_1() != "" ? "</br><strong>Address 1: </strong>".$order->get_shipping_address_1()  : "";
				 echo $order->get_shipping_address_2() != "" ? "</br><strong>Address 2: </strong>".$order->get_shipping_address_2()  : "";
				 echo $order->get_shipping_city() != "" ? "</br><strong>City: </strong>".$order->get_shipping_city()  : "";
				 echo $order->get_shipping_state() != "" ? "</br><strong>State: </strong>".$order->get_shipping_state()  : "";
				 echo $order->get_shipping_postcode() != "" ? "</br><strong>PostCode: </strong>".$order->get_shipping_postcode()  : "";
				 echo $order->get_shipping_country() != "" ? "</br><strong>Country: </strong>".$order->get_shipping_country()  : "";*/
			?>
		</address>

	</div><!-- /.col-2 -->

	</section><!-- /.col2-set -->

<?php endif; ?>

</section>
