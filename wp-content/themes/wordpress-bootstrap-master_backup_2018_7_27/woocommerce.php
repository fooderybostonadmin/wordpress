<?php
/**
 * WooCommerce File Support
 */
?>

<?php get_header(); ?>
			<div class="row-fluid">
				<div class="container">
					<div id="content" class="clearfix row-fluid site-content"role="main">

						<div id="main" class="span9 clearfix" role="main">
							<article> 
								<?php
								$order_offline = get_field('order_offline','option');
								$at_capacity = get_field('at_capacity','option');
								$show_order_now_button = false;
								$order_now = false;
								
								if( is_shop() ) : 
									
									$show_order_now_button = (! foodery_show_qty_add_to_cart());
									$order_now = isset($_REQUEST[FOODERY_ORDER_NOW_URL]);
									$order_now_button = foodery_order_now_button();
									if ( $show_order_now_button && !$order_now ) : //&& !foodery_is_order_delivery_date_set()
										echo $order_now_button;
									endif; ?>
									
								<?php endif;?>
								
  								<section class="post_content clearfix" itemprop="articleBody">
  								<?php
									if ( $order_offline === true){
										echo 'Ordering Offline';
									} elseif ($at_capacity === true){
										echo 'At Capacity';
									} else if ( $order_now ) {
										//Display the available Delivery Dates so user can select one
										$shop_url = get_permalink( wc_get_page_id( 'shop' ) );
										echo '<div class="delivery-date-selection">';
										echo '<div class="delivery-date-selection-title">Choose Delivery Date</div>';
										$dates = foodery_get_current_menus();
										foreach ( $dates as $date => $term_id ) :
											echo '<div class="span6"><div class="hero-unit">';
											$d = DateTime::createFromFormat("Y-m-d", $date);
											echo '<h2>' . $d->format("l") . '</h2>';
											echo '<h2>' . $d->format("M j") . '</h2>';
											echo '<a href="' . $shop_url . '?' . FOODERY_DELIVERY_URL . '=' . $date . '" class="green-button larger-text">Select</a>';
											echo '</div></div>';
										endforeach;
										echo '</div>';
									} else {
										//Delivery Date must have already been selected, so show available products
										if ( foodery_is_order_delivery_date_set() ) {
											//foodery_show_order_delivery_date();
											echo "<div class='delivery-date-check'>Wrong Delivery Date? <a href='#' class='wrong-delivery-date' data-clear-cart-url='"  . wc_get_cart_url() . "?empty-cart'>Click here to change</a>.</div>";
										}
										woocommerce_content();
									}
								?>
								</section> <!-- end article section -->
								
								<?php 
								if ( $show_order_now_button && !$order_now ) :  //&& !foodery_is_order_delivery_date_set()
									echo $order_now_button;
								endif; ?>								

							</article> <!-- end article -->

						</div> <!-- end #main -->

						<?php get_sidebar(); // sidebar 1 ?>

					</div> <!-- end #content -->


				</div>
			</div>

<?php get_footer(); ?>
