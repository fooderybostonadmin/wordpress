<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php get_template_part('partials/home-carousel');?>
					<?php get_template_part('partials/tres-tiles');?>
					<?php get_template_part('partials/featurette');?>



					<?php endwhile; ?>

					<?php else : ?>

					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>

					<?php endif; ?>

<?php get_footer(); ?>