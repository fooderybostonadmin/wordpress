<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

  $notes = get_user_meta(get_current_user_id(), 'shipping-notes-note', true);  
  ?>
	<div class="addresses">
		<header class="title">
			<h3>Delivery Instructions</h3>
			<a href="<?php echo wc_get_endpoint_url( 'edit-shipping-notes'); ?>" class="edit" style="float:right"><?php _e( 'Edit', 'woocommerce' ); ?></a>
		</header>
	
	<?php
		if ( empty( $notes ))
			_e( 'You have not set up any delivery instructions.', 'woocommerce' );
		else
			echo $notes;
	?>
	
	</div>
