				<div id="sidebar1" class="fluid-sidebar sidebar span3" role="complementary" >
					<?php if (!is_page( 'checkout' ) && !is_user_logged_in()) : ?>

					<?php include_once('partials/widget-zip-code.php');?>
					<div class="widget well cart-well">
						<div id="cart-well-logo">
						      	<img src="<?php echo bloginfo( 'template_directory' );?>/library/images/foodery_logo.png" width="450" height="50" alt="The Foodery" />
						</div>	
					</div>
					<?php endif;?>
					<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>

						<?php dynamic_sidebar( 'sidebar' ); ?>

					<?php else : ?>

						<!-- No Widgets -->


					<?php endif; ?>


				</div>