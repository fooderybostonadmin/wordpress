jQuery(document).ready(function($){
  var isBrowserCompatible = 
    $('html').hasClass('ua-ie-10') ||
    $('html').hasClass('ua-webkit') ||
    $('html').hasClass('ua-firefox') ||
    $('html').hasClass('ua-opera') ||
    $('html').hasClass('ua-chrome');

  if(isBrowserCompatible){
    window.card = new Skeuocard($(".payment_method_authorize_net_cim"), {
      debug: true
    });
  }
});