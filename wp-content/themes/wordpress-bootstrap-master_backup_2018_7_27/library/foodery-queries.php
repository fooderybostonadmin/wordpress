<?php
/*
 * Foodery Queries screen
 */


/*
 * Add 'Foodery Queries' admin page to WP Admin Menu
 */
function foodery_add_admin_pages() {
    add_menu_page('Foodery Queries', 'Foodery Queries', 'manage_options', 'foodery-queries', 'foodery_queries_admin_page' );
}
add_action('admin_menu', 'foodery_add_admin_pages');


/*
 * Define content for 'Foodery Queries' admin page
 */
function foodery_queries_admin_page() { ?>
    <style>
        .query-notes {
            margin: 0 0 25px;
        }
        .query-notes .note { 
            font-weight: bold;
            font-style: italic;
        }
    </style>
    <script>
    // as the page loads, call these scripts
    jQuery(document).ready(function($) {
        
        //Make sure when select changes, we modify criteria shown
        $( "#query-select-form .dropdown.queries" ).change(function() {
            showHideNotesCriteria();
        }); 
        
        //Show default notes & criteria
        showHideNotesCriteria();
    }); 
    function showHideNotesCriteria() {
        //Hide all query notes and criteria to start
        jQuery('#query-select-form .query-notes .note').hide();
        jQuery('#query-select-form .query-filters .qfilter').hide();
        
        //Get query name
        var rptname = jQuery('#query-select-form .dropdown.queries').val();
        
        //For some reason, this method wasn't working, so replaced with switch statement
        //jQuery('#query-select-form .query-notes .note').hasClass(rptname).show();
        //jQuery('#query-select-form .query-filters .filter').hasClass(rptname).show();
        
        switch (rptname) {
            case 'new-orders':
                jQuery('#query-select-form .new-orders').show();
                break;
            case 'newer-orders':
                jQuery('#query-select-form .newer-orders').show();
                break;
            case 'already-ordered':
                jQuery('#query-select-form .already-ordered').show();
                break;
            case 'shipping-notes':
                jQuery('#query-select-form .shipping-notes').show();
                break;
            case 'first-order':
                jQuery('#query-select-form .first-order').show();
                break;
//            default:
//                break;
        }

        
    }
    </script>
    
	<!-- Put any HTML content here -->
        <div class="wrap">
            <h2>Queries Dashboard</h2>

            <div style="padding: 20px 0;">
                <form id="query-select-form" action="<?php echo home_url(); ?>/query-output/" method="POST" target="_blank">
                    <div class='select-query' style="padding: 0 0 20px;">Choose Query:<br />
                        <select class='dropdown queries' name='queries'>
                            <option value='newer-orders' selected='selected'>New Orders</option>
                            <option value='new-orders'>New Orders (Old)</option>
                            <option value='already-ordered'>Already Ordered MailChimp Semgent</option>
                            <option value='shipping-notes'>Shipping Notes</option>
                            <option value='first-order'>New Customers / First Order</option>
                        </select>
                    </div>

                    <div class="query-notes">
                        <div class="note newer-orders">Pulls full order information for customers who ordered for a specific delivery date (CSV)</div>
                        <div class="note new-orders">Pulls full order information for customers who ordered within a given date range (CSV)</div>
                        <div class="note already-ordered">Pulls emails only of customers who ordered within a given date range. It is meant to be used to exclude customers from MailChimp order reminders. (Plain Text)</div>
                        <div class="note shipping-notes">Pulls customer emails and most recent delivery note. This note also included in New Orders file. (CSV)</div>
                        <div class="note first-order">Pulls a list of all customers along with their earlier order # and order date. (CSV)</div>
                    </div>
                    
					<?php
					$day_of_week = date('N');
					?>
                    <div class="query-filters">
                        <div class="qfilter new-orders already-ordered">
                            <strong>Order Dates:</strong><br />
                            <blockquote>
                                <?php
                                if ($day_of_week  > 4)
                                    $daysback = $day_of_week - 4;
                                else
                                    $daysback = $day_of_week + 3;
                                ?>
								<label for="start-date">Start Date/Time : </label><input type="date" name="start-date" value="<?php echo date('Y-m-d', strtotime("-" . $daysback . " days")); ?>" />
								<?php foodery_time_dropdown('start-time', ''); ?><br />
								<label for="end-date">End Date/Time: </label><input type="date" name="end-date" value="<?php echo date('Y-m-d'); ?>" />
								<?php foodery_time_dropdown('end-time',''); ?><br />
                            </blockquote>
                        </div>
                        <div class="qfilter newer-orders">
                            <blockquote>
                                <?php
                                if ($day_of_week  < 3 )
                                    $delivery = 2;
                                else
                                    $delivery = 3;
                                ?>
	                            <label for="delivery-date">Delivery Date : </label><input type="date" name="delivery-date" value="<?php echo date('Y-m-d', strtotime("+" . $delivery . " days")); ?>" />
                            </blockquote>
                        </div>						
                        <div class="qfilter newer-orders new-orders already-ordered">
                            <strong>Order Statuses to include:</strong><br />
                            <blockquote>
                                <table border="0" cellspacing="5">
                                    <?php
                                    //Get array of WooCommerce statuses
                                    $woostatuses = wc_get_order_statuses(); 
                                    $count = 0;
                                    foreach ($woostatuses as $slug => $name) : 
                                        $count++;
                                        //Default to all statuses for now
                                        //$selected = ($slug == 'wc-processing' || $slug == 'wc-completed'); 
                                        $selected = true; 

                                        if ($count % 2 == 1) : ?>
                                            <tr>
                                        <?php endif; ?>

                                            <td><input type="checkbox" name="order-status[]" value="<?php echo $slug; ?>"
                                                 <?php echo ($selected ? 'checked="checked"' : ''); ?>> <?php echo $name; ?></td>

                                        <?php if ($count % 2 == 0) : ?>
                                            </tr>
                                        <?php endif;                            

                                     endforeach; 

                                    if ($count % 2 == 1) : ?>
                                        </tr>
                                    <?php endif; ?>                              
                                  </table>
                            </blockquote>
                        </div>
                    </div>
                    
                    <input type="submit" id="export-query" name="export" value="Run Query" />
                </form>

            </div>

        </div><!-- .wrap -->
<?php
}

/*
 * Process query request from 'Foodery Queries' page
 */
function foodery_queries_processing( ) {
    global $wpdb;
    $sql = '';
    $error = '';
    $format = 'csv';

    if ( $_POST['export'] != 'Run Query') {
        return;
    }
   
    //Determine which query is being run
    $queryname = $_POST['queries'];

    //Get array of WooCommerce statuses
//    $woostatuslist = '';
//    $woostatuses = wc_get_order_statuses();
//    foreach ($woostatuses as $slug => $name) :
//        $woostatuslist .= "'" . $slug . "',";
//    endforeach;
//    $woostatuslist = substr( $woostatuslist, 0, strlen($woostatuslist) - 1);
    
    $orderstatus = '';
    if(!empty($_POST['order-status'])) {
        foreach($_POST['order-status'] as $status) {
            $orderstatus .= '"' . $status . '",';
        }
        //Strip last comma off
        $orderstatus = substr( $orderstatus, 0, strlen($orderstatus) - 1);
    }    
    

    //Generate the SQL statement for the chosen query
    switch($queryname) {
        case 'newer-orders':
            
            if ( $orderstatus == '' || $_POST['delivery-date'] == '' ) {
                $error = 'Query is missing needed criteria. Please correct and then resubmit.';
                break;
            }

            //Update group_concat limit, otherwise text gets cut off on Order Items listing for folks who order a lot
            $wpdb->query( 'SET SESSION group_concat_max_len = 10000000' );     
            
            //Generate the query
            $sql = "select p.ID as 'Order ID',
                    DATE_FORMAT(max(p.post_date), '%b %e %Y,  %h:%i:%s %p') as 'Order Date',
                    max( CASE WHEN pm.meta_key = '_billing_first_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Billing First Name',
                    max( CASE WHEN pm.meta_key = '_billing_last_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Billing Last Name',
                    (case when max( CASE WHEN pm.meta_key = '_shipping_email' and p.ID = pm.post_id THEN pm.meta_value END ) is NULL
                    then max( CASE WHEN pm.meta_key = '_billing_email' and p.ID = pm.post_id THEN pm.meta_value END )
                    else max( CASE WHEN pm.meta_key = '_shipping_email' and p.ID = pm.post_id THEN pm.meta_value END ) END ) as 'Billing E-mail ID',
                    (case when max( CASE WHEN pm.meta_key = '_shipping_phone' and p.ID = pm.post_id THEN pm.meta_value END ) is NULL
                    then max( CASE WHEN pm.meta_key = '_billing_phone' and p.ID = pm.post_id THEN pm.meta_value END )
                    else max( CASE WHEN pm.meta_key = '_shipping_phone' and p.ID = pm.post_id THEN pm.meta_value END ) END) as 'Billing Phone Number',

                    max( CASE WHEN pm.meta_key = '_order_shipping' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Shipping',
                    '' as 'Order Discount', 
                    max( CASE WHEN pm.meta_key = '_cart_discount' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Cart Discount',
                    (select group_concat(DISTINCT coupons.order_item_name separator ', ')) as 'Coupons Used',
                    max( CASE WHEN pm.meta_key = '_order_tax' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Tax',
                    max( CASE WHEN pm.meta_key = '_order_shipping_tax' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Shipping Tax',
                    max( CASE WHEN pm.meta_key = '_order_total' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Total',
                    max( CASE WHEN pm.meta_key = '_order_currency' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Currency',

                    ( select group_concat( DISTINCT
                        concat(items.order_item_name, ' [Qty: ', qty.meta_value, ']','[Price: ', prodmeta.meta_value, ']')  separator ',  ' ) 
                        from wp_woocommerce_order_items items
                            left outer join wp_woocommerce_order_itemmeta qty on items.order_item_id = qty.order_item_id and qty.meta_key = '_qty' 
                            left outer join wp_woocommerce_order_itemmeta prodid on items.order_item_id = prodid.order_item_id and prodid.meta_key = '_product_id' 
                            left outer join wp_postmeta prodmeta on prodid.meta_value = prodmeta.post_id and prodmeta.meta_key = '_price'
                        where items.order_id = p.ID and items.order_item_type = 'line_item' ) as 'Order Items (Product Name [SKU][Qty][Price])',

                    max( CASE WHEN pm.meta_key = '_payment_method_title' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Payment Method',
                    replace(p.post_status,'wc-','') as 'Order Status',
                    max(delivery.order_item_name) as 'Shipping Method',

                    max( CASE WHEN pm.meta_key = '_shipping_first_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping First Name',
                    max( CASE WHEN pm.meta_key = '_shipping_last_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping Last Name',
                    max( CASE WHEN pm.meta_key = '_shipping_address_1' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping Address 1',
                    max( CASE WHEN pm.meta_key = '_shipping_address_2' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping Address 2',
                    max( CASE WHEN pm.meta_key = '_shipping_postcode' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping Postcode',
                    max( CASE WHEN pm.meta_key = '_shipping_city' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping City',
                    max( CASE WHEN pm.meta_key = '_shipping_state' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping State / Region',
                    max( CASE WHEN pm.meta_key = '_shipping_country' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shippping Country',
                    max( instructions.meta_value ) as 'Delivery Instructions',
                    max( CASE WHEN pm.meta_key = 'order-delivery-date' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Delivery Date',
                    ( select group_concat( DISTINCT
                        concat(sku.meta_value, ' [Qty: ', qty.meta_value, ']','[Price: ', price.meta_value, ']')  separator ',  ' ) 
                        from wp_woocommerce_order_items items
                            left outer join wp_woocommerce_order_itemmeta qty on items.order_item_id = qty.order_item_id and qty.meta_key = '_qty' 
                            left outer join wp_woocommerce_order_itemmeta prodid on items.order_item_id = prodid.order_item_id and prodid.meta_key = '_product_id' 
                            left outer join wp_postmeta price on prodid.meta_value = price.post_id and price.meta_key = '_price'
                            left outer join wp_postmeta sku on prodid.meta_value = sku.post_id and sku.meta_key = '_sku'
                        where items.order_id = p.ID and items.order_item_type = 'line_item' ) as 'Order Items (SKU [Qty][Price])',
						
                    ( select group_concat( DISTINCT
                        concat(items.order_item_name, ' [SKU: ', sku.meta_value, ']',' [Qty: ', qty.meta_value, ']','[Price: ', price.meta_value, ']')  separator ',  ' ) 
                        from wp_woocommerce_order_items items
                            left outer join wp_woocommerce_order_itemmeta qty on items.order_item_id = qty.order_item_id and qty.meta_key = '_qty' 
                            left outer join wp_woocommerce_order_itemmeta prodid on items.order_item_id = prodid.order_item_id and prodid.meta_key = '_product_id' 
                            left outer join wp_postmeta price on prodid.meta_value = price.post_id and price.meta_key = '_price'
                            left outer join wp_postmeta sku on prodid.meta_value = sku.post_id and sku.meta_key = '_sku'
                        where items.order_id = p.ID and items.order_item_type = 'line_item' ) as 'Order Items (Product Name [SKU][Qty][Price])'

                from
                    wp_posts p 
                    inner join wp_postmeta deliverydate on p.ID = deliverydate.post_id 
						and deliverydate.meta_key = 'order-delivery-date' and deliverydate.meta_value = '" . $_POST['delivery-date'] . "' 
                    left outer join wp_postmeta pm on p.ID = pm.post_id 
                    left outer join wp_woocommerce_order_items coupons on p.ID = coupons.order_id and coupons.order_item_type = 'coupon' 
                    left outer join wp_woocommerce_order_items delivery on p.ID = delivery.order_id and delivery.order_item_type = 'shipping'
                    left outer join (select wp_postmeta.post_id, wp_postmeta.meta_value from wp_postmeta where wp_postmeta.meta_key = 'Delivery Instructions') as instructions on p.ID = instructions.post_id
                    
                where
                    post_type = 'shop_order' and
                    p.post_status in (" . $orderstatus . ") 
                group by  p.ID 
                order by p.post_date DESC";
            break;

        case 'new-orders':
            
            if ( $orderstatus == '' || $_POST['start-date'] == '' || $_POST['start-time'] == '' || $_POST['end-date'] == '' || $_POST['end-time'] == '' ) {
                $error = 'Query is missing needed criteria. Please correct and then resubmit.';
                break;
            } 

            //Update group_concat limit, otherwise text gets cut off on Order Items listing for folks who order a lot
            $wpdb->query( 'SET SESSION group_concat_max_len = 1000000' );     
            
            //Generate the query
            $sql = "select p.ID as 'Order ID',
                    DATE_FORMAT(max(p.post_date), '%b %e %Y,  %h:%i:%s %p') as 'Order Date',
                    max( CASE WHEN pm.meta_key = '_billing_first_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Billing First Name',
                    max( CASE WHEN pm.meta_key = '_billing_last_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Billing Last Name',
                    (case when max( CASE WHEN pm.meta_key = '_shipping_email' and p.ID = pm.post_id THEN pm.meta_value END ) is NULL
                    then max( CASE WHEN pm.meta_key = '_billing_email' and p.ID = pm.post_id THEN pm.meta_value END )
                    else max( CASE WHEN pm.meta_key = '_shipping_email' and p.ID = pm.post_id THEN pm.meta_value END ) END ) as 'Billing E-mail ID',
                    (case when max( CASE WHEN pm.meta_key = '_shipping_phone' and p.ID = pm.post_id THEN pm.meta_value END ) is NULL
                    then max( CASE WHEN pm.meta_key = '_billing_phone' and p.ID = pm.post_id THEN pm.meta_value END )
                    else max( CASE WHEN pm.meta_key = '_shipping_phone' and p.ID = pm.post_id THEN pm.meta_value END ) END) as 'Billing Phone Number',

                    max( CASE WHEN pm.meta_key = '_order_shipping' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Shipping',
                    '' as 'Order Discount', 
                    max( CASE WHEN pm.meta_key = '_cart_discount' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Cart Discount',
                    (select group_concat(DISTINCT coupons.order_item_name separator ', ')) as 'Coupons Used',
                    max( CASE WHEN pm.meta_key = '_order_tax' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Tax',
                    max( CASE WHEN pm.meta_key = '_order_shipping_tax' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Shipping Tax',
                    max( CASE WHEN pm.meta_key = '_order_total' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Total',
                    max( CASE WHEN pm.meta_key = '_order_currency' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Order Currency',

                    ( select group_concat( DISTINCT
                        concat(items.order_item_name, ' [Qty: ', qty.meta_value, ']','[Price: ', prodmeta.meta_value, ']')  separator ',  ' ) 
                        from wp_woocommerce_order_items items
                            left outer join wp_woocommerce_order_itemmeta qty on items.order_item_id = qty.order_item_id and qty.meta_key = '_qty' 
                            left outer join wp_woocommerce_order_itemmeta prodid on items.order_item_id = prodid.order_item_id and prodid.meta_key = '_product_id' 
                            left outer join wp_postmeta prodmeta on prodid.meta_value = prodmeta.post_id and prodmeta.meta_key = '_price'
                        where items.order_id = p.ID and items.order_item_type = 'line_item' ) as 'Order Items (Product Name [SKU][Qty][Price])',

                    max( CASE WHEN pm.meta_key = '_payment_method_title' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Payment Method',
                    replace(p.post_status,'wc-','') as 'Order Status',
                    max(delivery.order_item_name) as 'Shipping Method',

                    max( CASE WHEN pm.meta_key = '_shipping_first_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping First Name',
                    max( CASE WHEN pm.meta_key = '_shipping_last_name' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping Last Name',
                    max( CASE WHEN pm.meta_key = '_shipping_address_1' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping Address 1',
                    max( CASE WHEN pm.meta_key = '_shipping_address_2' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping Address 2',
                    max( CASE WHEN pm.meta_key = '_shipping_postcode' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping Postcode',
                    max( CASE WHEN pm.meta_key = '_shipping_city' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping City',
                    max( CASE WHEN pm.meta_key = '_shipping_state' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shipping State / Region',
                    max( CASE WHEN pm.meta_key = '_shipping_country' and p.ID = pm.post_id THEN pm.meta_value END ) as 'Shippping Country',
                    max( instructions.meta_value ) as 'Delivery Instructions' 
                from
                    wp_posts p 
                    left outer join wp_postmeta pm on p.ID = pm.post_id 
                    left outer join wp_woocommerce_order_items coupons on p.ID = coupons.order_id and coupons.order_item_type = 'coupon' 
                    left outer join wp_woocommerce_order_items delivery on p.ID = delivery.order_id and delivery.order_item_type = 'shipping'
                    left outer join (select wp_postmeta.post_id, wp_postmeta.meta_value from wp_postmeta where wp_postmeta.meta_key = 'Delivery Instructions') as instructions on p.ID = instructions.post_id
                    
                where
                    post_type = 'shop_order' and
                    post_date BETWEEN '" . $_POST['start-date'] . " " . $_POST['start-time'] . "' AND '" . $_POST['end-date'] . " " . $_POST['end-time'] . "' and
                    p.post_status in (" . $orderstatus . ") 
                group by  p.ID 
                order by p.post_date DESC";
            break;

        case 'already-ordered';
            //For Already Ordered, switch format to plain text list of emails
            $format = 'email';
            
            if ( $orderstatus == '' || $_POST['start-date'] == '' || $_POST['start-time'] == '' || $_POST['end-date'] == '' || $_POST['end-time'] == '' ) {
                $error = 'Query is missing needed criteria. Please correct and then resubmit.';
                break;
            }
            
            //Update group_concat limit, otherwise text gets cut off on Order Items listing for folks who order a lot
            $wpdb->query( 'SET SESSION group_concat_max_len = 1000000' );
           
            
            //Generate the query
            $sql = "select pm.meta_value emailaddr  
                from
                    wp_posts p 
                    inner join wp_postmeta pm on p.ID = pm.post_id and pm.meta_key in ('_billing_email','_shipping_email') 
                where
                    post_type = 'shop_order' and
                    post_date BETWEEN '" . $_POST['start-date'] . " " . $_POST['start-time'] . "' AND '" . $_POST['end-date'] . " " . $_POST['end-time'] . "' and 
                    p.post_status in (" . $orderstatus . ") 
                group by pm.meta_value  
                order by pm.meta_value ASC";
            break;
            
        case 'shipping-notes':
            $sql = "
                SELECT email.meta_value as email, trim(notes.meta_value) as shipping_notes
                FROM  {$wpdb->prefix}users users
                INNER JOIN {$wpdb->prefix}usermeta notes on users.ID = notes.user_id and notes.meta_key = 'shipping-notes-note' and notes.meta_value IS NOT NULL
                INNER JOIN {$wpdb->prefix}usermeta email on users.ID = email.user_id and email.meta_key = 'billing_email' and email.meta_value IS NOT NULL
                order by email.meta_value";
            break;
        case 'first-order':
            $sql = "
                SELECT users.ID 'User ID', email.meta_value Email, min(posts.ID) 'First Order ID', min(posts.post_date) 'First Order Date'
                FROM  {$wpdb->prefix}users users
                INNER JOIN {$wpdb->prefix}usermeta email on users.ID = email.user_id and email.meta_key = 'billing_email' and email.meta_value IS NOT NULL
                INNER JOIN {$wpdb->prefix}postmeta customer on users.ID = customer.meta_value and customer.meta_key = '_customer_user'
                INNER JOIN {$wpdb->prefix}posts posts on customer.post_id = posts.ID and posts.post_type = 'shop_order' and post_status in ('wc-processing','wc-completed') 
                group by users.ID, email.meta_value
                order by email.meta_value";
            break;
    }
    
    if ($format == 'csv') :
        
        if ( $sql != '' )
            $rows = $wpdb->get_results( $sql );        
        
        $count = 0;
        //Empty array for header row
        $headers = array();

        //Create file name from query name and current date
        $fileName = sanitize_title_with_dashes($queryname) . '-' . date('Y-m-d') . '.csv';

        //Header for CSV file
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        //Open the file
        $fh = @fopen( 'php://output', 'w' );

        //See what SQL query is
        //fwrite($fh, $sql);
        error_log($sql);

        if ( $error != '') {
            fwrite($fh, $error);
        } elseif ( !$rows ) {
            fwrite($fh, 'No data found found. Please try again.');
        } else {
            foreach ( $rows as $row ) {
                $count++;

                //If on 1st row, gather header column names for file
                if ( $count == 1) {            
                    foreach ( $row as $key => $value ) {
                        $headers[] = ucwords($key);
                    }
                    // Write column names to file
                    fputcsv($fh, $headers);
                }

                // Cast the results as an array
                $data = (array) $row; 
                // Write row data to file
                fputcsv($fh, $data);
            }
        } 

        // Close the file
        fclose($fh);
        
    elseif ($format == 'email') :
//        //Create file name from query name and current date
//        $fileName = sanitize_title_with_dashes($queryname) . '-' . date('Y-m-d') . '.txt';
//
//        //Header for TXT file
//        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//        header('Content-Description: File Transfer');
//        header("Content-Type: plain/text");
//        header("Content-Disposition: attachment; filename={$fileName}");
//        header("Expires: 0");
//        header("Pragma: public");
        
        echo '<html><head><title>Already Ordered MailChimp Semgent</title></head><body>';        
    
        //See SQL query (debugging only)
        //echo $sql . '<br />';
    
        if ( $error != '') {
            echo $error;
        } else {
            if ( $sql != '' )
                $rows = $wpdb->get_col( $sql );   
            
            if ( !$rows ) {
                echo 'No data found found. Please try again.';
            } else {        
                foreach ( $rows as $email ) {
                    // Write data to screen
                    echo $email . '<br />';
                }        
            }
        }
        echo '</body></html>';
    endif;

}

function foodery_time_dropdown($name, $class) { ?>
        <select class='<?php echo $class; ?>' name='<?php echo $name; ?>'>
            <option value=''></option>
            <option value='00:00:00'>12:00 AM</option>
            <option value='00:30:00'>12:30 AM</option>
            <option value='01:00:00'>01:00 AM</option>
            <option value='01:30:00'>01:30 AM</option>
            <option value='02:00:00'>02:00 AM</option>
            <option value='02:30:00'>02:30 AM</option>
            <option value='03:00:00'>03:00 AM</option>
            <option value='03:30:00'>03:30 AM</option>
            <option value='04:00:00'>04:00 AM</option>
            <option value='04:30:00'>04:30 AM</option>
            <option value='05:00:00'>05:00 AM</option>
            <option value='05:30:00'>05:30 AM</option>
            <option value='06:00:00'>06:00 AM</option>
            <option value='06:30:00'>06:30 AM</option>
            <option value='07:00:00'>07:00 AM</option>
            <option value='07:30:00'>07:30 AM</option>
            <option value='08:00:00'>08:00 AM</option>
            <option value='08:30:00'>08:30 AM</option>
            <option value='09:00:00'>09:00 AM</option>
            <option value='09:30:00'>09:30 AM</option>
            <option value='10:00:00'>10:00 AM</option>
            <option value='10:30:00'>10:30 AM</option>
            <option value='11:00:00'>11:00 AM</option>
            <option value='11:30:00'>11:30 AM</option>
            <option value='12:00:00'>12:00 PM</option>
            <option value='12:30:00'>12:30 PM</option>
            <option value='13:00:00'>01:00 PM</option>
            <option value='13:30:00'>01:30 PM</option>
            <option value='14:00:00'>02:00 PM</option>
            <option value='14:30:00'>02:30 PM</option>
            <option value='15:00:00'>03:00 PM</option>
            <option value='15:30:00'>03:30 PM</option>
            <option value='16:00:00'>04:00 PM</option>
            <option value='16:30:00'>04:30 PM</option>
            <option value='17:00:00'>05:00 PM</option>
            <option value='17:30:00'>05:30 PM</option>
            <option value='18:00:00'>06:00 PM</option>
            <option value='18:30:00'>06:30 PM</option>
            <option value='19:00:00'>07:00 PM</option>
            <option value='19:30:00'>07:30 PM</option>
            <option value='20:00:00'>08:00 PM</option>
            <option value='20:30:00'>08:30 PM</option>
            <option value='21:00:00'>09:00 PM</option>
            <option value='21:30:00'>09:30 PM</option>
            <option value='22:00:00'>10:00 PM</option>
            <option value='22:30:00'>10:30 PM</option>
            <option value='23:00:00'>11:00 PM</option>
            <option value='23:30:00'>11:30 PM</option>
            <option value='23:59:59'>11:59 PM</option>
        </select>    
<?php }