<?php
/*
This file handles the admin area and functions.
You can use this file to make changes to the
dashboard. Updates to this page are coming soon.
It's turned off by default, but you can call it
via the functions file.

Developed by: Eddie Machado
URL: http://themble.com/bones/

Special Thanks for code & inspiration to:
@jackmcconnell - http://www.voltronik.co.uk/
Digging into WP - http://digwp.com/2010/10/customize-wordpress-dashboard/

*/

/************* DASHBOARD WIDGETS *****************/

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
	// remove_meta_box('dashboard_right_now', 'dashboard', 'core');    // Right Now Widget
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');         // Plugins Widget

	//remove_meta_box('dashboard_quick_press', 'dashboard', 'core');  // Quick Press Widget
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
	remove_meta_box('dashboard_primary', 'dashboard', 'core');         //
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');       //

	// removing plugin dashboard boxes
	remove_meta_box('yoast_db_widget', 'dashboard', 'normal');         // Yoast's SEO Plugin Widget

	/*
	have more plugin widgets you'd like to remove?
	share them with us so we can get a list of
	the most commonly used. :D
	https://github.com/eddiemachado/bones/issues
	*/
}

/*
Now let's talk about adding your own custom Dashboard widget.
Sometimes you want to show clients feeds relative to their
site's content. For example, the NBA.com feed for a sports
site. Here is an example Dashboard Widget that displays recent
entries from an RSS Feed.

For more information on creating Dashboard Widgets, view:
http://digwp.com/2010/10/customize-wordpress-dashboard/
*/

// removing the dashboard widgets
add_action('admin_menu', 'disable_default_dashboard_widgets');


/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it
function bones_login_css() {
    wp_enqueue_style('login-page', 
         get_template_directory_uri() . '/library/css/login.css', false );	
}

// changing the logo link from wordpress.org to your site
function bones_login_url() { echo bloginfo('url'); }

// changing the alt text on the logo to show your site name
function bones_login_title() { echo get_option('blogname'); }

// calling it only on the login page
add_action('login_head', 'bones_login_css');
add_filter('login_headerurl', 'bones_login_url');
add_filter('login_headertitle', 'bones_login_title');


/************* CUSTOMIZE ADMIN *******************/

/*
I don't really reccomend editing the admin too much
as things may get funky if Wordpress updates. Here
are a few funtions which you can choose to use if
you like.
*/


//Add widget to dashboard page
function foodery_add_events_dashboard_widget() {
   wp_add_dashboard_widget('foodery-events-widget', 'Event Links', 
      'foodery_events_dashboard_widget');
}
add_action('wp_dashboard_setup', 'foodery_add_events_dashboard_widget');

//Dashboard widget logic (can be anything!)
function foodery_events_dashboard_widget() {
    $args = array (
        'post_type' => 'foodery-events',
        'posts_per_page' => 10,
        'post_status' => 'publish'
    );
    $events = new WP_Query( $args );
    
    if ( $events->have_posts() ) :
        while ($events->have_posts()) : $events->the_post();
            echo '<div style="margin-bottom: 10px;">';
            echo '<a href="' . get_the_permalink() . '">';
            echo get_the_title() . ' (' . get_field('coupon_code') . ')</a>';
            echo ' - <a href="' . home_url() . '/wp-admin/post.php?post=' . get_the_id() . '&action=edit">Edit</a></div>';
        endwhile;
    endif;

}
