<?php
/*
Template Name: Update User Names
*/
get_header(); 

// The Query
$args = array(
    'role' => 'Customer' ,

);
$users = new WP_User_Query( $args );

$count = 0;
$updated = 0;


echo '"Email","Display Name (previous)", "Updated Name","User ID"<br />';  

// User Loop
if ( ! empty( $users->results ) ) {
	foreach ( $users->results as $user ) {
            $user_id = $user->ID;
            $count++;
            $firstname = get_user_meta($user_id, 'first_name', true);
            $lastname = get_user_meta($user_id, 'last_name', true);    

            if ($firstname == '' || $lastname == '') {

                //Get Billing First & Last Name
                $firstname = get_user_meta($user_id, 'billing_first_name', true);
                $lastname = get_user_meta($user_id, 'billing_last_name', true);
                if ($firstname != '' && $lastname != '') {
                    update_user_meta($user_id, 'first_name', $firstname);
                    update_user_meta($user_id, 'last_name', $lastname);   
                    $updated++;                    
                    echo '"' . $user->user_email . '","' . $user->display_name . '","' . $firstname . ' ' . $lastname . '","' . $user->ID . '"<br />';  
                }
        
            }   
        }
} else {
	echo 'No users found.';
}


echo 'Total Users processed: ' . $count . '<br />';
echo 'Total Users updated: ' . $updated;

get_footer(); 