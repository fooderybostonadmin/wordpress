
<?php

$slides = get_field('slider_image');

if($slides){
	echo '<div id="myCarousel" class="hidden-phone carousel slide"><div class="carousel-inner">';
	$i = 0; //counter
	foreach($slides as $s){
		if($i==0){
			echo '<div class="item active">';
		} else {
			echo '<div class="item">';
		}
		$i++;
		//print_r($s['image']);
		echo '<img src="' . $s['image']['sizes']['home-carousel'] .'" alt="">';
		echo '<div class="container"><div class="hidden-phone carousel-caption"><h1>' . $s['title'] . '</h1>';
		$excerpt_length = 100; // length of excerpt to show (in characters)
		$the_excerpt = $s['caption'];

		if(strlen($the_excerpt) >= $excerpt_length){
			$caption = substr( $the_excerpt, 0, $excerpt_length ) . '... ';
		} else {
			$caption = $the_excerpt;
		}
		echo '<p class="lead">' . $caption . '</p>';

		$link = ($s['button_link']) ? $s['button_link']: $s['button_link_manual'];
		echo '<a class="btn btn-large btn-success" href="'. $link .'">' . $s['button_title'] . '</a>';
		echo '</div></div></div>';

	}
	echo '</div><a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>';
	echo '<a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a></div>';


}; ?>
<?php if (get_field('home-mobile-static-image')){
	$mobile_image = get_field('home-mobile-static-image');
	echo '<div class="visible-phone mobile-static">';
	echo '<img src="'.$mobile_image['sizes']['home-mobile-static-carousel'].'" height="400" width="764" />';
	echo '</div>';
};?>


