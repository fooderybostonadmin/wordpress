<?php
/*
	Partial: Quotes Slider
 * 
 * (was Soliloquy, now Slider Revolution)
*/
?>
	<div class="row-fluid quotes-soliloquy">
		<div class="container">
			<div class="row">
				<?php if ( $f = get_field('quotes_soliloquy')): ?>
					<?php echo do_shortcode($f); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
