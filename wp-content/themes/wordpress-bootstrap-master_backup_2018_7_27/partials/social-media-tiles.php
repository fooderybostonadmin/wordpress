<?php
/*
	Partial: social media tiles
*/

$title = get_field('social_media_title', 'option');
$content = get_field('social_media_display', 'option');
?>
	<div class="row-fluid">
		<div class="container social-media-tiles">
                    <?php if ($title != '') : ?>
                    <div class="distress-title"><?php echo $title; ?></div>
                    <?php endif; ?>
                    
                    <?php //Uses Enjoy plugin for Instagram
                    echo $content; ?>
                </div>
        </div>

