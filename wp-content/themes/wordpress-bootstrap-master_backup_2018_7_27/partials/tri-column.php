      <div class="row-fluid stack">
        <div class="container">  
          <div class="row">
            <h2 class="text-center">Three Great Times to Give The Gift of Food</h2>
          </div>
          <div class="row">
            <div class="span4">
              <h2>Hardships</h2>
              <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
            </div><!-- /.span4 -->
            <div class="span4">
              <h2>Newborns</h2>
              <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            </div><!-- /.span4 -->
            <div class="span4">
              <h2>Transitions</h2>
              <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            </div><!-- /.span4 -->
          </div><!-- row -->
          <div class="row button-row">
            <div class="span6 offset3"><a href="#giving-a-card" class="btn btn-large btn-block">Give A Gift Card </a></div>
          </div>
        </div>
      </div><!-- /.row -->
