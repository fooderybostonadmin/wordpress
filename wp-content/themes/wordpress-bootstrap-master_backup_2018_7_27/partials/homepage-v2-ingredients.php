<?php
/*
	Partial: ingredients
*/

if( have_rows('ingredients') ): 
    $bkgdimg = get_field('ingredients_background'); 
    $marker = get_field('ingredients_item_marker'); ?>

	<div class="row-fluid ingredients" style="background-image:url(<?php echo $bkgdimg; ?>)">
            <div class="container">

                <div class="ingredients-title-block">
                    <?php
                    $titleimgid = get_field( 'ingredients_header_image');
                    $titletext =  get_field( 'ingredients_title');
                    echo wp_get_attachment_image ($titleimgid, 'full', false, array( 'alt' => $titletext, 'class' => 'title-img' ));
                    ?>
                </div>
                
                <div class="row">                    
                <?php while( have_rows('ingredients') ): the_row(); ?>
                    <div class="span4 ingredient" style="">

                            <img class="icon" src="<?php echo get_sub_field('image'); ?>" />
                            <h3><?php echo get_sub_field('title'); ?></h3>
                            <?php if( have_rows('items') ): ?>
                                    <ul>
                                        <?php while( have_rows('items') ): the_row(); ?>
                                                <li style="background-image:url(<?php echo $marker; ?>)"><?php echo get_sub_field('item'); ?></></li>
                                        <?php endwhile; ?>
                                    </ul>
                            <?php endif; ?>

                            <p><?php echo get_sub_field('content'); ?></p>
                            <?php if( $l = get_sub_field('link')): ?>
                                    <a class="link" href="<?php echo $l; ?>">
                            <?php endif; ?>
                                            
                            <?php echo get_sub_field( 'link_title'); ?>
                            <?php if( $l): ?>
                                    </a>
                            <?php endif; ?>

                    </div>
                <?php endwhile; ?>
                </div><!-- .row -->
                
            </div><!-- .container -->
	</div><!-- .row-fluid ingredients -->
<?php endif; ?>
