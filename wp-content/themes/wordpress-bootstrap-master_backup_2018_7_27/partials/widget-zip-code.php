<div class="widget well">
	<h3><?php echo get_field('zone_widget_title','options');?></h3>
	<p><?php echo get_field('zone_widget_text','options');?></p>
	<form class="zipForm" action="">
		<div class="input-append">
		  <input class="span8 zipWidgetInput" type="text" placeholder="Enter Zip Code">
		  <a class="zipWidget btn">Check</a>
		</div>
		<div class="zipCheckResults"></div>
<!-- 		<input type="text" placeholder="Your Zip Code"/> -->
	</form>
</div>

<?php 
global $zipformcount;

//Make sure we only include the JS once even if the form is shown multiple times on a page
if ($zipformcount <= 1 ) : ?>
<script type="text/javascript">
    //declare the arrays coming from the options panel (if we haven't already run this code
       
	<?php 	$widget_zones = get_field('zone', 'option');
			$out_of_range = get_field('out_of_range_message','option');
			$in_range = get_field('we_deliver_message','option');
			$js_widget_zones = array();
			foreach($widget_zones as $zone){
				array_push($js_widget_zones,explode(",",$zone['zip_codes']));
			};
			echo 'var widgetZones = '. json_encode($js_widget_zones).';';
			echo 'var widgetOutOfRange = "'. $out_of_range.'";';
			echo 'var widgetInRange = "'.$in_range .'";';

	?>
	jQuery(document).ready(function($){

		widgetOutOfRange = $('<p/>').html(widgetOutOfRange).text();
		widgetInRange = $('<p/>').html(widgetInRange).text();
                $('.zipWidget').on('click',function(e){

			e.preventDefault();
                        var zipForm = $(this).closest('form');
			var widgetDeliveryZip = zipForm.find('.zipWidgetInput').val();
                                            
                        //Remove success or failure classes
                        zipForm.find('.zipCheckResults').removeClass('zip-success').removeClass('zip-failure');
                        
			$.each(widgetZones,function(i,v){
                            
				if ( $.inArray(widgetDeliveryZip, widgetZones[i]) > -1 ){
					zipForm.find('.zipCheckResults').text(widgetInRange).addClass('zip-success');
					return false;
				} else {
					zipForm.find('.zipCheckResults').text(widgetOutOfRange).addClass('zip-failure');
				}
			});                    
                });
		$(".zipWidgetInput").keypress(function(e) {
		    if (e.which == 13) {
		        e.preventDefault();
                        var zipForm = $(this).closest('form');
		        zipForm.find('.zipWidget').click();
		    }
		});                

	});

</script>
<?php endif; //if ($zipformcount <= 1 )?>
