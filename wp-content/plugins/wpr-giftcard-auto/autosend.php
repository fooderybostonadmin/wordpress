<?php
/*
Plugin Name: WooCommerce - Gift Cards | Auto Send Gift Card
Plugin URI: http://wp-ronin.com
Description: Have the giftcard sent automaticly when the gift card is sold
Version: 1.9.8
Author: WP Ronin
Author URI: http://wp-ronin.com
License: GPL2
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	// load our custom updater
	include( plugin_dir_path( __FILE__ ) . '/includes/EDD_SL_Plugin_Updater.php' );
}

//******************************************//

if( ! class_exists( 'WPRWG_Auto_Send' ) ) {

	class WPRWG_Auto_Send {
		private static $wpr_wg_autosend_instance;

		/**
		 * Get the singleton instance of our plugin
		 * @return class The Instance
		 * @access public
		 */
		public static function getInstance() {
		
			if ( !self::$wpr_wg_autosend_instance  ) {
				self::$wpr_wg_autosend_instance = new WPRWG_Auto_Send();
	            
	            if( ! class_exists( 'WPRWG_GiftCards_Pro' ) ){
	            	self::$wpr_wg_autosend_instance->setup_constants();
	            	self::$wpr_wg_autosend_instance->wpr_loaddomain();
	            }

	            self::$wpr_wg_autosend_instance->includes();
	            self::$wpr_wg_autosend_instance->hooks();
			}
			return self::$wpr_wg_autosend_instance;
		}

	    /**
	     * Setup plugin constants
	     *
	     * @access      private
	     * @since       1.0.0
	     * @return      void
	     */
	    private function setup_constants() {

			// Plugin version
			if ( ! defined( 'WPR_AUTO_SEND_VERSION' ) )
				define( 'WPR_AUTO_SEND_VERSION', '1.9.8' );

			// Plugin Folder Path
			if ( ! defined( 'RPWCGC_AUTO_PATH' ) )
				define( 'RPWCGC_AUTO_PATH', plugin_dir_path( __FILE__ ) );

			// Plugin Folder URL
			if ( ! defined( 'RPWCGC_AUTO_URL' ) )
				define( 'RPWCGC_AUTO_URL', plugins_url( 'wpr-giftcard-auto', 'autosend.php' ) );

			// Plugin Root File
			if ( ! defined( 'RPWCGC_AUTO_FILE' ) )
				define( 'RPWCGC_AUTO_FILE', plugin_basename( __FILE__ )  );

			// Premium Plugin Store
			if ( ! defined( 'WPR_STORE_URL' ) )
				define( 'WPR_STORE_URL', 'https://wp-ronin.com' );

			if ( ! defined( 'WPR_GC_ACTIVE_PLUGIN' ) ) define( 'WPR_GC_ACTIVE_PLUGIN', 'Active' );

			define( 'WPR_AUTO_SEND_NAME', 'WooCommerce - Gift Cards | Auto Send Card' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

	    }

	    /**
         * Include necessary files
         *
         * @access      private
         * @since       1.0.0
         * @return      void
         */
        private function includes() {

            // Include scripts
        	if( ! class_exists( 'WPR_Giftcard' ) ) {
	            require_once get_home_path() . '/wp-content/plugins/gift-cards-for-woocommerce/includes/class.giftcard.php';
	        }
	        
	        if( ! class_exists( 'WPR_Giftcard_Email' ) ) {
		        require_once get_home_path() . '/wp-content/plugins/gift-cards-for-woocommerce/includes/class.giftcardemail.php';
		    }

        }
        

		public function rpgc_auto_send_settings( $options, $current_section ) {

			if( $current_section == 'auto' ) {

				$title = array(
					array( 'title' 		=> __( 'Auto Send Settings',  'wpr-pro'  ), 'type' => 'title', 'id' => 'giftcard_processing_options_title' ),
				);

				$options = apply_filters( 'wpr_giftcard_auto_settings', array(
					array(
						'name'		=> __( 'Time to Expire', 'wpr-pro' ),
						'desc'		=> __( 'Select the number of days you would like cards to be valid for.', 'wpr-pro' ),
						'id'		=> 'rpgc_auto_defaultExpiry',
						'std'		=> '0', // WooCommerce < 2.0
						'default'	=> '0', // WooCommerce >= 2.0
						'type'		=> 'text',
						'desc_tip'	=>  true,
					),

					array(
						'name'		=> __( 'Send', 'wpr-pro' ),
						'desc'		=> __( 'Select when you would like the card sent. Example: If you want the card sent imediately, select "On Placing Order"', 'wpr-pro' ),
						'id'		=> 'rpgc_auto_when',
						'std'		=> 'payment', // WooCommerce < 2.0
						'default'	=> 'payment', // WooCommerce >= 2.0
						'type'		=> 'select',
						'class'		=> 'chosen_select',
						'options'	=> array(
							'payment'	=> __( 'On Placing Order', 'wpr-pro' ),
							'order'		=> __( 'On Order Completion', 'wpr-pro' )
						),
						'desc_tip' =>  true,
					),
					array(
						'name'         => __( 'Enable Send Later',  'wpr-pro'  ),
						'desc'          => __( 'Select this to enable the option to send the card later.',  'wpr-pro'  ),
						'id'            => 'wpr_enable_send_later',
						'default'       => 'no',
						'type'          => 'checkbox',
						'desc_tip'		=> true,
					),
					
					array(
					'name'     => __( 'Send Later Test', 'wpr-pro' ),
					'desc'     => __( 'This is what will display on the send later box.', 'wpr-pro' ),
					'id'       => 'woocommerce_giftcard_send_later',
					'std'      => 'Send Later', // WooCommerce < 2.0
					'default'  => 'Send Later', // WooCommerce >= 2.0
					'type'     => 'text',
					'desc_tip' =>  true,
				),
					


					array( 'type' => 'sectionend', 'id' => 'rpgc_auto_settings'),
				)); // End pages settings

				$options = array_merge( $title, $options );
			}

			return $options;
		}


	    /**
	     * Run action and filter hooks
	     *
	     * @access      private
	     * @since       1.0.0
	     * @return      void
	     *
	     */
	    private function hooks() {
			// Handle licenses
			if( ! class_exists( 'WPRWG_GiftCards_Pro' ) ){
				
				if ( ! class_exists( 'WPRWooGiftcards' ) )
					add_action( 'admin_notices', array( $this, 'no_woo_gift_nag' ) );

				add_action( 'wpr_add_license_field', array( $this, 'wpr_auto_option' ) );

				add_action( 'admin_init', array( $this, 'plugin_updater' ) );
				add_action( 'admin_init', array( $this, 'activate_license' ) );
				add_action( 'admin_init', array( $this, 'deactivate_license' ) );
					
			}

			if( is_admin() ) {
				add_action( 'get_giftcard_settings', array( $this, 'wpr_autosend_page'), 10, 2 );
		    	add_filter( 'woocommerce_add_section_giftcard', array( $this, 'wpr_autosend_settings' ) );
		    	add_filter( 'get_giftcard_settings', array( $this, 'rpgc_auto_send_settings' ), 10, 2 );
		    	add_action( 'rpgc_woocommerce_options_after_personalize', array( $this, 'wpr_add_send_later_field' ), 10, 1 );
			}
			
			if ( get_option( 'rpgc_auto_when' ) == NULL ) {
				$wpr_when = "order";
			} else {
				$wpr_when = get_option( 'rpgc_auto_when' );
			}

			if( $wpr_when == "order" ) {
				add_action( 'woocommerce_order_status_completed', array( $this, 'rpgc_create_automatically'), 10, 1);
				add_action( 'woocommerce_order_status_completed', array( $this, 'rpgc_send_automatically'), 20, 1);
			} else {
				add_action( 'woocommerce_order_status_processing', array( $this, 'rpgc_create_automatically'), 10, 1);
				add_action( 'woocommerce_order_status_processing', array( $this, 'rpgc_send_automatically'), 20, 1);
			}

			add_action( 'rpgc_after_product_fields', array( $this, 'wpr_setup_send_later'));
			add_filter( 'rpgc_giftcard_data', array( $this, 'wpr_add_gift_card_info'));
			add_action( 'wp_loaded', array( $this, 'wpr_send_gift_cards' ) );

	    }

		/**
		 * If no license key is saved, show a notice
		 * @return void
		 */
		public function no_woo_gift_nag() {
			// We need plugin.php!
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			
			$plugins = get_plugins();
			
			// Set plugin directory
			$plugin_path = array_filter( explode( '/', $plugin_path ) );
			$this->plugin_path = end( $plugin_path );
			
			// Set plugin file
			$this->plugin_file = $plugin_file;
			
			// Set plugin name
			$this->plugin_name = 'WooCommerce - Gift Cards | Auto Send';
			
			// Is EDD installed?
			foreach( $plugins as $plugin_path => $plugin ) {

				if( $plugin['Name'] == 'WooCommerce - Gift Cards' ) {
					$this->has_woo = true;
					$this->wpr_base = $plugin_path;
					break;
				}
			}

	        if( $this->has_woo ) {
	            $url  = esc_url( wp_nonce_url( admin_url( 'plugins.php?action=activate&plugin=' . $this->wpr_base ), 'activate-plugin_' . $this->wpr_base ) );
	            $link = '<a href="' . $url . '">' . __( 'activate it', 'wpr-pro' ) . '</a>';
	        } else {
	            $url  = esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=woocommerce' ), 'install-plugin_woocommerce' ) );
	            $link = '<a href="' . $url . '">' . __( 'install it', 'wpr-pro' ) . '</a>';
	        }
	        
	        echo '<div class="error"><p>' . $this->plugin_name . sprintf( __( ' requires WooCommerce - Gift Cards! Please %s to continue!', 'wpr-pro' ), $link ) . '</p></div>';

		}


		/**
		 * Load the Text Domain for i18n
		 * @return void
		 * @access public
		 */
		public function wpr_loaddomain() {
			load_plugin_textdomain( 'wpr-auto-txt', false, '/rpgc_auto_send_card/languages/' );
		}

	//****************************************************************************************
	//  License Functions ********************************************************************
	//****************************************************************************************


		/**
		 * Sets up the EDD SL Plugin updated class
		 * @return void
		 */
		public function plugin_updater() {
			$licenses = get_option( 'wpr_license_key' );

			$license_key = '';

			if( isset( $licenses[ "auto" ] ))
				$license_key = $licenses[ "auto" ];

			// setup the updater
			$edd_updater = new EDD_SL_Plugin_Updater( WPR_STORE_URL, __FILE__, array(
					'version'   => WPR_AUTO_SEND_VERSION,	// current version number
					'license'   => $license_key,        	// license key (used get_option above to retrieve from DB)
					'item_name' => WPR_AUTO_SEND_NAME,     	// name of this plugin
					'author'    => 'WP Ronin'  				// author of this plugin
				)
			);
		}


		public function wpr_auto_option() {
			$license 	= get_option( 'wpr_license_key' );
			$status 	= get_option( 'wpr_auto_license_status' );

			settings_fields('wpr-options');

			if( isset( $_POST["wpr_auto_license_key"] ) ) {
				$license_key = $_POST["wpr_auto_license_key"];

				$license["auto"] = $this->wpr_sanitize_license( $license_key );

				update_option( 'wpr_license_key', $license );
			}

			?>
			<tr valign="top">	
				<th scope="row" valign="middle" style="text-align: left;">
					<?php _e('WooCommerce - Gift Cards | Auto Send Card', 'wpr-pro'); ?>
				</th>
				<td>
					<input id="wpr_auto_license_key" name="wpr_auto_license_key" type="text" class="regular-text" placeholder="<?php _e('Enter License Number', 'wpr-pro' ); ?>" value="<?php esc_attr_e( $license["auto"] ); ?>" <?php if( false !== $license[ "auto" ] && $status !== false && $status == 'valid' ) { ?> style="border: 1px solid green;" <?php } ?> />
					<input id="wpr_auto_options" name="wpr_auto_options" type="hidden" value="wpr_auto_license_status" />
					<?php 
					if( "" !== $license["auto"] ) {  
						if( $status !== false && $status == 'valid' ) { 
					?>
							<?php wp_nonce_field( 'wpr_auto_de_nonce', 'wpr_auto_de_nonce' ); ?>
							<input type="submit" class="button-secondary" name="wpr_auto_license_deactivate" value="<?php _e('Deactivate License', 'wpr-pro' ); ?>"/>
						<?php } else {
							wp_nonce_field( 'wpr_auto_ac_nonce', 'wpr_auto_ac_nonce' ); ?>
							<input type="submit" class="button-secondary" name="wpr_auto_license_activate" value="<?php _e('Activate License', 'wpr-pro' ); ?>"/>
						<?php } ?>
					<?php } ?>

				</td>
			</tr>
			<br />
			<?php 
		}

		/**
		 * Deactivates the license key
		 * @return void
		 */
		public function deactivate_license() {
			// listen for our activate button to be clicked
			if( isset( $_POST['wpr_auto_license_deactivate'] ) ) {

				// run a quick security check
				if( ! check_admin_referer( 'wpr_auto_de_nonce', 'wpr_auto_de_nonce' ) )
					return; // get out if we didn't click the Activate button

				// retrieve the license from the database
				$licenses = get_option( 'wpr_license_key' );

				$license = $licenses["auto"] ;

				// data to send in our API request
				$api_params = array(
					'edd_action'=> 'deactivate_license',
					'license' 	=> $license,
					'item_name' => urlencode( WPR_AUTO_SEND_NAME ) // the name of our product in EDD
				);

				// Call the custom API.
				$response = wp_remote_get( add_query_arg( $api_params, WPR_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

				// make sure the response came back okay
				if ( is_wp_error( $response ) )
					return false;

				// decode the license data
				$license_data = json_decode( wp_remote_retrieve_body( $response ) );

				// $license_data->license will be either "deactivated" or "failed"
				if( $license_data->license == 'deactivated' )
					delete_option( 'wpr_auto_license_status' );

			}
		}

		/**
		 * Activates the license key provided
		 * @return void
		 */
		public function activate_license() {
			// listen for our activate button to be clicked
			// 
			//var_dump($_POST);

			if( isset( $_POST['wpr_auto_license_activate'] ) ) {

				// run a quick security check
			 	if( ! check_admin_referer( 'wpr_auto_ac_nonce', 'wpr_auto_ac_nonce' ) )
					return; // get out if we didn't click the Activate button

				// retrieve the license from the database
				$licenses = get_option( 'wpr_license_key' );

				$license = '';
				if( isset( $licenses[ "auto" ] ))
					$license = $licenses["auto"];


				// data to send in our API request
				$api_params = array(
					'edd_action'=> 'activate_license',
					'license' 	=> $license,
					'item_name' => urlencode( WPR_AUTO_SEND_NAME ) // the name of our product in EDD
				);

				// Call the custom API.
				$response = wp_remote_get( add_query_arg( $api_params, WPR_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

				// make sure the response came back okay
				if ( is_wp_error( $response ) )
					return false;

				// decode the license data
				$license_data = json_decode( wp_remote_retrieve_body( $response ) );

				// $license_data->license will be either "active" or "inactive"
				update_option( 'wpr_auto_license_status', $license_data->license );


			}
		}


		public function wpr_register_settings() {
			// creates our settings in the options table
			
			register_setting('wpr-options', 'wpr_options' );
			register_setting('wpr-options', 'wpr_license_key', array( $this, 'wpr_sanitize_license' ) );
		}

		/**
		 * Sanatize the liscense key being provided
		 * @param  string $new The License key provided
		 * @return string      Sanitized license key
		 */
		public function wpr_sanitize_license( $new ) {

			$keys = get_option( 'wpr_license_key' );
			$old = $keys["auto"];

			if( $old && $old != $new ) {
				delete_option( 'wpr_auto_license_status' ); // new license has been entered, so must reactivate
			}
			return $new;
		}
		

	//****************************************************************************************
	//  End License Functions ***********************************************************
	//****************************************************************************************


	    public function wpr_autosend_page( $options, $current_section ){

	        if( $current_section == 'auto' ) {
	            $options = array(

	                array( 'title'  => __( 'Gift Cards Auto Send',  'wpr-pro'  ), 'type' => 'title', 'id' => 'giftcard_autosend_options_title' ),
	                array( 'type'   => 'sectionend', 'id' => 'giftcard_import' ),
	            );
	        }

	        return $options;
	    }

	    public function wpr_autosend_settings( $sections ){
	        $auto = array( 'auto' => __( 'Gift Cards Auto Send', 'wpr-pro' ) );
	        return array_merge( $sections, $auto );

	    }

	    function wpr_add_gift_card_info ( $giftcard_data ) {
			$rpw_send_later_card 	= ( get_option( 'woocommerce_giftcard_send_later' ) <> NULL ? get_option( 'woocommerce_giftcard_send_later' ) : __('Send Later', 'wpr-pro' )  );
			
			$gift_send = array(
				$rpw_send_later_card => 'NA'
				);

			if ( isset( $_POST[ 'rpgc_send_later_date' ] ) && ( $_POST[ 'rpgc_send_later_date' ] <> '' ) ) {
				$gift_send[ $rpw_send_later_card ] = wc_clean( $_POST[ 'rpgc_send_later_date' ] );
				$giftcard_data = array_merge( $giftcard_data, $gift_send);
			}

			return $giftcard_data;
	    }

		public function wpr_send_automatically ( $order_id, $old_status, $new_status ) {
			$cards = update_post_meta( $order_id, 'rpgc_numbers', true );

		}

		public function rpgc_create_automatically ( $order_id ) {
			global $wpdb, $current_user;
			$current_user = wp_get_current_user();

			$order = new WC_Order( $order_id ); 
			$theItems = $order->get_items();
			$firstName = $order->get_billing_first_name();
			$lastName = $order->get_billing_last_name();

			$rpw_to_check 				= ( get_option( 'woocommerce_giftcard_to' ) <> NULL ? get_option( 'woocommerce_giftcard_to' ) : __('To', 'wpr-pro' ) );
			$rpw_toEmail_check 			= ( get_option( 'woocommerce_giftcard_toEmail' ) <> NULL ? get_option( 'woocommerce_giftcard_toEmail' ) : __('To Email', 'wpr-pro' )  );
			$rpw_note_check				= ( get_option( 'woocommerce_giftcard_note' ) <> NULL ? get_option( 'woocommerce_giftcard_note' ) : __('Note', 'wpr-pro' )  );
			$rpw_address_check			= ( get_option( 'woocommerce_giftcard_address' ) <> NULL ? get_option( 'woocommerce_giftcard_address' ) : __('Note', 'wpr-pro' )  );
			$rpw_send_later				= ( get_option( 'woocommerce_giftcard_send_later' ) <> NULL ? get_option( 'woocommerce_giftcard_send_later' ) : __('Send Later', 'wpr-pro' )  );
			$rpw_enable_send_later		= ( get_option( 'wpr_enable_send_later' ) <> NULL ? get_option( 'wpr_enable_send_later' ) : "no"  );
			$rpw_reload_card_check 		= ( get_option( 'woocommerce_giftcard_reload_card' ) <> NULL ? get_option( 'woocommerce_giftcard_reload_card' ) : __('Reload Card', 'wpr-pro' )  );

			$numberofGiftCards = 0;

			foreach( $theItems as $item ){
					
				$qty = (int) $item["quantity"];
					 
				$theItem = (int) $item["product_id"];

				$is_giftcard = get_post_meta( $theItem, '_giftcard', true );
				$is_physical  = get_post_meta( $theItem, '_wpr_physical_card', true );
				
				if ( $is_giftcard == "yes" ) {
					 
					for ($i = 0; $i < $qty; $i++){

						if ( ( $item["item_meta"][$rpw_toEmail_check] == "NA") || ( $item["item_meta"][$rpw_reload_card_check] == "" ) ) {
						 
							if( ( $item["item_meta"][$rpw_toEmail_check] <> "NA") && ( $item["item_meta"][$rpw_toEmail_check] <> "") ) {
								$giftCardInfo[$numberofGiftCards]["To Email"] = $item["item_meta"][$rpw_toEmail_check];
							} else {
								$giftCardInfo[$numberofGiftCards]["To Email"] = $current_user->user_email;
							}
								 
							if ( ( $item["item_meta"][$rpw_to_check] <> "NA") && ( $item["item_meta"][$rpw_to_check] <> "") ) {
								$giftCardInfo[$numberofGiftCards]["To"] = $item["item_meta"][$rpw_to_check];
							} else {
								$giftCardInfo[$numberofGiftCards]["To"] = '' . $firstName . ' ' . $lastName . '';
							}
								 
							if ( ( $item["item_meta"][$rpw_note_check] <> "NA") && ( $item["item_meta"][$rpw_to_check] <> "") ) {
								$giftCardInfo[$numberofGiftCards]["Note"] = $item["item_meta"][$rpw_note_check];
							} else {
								$giftCardInfo[$numberofGiftCards]["Note"] = "";
							}

							if ( ( $item["item_meta"][$rpw_address_check] <> "NA") && ( $item["item_meta"][$rpw_address_check] <> "") ) {
								$giftCardInfo[$numberofGiftCards]["Address"] = $item["item_meta"][$rpw_address_check];
							} else {
								$giftCardInfo[$numberofGiftCards]["Address"] = "";
							}

							if ( isset( $item["item_meta"][$rpw_send_later] ) ) {
								if ( ( $item["item_meta"][$rpw_send_later] <> "NA") && ( $item["item_meta"][$rpw_send_later] <> "" ) ) {
									$giftCardInfo[$numberofGiftCards]["SendLater"] = $item["item_meta"][$rpw_send_later];
								} else {
									$giftCardInfo[$numberofGiftCards]["SendLater"] = "";
								}
							}
							
							$product = new WC_Product ( $theItem );

							$giftCardTotal = (float) $item["subtotal"];

							$giftCardInfo[$numberofGiftCards]["Balance"] = $giftCardTotal / $qty;
							$giftCardInfo[$numberofGiftCards]["Amount"] = $giftCardTotal / $qty;
							$giftCardInfo[$numberofGiftCards]["Description"] = 'Generated from Website';
							$giftCardInfo[$numberofGiftCards]["From"] = '' . $firstName . ' ' . $lastName . '';
							$giftCardInfo[$numberofGiftCards]["From Email"] = $order->get_billing_email();
							$giftCardInfo[$numberofGiftCards]["Expiry Date"] = '';
							$giftCardInfo[$numberofGiftCards]["Physical"] = $is_physical;

							$deafultExpiry = ( get_option ( 'rpgc_auto_defaultExpiry' ) <> NULL ? get_option ( 'rpgc_auto_defaultExpiry' ) : "NA" );

							if( $deafultExpiry != "NA" ) {
								$delayStart = 0; // Send later days so it gives the right number of days from when it is sent out.
								$timeToExpire = (int) $deafultExpiry;

								$totalTime = $timeToExpire + $delayStart;

								if( $timeToExpire > 0 ) {
									$newdate = strtotime ( '+' . $totalTime . ' day' , strtotime ( 'today' ) ) ;
									$giftCardInfo[$numberofGiftCards]["Expiry Date"] = date ( 'Y-m-j' , $newdate );
								}
							}

							$numberofGiftCards++;
						}
					}
						 
				}
			
			}

			$giftNumbers = array();

			$giftcard = new WPR_Giftcard();
			for ($i = 0; $i < $numberofGiftCards; $i++){

				// Create gift card object
				$my_giftCard = array(
					'post_title'	=> $giftcard->generateNumber(),
					'post_status'	=> 'publish',
					'post_type'		=> 'rp_shop_giftcard',
					'post_author'	=> 1,
				);

				// Insert the gift card into the database
				$post_id = wp_insert_post( $my_giftCard );

				$giftCardInfo[$i]["ID"] = $post_id;
				$giftCardInfo[$i]["Number"] = $my_giftCard["post_title"];

				$giftNumbers[] = $giftCardInfo[$i]["ID"];

				if ( isset( $giftCardInfo[$i]['Description'] ) ) {
		            $giftCard['description']    = wc_clean( $giftCardInfo[$i]['Description'] );
		        }


		        if ( isset( $giftCardInfo[$i]['To'] ) ) {
		            $giftCard['to'] = wc_clean( $giftCardInfo[$i]['To'] );
		        }

		        if ( isset( $giftCardInfo[$i]['To Email'] ) ) {
		        	if ( $giftCardInfo[$i]["To Email"] != "NA") {
						$giftCard['toEmail']        = wc_clean( $giftCardInfo[$i]["To Email"] );
					} else {
						$giftCard['toEmail']        = wc_clean( $giftCardInfo[$i]["From Email"] );
					}
		        }

		        if ( isset( $giftCardInfo[$i]['From'] ) ) {
		            $giftCard['from']           = wc_clean( $giftCardInfo[$i]['From'] );
		        }

		        if ( isset( $giftCardInfo[$i]['From Email'] ) ) {
		            $giftCard['fromEmail']      = wc_clean( $giftCardInfo[$i]['From Email'] );
		        }

		        if ( isset( $giftCardInfo[$i]['Amount'] ) ) {
		            $giftCard['amount']         = wc_clean( $giftCardInfo[$i]['Amount'] );
		            $giftCard['balance']		= $giftCard['amount'];
		        }

		        if ( isset( $giftCardInfo[$i]['Note'] ) ) {
		            $giftCard['note']   = wc_clean( $giftCardInfo[$i]['Note'] );
		        }

		        if ( $giftCardInfo[$i]['Expiry Date'] != '' ) {
		            $giftCard['expiry_date'] = wc_clean( strftime("%Y-%m-%d", strtotime( $giftCardInfo[$i]['Expiry Date'] ) ) );
		        } else {
		            $giftCard['expiry_date'] = '';
		        }

				if ( isset( $giftCardInfo[$i]['Address'] ) ) {
		            $giftCard['Address']      = wc_clean( $giftCardInfo[$i]['Address'] );
		        }

				if ( isset( $giftCardInfo[$i]['SendLater'] ) ) {
		            $giftCard['SendLater']      = wc_clean( $giftCardInfo[$i]['SendLater'] );
		        }

		        if ( isset( $giftCardInfo[$i]['Physical'] ) ) {
		            $physicalCard      = wc_clean( $giftCardInfo[$i]['Physical'] );
		        }

		        $giftCard['sendTheEmail'] = 0;
		        				
				update_post_meta( $post_id, '_wpr_giftcard', $giftCard );

				$wpr_when = ( get_option ( 'rpgc_auto_when' ) <> NULL ? get_option( 'rpgc_auto_when' ) : "order" );

				if ( $wpr_when != "order" ) {
					
					//if ( isset( $giftCard['SendLater'] ) != true ) {
						$email = new WPR_Giftcard_Email();
						$post = get_post( $post_id );
						$email->sendEmail ( $post );

						$giftCard['sendTheEmail'] = 1;
						update_post_meta( $post_id, '_wpr_giftcard', $giftCard );
					//}
				}
			}

			update_post_meta( $order_id, 'rpgc_numbers', $giftNumbers );

		}

		public function rpgc_send_automatically ( $order_id ) {

			$theCards = array();
			$importedCards = get_post_meta( $order_id, 'rpgc_numbers', true);

			if( $importedCards != '' ) {
				if( ! is_array( $importedCards ) ) {
					$theCards[0] = $importedCards;
				} else {
					$theCards = $importedCards;
				}

				foreach( $theCards as $card ){
					$toSend = get_post_meta( $card, '_wpr_giftcard', true );

					if( $toSend['sendTheEmail'] == 0 ) {
						

							$email = new WPR_Giftcard_Email();
							$post = get_post( $card );
							$email->sendEmail ( $post );

							$toSend['sendTheEmail'] = 1;

							update_post_meta( $card, '_wpr_giftcard', $toSend );
						
					}
				}
			}
		}

		public function wpr_setup_send_later( $post_id ) {
			$canSendLater = get_option( 'wpr_enable_send_later' );
			$is_reload	  = get_post_meta( $post_id, '_wpr_allow_reload', true );
			$is_physical  = get_post_meta( $post_id, '_wpr_physical_card', true );

			if ( ( $canSendLater == "yes" ) && ( $is_physical == "no" ) ){
				$rpgc_send_later		= ( isset( $_POST['rpgc_send_later_check'] ) ? sanitize_text_field( $_POST['rpgc_send_later_check'] ) : ""  );
				$rpgc_send_later_date	= ( isset( $_POST['rpgc_send_later_date'] ) ? sanitize_text_field( $_POST['rpgc_send_later_date'] ) : ""  );

				?><br />
				<div class="hide-on-reload">
					<input type="checkbox" name="rpgc_send_later_check" id="rpgc_send_later_check" <?php if ( $rpgc_send_later == "on") { echo "checked=checked"; } ?>> <label for="rpgc_send_later_check"><?php _e('Send Gift Card Later', 'wpr-pro'); ?></label>
					<input type="text" name="rpgc_send_later_date" id="rpgc_send_later_date" class="input-text show-on-send-later" style="margin-bottom:5px; display:none;" placeholder="<?php _e('Enter Sending Date', 'wpr-pro' ); ?>" value="<?php echo $rpgc_send_later_date; ?>">
				</div>
			<?php
			}
		}

		public function wpr_add_send_later_field( $giftValue ) {
			if ( isset( $giftValue['SendLater'] ) ) {
				// Send Later
				woocommerce_wp_text_input(
					array(
						'id' 						=> 'rpgc_send_later',
						'label' 					=> __( 'Send Later Date', 'wpr-pro' ),
						'description' 				=> __( 'The date this Gift Card will be sent out, <code>YYYY-MM-DD</code>.', 'wpr-pro' ),
						'class' 					=> 'date-picker short',
						'custom_attributes' 		=> array( 'pattern' => "[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" ),
						'value'						=> isset( $giftValue['SendLater'] ) ? $giftValue['SendLater'] : ''
					)
				);
			}
		}

		public function wpr_send_gift_cards() {

			$lastSent = get_option( 'wpr_check_send' );
			$date = date('Y-m-d H:i:s');

			if ( $date >= $lastSent ) {
				$tomorrow = date('Y-m-d H:i:s', strtotime($date . ' +1 day'));
				update_option( 'wpr_check_send', $tomorrow );
			
				$args = array( 'post_type' => 'rp_shop_giftcard', 'posts_per_page' => -1 );
				$myposts = get_posts( $args );

				foreach ( $myposts as $key => $post ) {
					$giftCard = get_post_meta( $post->ID, '_wpr_giftcard', true );
					if ( isset( $giftCard['SendLater'] ) ) {
						if ( ( $giftCard['sendTheEmail'] == 0 ) && ( $giftCard['SendLater'] <= date('Y-m-d') ) ) {
						
							$email = new WPR_Giftcard_Email();
							$post = get_post( $post->ID );
							$email->sendEmail ( $post );
						
							$giftCard['sendTheEmail'] = 1;
							update_post_meta( $post->ID, '_wpr_giftcard', $giftCard );
						}
					}
				}
			}
		}
	}
//******************************************//

}


function wpr_add_autoSend () {
	if( ! class_exists( 'WPRWooGiftcards' ) ) {
        if( ! class_exists( 'WPR_Auto_Giftcard_Activation' ) ) {
            require_once 'includes/class.activation.php';
        }

        $activation = new WPR_Auto_Giftcard_Activation( plugin_dir_path( __FILE__ ), basename( __FILE__ ) );
        $activation = $activation->run();
        
        //return WPRWooGiftcards::instance();
    } else {
        $wprgift_pro_loaded = WPRWG_Auto_Send::getInstance();
    }
}


//function wpr_add_autoSend () {
//	$wprgift_autosend_loaded = WPRWG_Auto_Send::getInstance();
//}
add_action( 'plugins_loaded', 'wpr_add_autoSend', 25 );




