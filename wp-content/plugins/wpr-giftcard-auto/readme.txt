==== WooCommerce - Gift Cards | Auto Send Gift Card ====
Contributors: rpletcher
Tags: woocommerce, gift, gift card, payment, gift certificate, certificate
Requires at least: 3.0
Tested up to: 4.8.0
Stable tag: 1.9.8
Donate link: https://wp-ronin.com/
License: GPLv2 or later

WooCommerce: Gift Cards Auto Send allows you to automaticly send gift cards

== Description ==

WooCommerce: Gift Cards Pro allows you to add new features to help create gift cards that your customers purchase on your site.

== Installation ==
1. Install the WooCommerce: Gift Cards Auto Send plugin
2. Activate the plugin
3. Select any of the features you would like to enable

== Changelog ==
= 1.9.7 =
* FIX: Function calls for Woocommerce Version 3
* ADD: Language file for translations

= 1.9.6 =
* FIX: Auto send fix