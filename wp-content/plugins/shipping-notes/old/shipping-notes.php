<?php
/*
Plugin Name: Shipping Notes
Plugin URI: 
Description: Allow users to enter shipping notes for their account
Version: 1.0.0
Author: Joseph Wu
Author URI: http://www.josephjwu.com
Text Domain: shipping-notes

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

class Shipping_Notes {

  public function __construct() {
    $this->hooks();
    add_shortcode( 'edit-shipping-notes', array( $this, 'edit_shipping_notes'));
  }

  public function hooks() {
    add_filter( 'query_vars', array( $this, 'query_vars' ) );
    add_filter( 'woocommerce_locate_template', array( $this, 'locate_template'), 20, 3);
    add_filter( 'wc_get_template', array( $this, 'get_template'), 10, 5);
    add_action('init', array( $this, 'add_endpoint_url'));
    add_action( 'parse_request', array( $this, 'edit_shipping_notes_parse_request') );
  }

  public function edit_shipping_notes() {
    wc_get_template( 'myaccount/form-edit-shipping-notes.php' );
  }

  public function add_endpoint_url(){
    global $wp_rewrite;
    add_rewrite_rule('my-account/edit-shipping-notes/?$', 'index.php?edit-shipping-notes=true', 'top');
  }

  public function get_template($located, $template_name, $args, $template_path, $default_path) {

    if( $template_name === 'myaccount/my-shipping-notes.php' ) {

	$located = get_template_directory() . '/woocommerce/templates/myaccount/my-shipping-notes.php';
	if ( !file_exists($located) ) $located = plugin_dir_path( __FILE__ ) . 'templates/myaccount/my-shipping-notes.php';
    }
    return $located;
  }

  public function locate_template($template, $template_name, $template_path ){

    if( $template_name === 'myaccount/my-shipping-notes.php' ) {

	$template = get_template_directory() . '/woocommerce/templates/myaccount/my-shipping-notes.php';
	if ( !file_exists($template) )
		$template = plugin_dir_path( __FILE__ ) . 'templates/myaccount/my-shipping-notes.php';
    }

    if( $template_name == 'myaccount/form-edit-shipping-notes.php' ) {

	$template = get_template_directory() . '/woocommerce/templates/myaccount/form-edit-shipping-notes.php';
	if ( !file_exists($template) )
		$template = plugin_dir_path( __FILE__ ) . 'templates/myaccount/form-edit-shipping-notes.php';
    }

    return $template;
  }

  public function edit_shipping_notes_parse_request( &$wp ) {
      if ( array_key_exists( 'edit-shipping-notes', $wp->query_vars ) ) {
        $template = get_template_directory() . '/woocommerce/templates/myaccount/form-edit-shipping-notes.php';
	if ( !file_exists($located) ) $template = plugin_dir_path( __FILE__ ) . 'templates/myaccount/form-edit-shipping-notes.php';
        include(STYLESHEETPATH."/page.php");
        include( $template );
        exit();
    }
  }
  public function query_vars( $qv ) {
    $qv[] = 'edit-shipping-notes';
    return $qv;
  }
}

new Shipping_Notes;


 
