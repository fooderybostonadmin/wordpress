<?php
/**
 * My Account Page
 *
 * @author 	Joseph Wu
 * @package 	WooCommerce/Templates
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


$current_user = wp_get_current_user();
$current_user_id = get_current_user_id();

  //response generation function
  $response = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
   //Store Notes value if it's changed
   $notes = trim($_POST["shipping_notes"]);
   $originalnotes = trim(get_user_meta($current_user_id, 'shipping-notes-note', true));
   if ($notes != $originalnotes)
       update_user_meta($current_user_id, 'shipping-notes-note', $notes);
   
   $response = "<div class='success' style='text-align:center; align:left width:80%; padding:5px 9px; border: 1px solid green; color:green; border-radius:3px;'>Your shipping and handling notes have been successfully updated!";
   
   if ( ! WC()->cart->is_empty() )
       $response .= '<br /><br /><div class="woocommerce"><a href="' . esc_url( wc_get_checkout_url() ) . '" class="button checkout wc-forward">Back to Checkout</a></div>';
   
   $response .= '</div>';
       
} else {
   $notes = get_user_meta($current_user_id, 'shipping-notes-note', true);
}

?>

<?php
	printf(__( 'Hello <strong>%1$s</strong> (not %1$s? <a href="%2$s">Sign out</a>).', 'woocommerce' ) . ' ',
		$current_user->display_name,
		wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
	);
?>

<?php wc_print_notices(); ?>

<div id="shipping_notes_form">
	<form method="post" action="<?php echo the_permalink(); ?>">
		<textarea type="text" id="shipping_notes" name="shipping_notes" style="width: 99%; height:auto" rows="3"><?php echo $notes ?></textarea> <br>
		<input type="submit" style="float:right" class="button" name="save_shipping_notes" value="<?php esc_attr_e( 'Save Notes', 'woocommerce' ); ?>" /><br>
		<?php wp_nonce_field( 'woocommerce-shipping-notes-note' ); ?>
	</form>
</div>
<?php echo $response; ?>
