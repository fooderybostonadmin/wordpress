<?php
/*
Plugin Name: Zocalo WPSC Zone Delivery
Plugin URI: http://zocaloconsulting.com
Description: Enables the use of an options panel to set zones (rows) with zipcodes seperated by"
Version: 1.0
Author: Zocalo Consulting Services.
Author URI: http://zocaloconsulting.com
License: GPL2
*/


class wpsc_zone_delivery {

	var $internal_name, $name;


	function wpsc_zone_delivery() {
		$this->internal_name = "zone_delivery";
		$this->name="Zone Delivery";
		$this->is_external=false;
		return true;
	}


	function getName() {
		return $this->name;
	}


	function getInternalName() {
		return $this->internal_name;
	}


	function getForm() {

		$output.="<tr><td>Settings for this delivery plugin are located <a href='../wp-admin/admin.php?page=acf-options'>here</a></td></tr>";

		return $output;
	}


	function submit_form() {
		$nonce=$_REQUEST['aaaaaa'];
		//if (! wp_verify_nonce($nonce, 'aaaaaa') ) return false;

		//if it's not simple shipping - do nothing
		if($_POST['checkpage'] != 'simple' || !isset($_POST['options'])) return false;

		if (!isset($_POST['options'])) $_POST['options'] = '';

		$options = (array)$_POST['options'];
		$prices = (array)$_POST['prices'];
		if ( !empty( $prices ) ) {
			foreach ($prices as $key => $price) {
				if ( is_numeric($price) ) {
					$simple_shipping_options[$options[$key]] = $price;
				}
			}
		}
		update_option('wpsc_zone_delivery', $simple_shipping_options);
		return true;
	}


	function getQuote() {

		global $wpdb, $wpsc_cart;
		if (isset($_SESSION['nzshpcrt_cart'])) {
			$shopping_cart = $_SESSION['nzshpcrt_cart'];
		}
		if (is_object($wpsc_cart)) {
			$price = $wpsc_cart->calculate_subtotal(true);
		}
		$keys = array();
		$values = array();
		$zones = get_field('zone', 'option');
		$i = 0;
		foreach($zones as $zone){
			array_push($keys, 'zone'.$i);
			array_push($values, $zone['rate']);
			$i++;
		};
		$layers = array_combine($keys, $values);

		if ($layers != '') {
			return $layers;
		}
	}


	function get_item_shipping(&$cart_item) {

		global $wpdb, $wpsc_cart;

		$unit_price = $cart_item->unit_price;
		$quantity = $cart_item->quantity;
		$weight = $cart_item->weight;
		$product_id = $cart_item->product_id;

		$uses_billing_address = false;
		foreach ($cart_item->category_id_list as $category_id) {
			$uses_billing_address = (bool)wpsc_get_categorymeta($category_id, 'uses_billing_address');
			if ($uses_billing_address === true) {
				break; /// just one true value is sufficient
			}
		}

		if (is_numeric($product_id) && (get_option('do_not_use_shipping') != 1)) {
			if ($uses_billing_address == true) {
				$country_code = $wpsc_cart->selected_country;
			} else {
				$country_code = $wpsc_cart->delivery_country;
			}

			if ($cart_item->uses_shipping == true) {
				//if the item has shipping
				$additional_shipping = '';
				if (isset($cart_item->meta[0]['shipping'])) {
					$shipping_values = $cart_item->meta[0]['shipping'];
				}
				if (isset($shipping_values['local']) && $country_code == get_option('base_country')) {
					$additional_shipping = $shipping_values['local'];
				} else {
					if (isset($shipping_values['international'])) {
						$additional_shipping = $shipping_values['international'];
					}
				}
				$shipping = $quantity * $additional_shipping;
			} else {
				//if the item does not have shipping
				$shipping = 0;
			}
		} else {
			//if the item is invalid or all items do not have shipping
			$shipping = 0;
		}
		return $shipping;
	}

}

function wpsc_zone_delivery_setup() {
	global $wpsc_shipping_modules;
	$zone_delivery = new wpsc_zone_delivery();
	$wpsc_shipping_modules[$zone_delivery->getInternalName()] = $zone_delivery;
}

add_action('plugins_loaded', 'wpsc_zone_delivery_setup');
?>