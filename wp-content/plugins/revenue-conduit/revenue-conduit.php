<?php

/**
 * Plugin Name: Revenue Conduit 
 * Plugin URI: http://www.revenueconduit.com/
 * Description: Revenue Conduit integrates Woocommerce with Marketing Platforms like HubSpot, Infusionsoft and ActiveCampaign.
 * Version: 2.0.0
 * Author: Revenue Conduit
 * Author URI: http://www.revenueconduit.com/
 * Requires at least: 4.1
 * Tested up to: 4.1
 * Woocommerce version at least: 2.4
 *
 * Text Domain: revenue-conduit
 * Domain Path: /i18n/languages/
 *
 * @package revenue-conduit
 * @category Core
 * @author Revenue Conduit
 */


if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}


/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	/****************** Newly added for WC abandoned cart  ****************************/

	register_activation_hook ( __FILE__, 'rc_activate');
	//fire de-activate event after de-activate the plugin	
	register_deactivation_hook( __FILE__, 'rc_deactivate' );
	//fire unistall event after deletion of plugin
	register_uninstall_hook( __FILE__, 'rc_uninstall');

	// Put plugin activation code here
	if(!function_exists('rc_activate')){
		function rc_activate(){
			global $wpdb; 
			$wcap_collate = '';
			if ( $wpdb->has_cap( 'collation' ) ) {
				$wcap_collate = $wpdb->get_charset_collate();
			}

			$table_name = $wpdb->prefix . "usermeta_revenueconduit";
			$index1 = "ix_{$table_name}_entityid_entitytype";
			$index2 = "ix_{$table_name}_entityid_entitytype_metakey";
			$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (

				`rcmeta_id` int(11) NOT NULL AUTO_INCREMENT,

				`entity_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,

				`entity_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,

				`created` datetime NULL,

				`modified` datetime NULL,

				`meta_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,

				`meta_value` mediumtext COLLATE utf8_unicode_ci NOT NULL,

				PRIMARY KEY (`rcmeta_id`)

					) {$wcap_collate} AUTO_INCREMENT=1 ";

			require_once ( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			//check and create index1
			$sql_check_index1 = "SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema=DATABASE() AND table_name = %s AND index_name = %s";
			$sql_check_index1_result = $wpdb->get_results($wpdb->prepare($sql_check_index1, $table_name, $index1));
			if(count( $sql_check_index1_result ) == 0){
				$sql_index1 = "CREATE INDEX {$index1} ON {$table_name} (`entity_id`,`entity_type`)";
				$wpdb->get_results( $sql_index1 );
			}

			//check and create index2
			$sql_check_index2 = "SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema=DATABASE() AND table_name = %s AND index_name = %s";
			$sql_check_index2_result = $wpdb->get_results($wpdb->prepare($sql_check_index2, $table_name, $index2));
			if(count( $sql_check_index2_result ) == 0){
				$sql_index2 = "CREATE INDEX {$index2} ON {$table_name} (`entity_id`,`entity_type`,`meta_key`)";
				$wpdb->get_results( $sql_index2 );
			}
		}
	}

	// Put plugin de-activate code here
	if(!function_exists('rc_deactivate')){
		function rc_deactivate(){
			
		}
	}

	// Put plugin uninstall code here
	if(!function_exists('rc_uninstall')){
		function rc_uninstall(){
			global $wpdb;

			$table_name = $wpdb->prefix . "usermeta_revenueconduit";

			$sql = "DROP TABLE IF EXISTS " . $table_name ;

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			$wpdb->get_results( $sql );
		}
	}

	/****************************** End here  *************************************/

	if(!function_exists('rc_save_extra_checkout_fields')){
		function rc_save_extra_checkout_fields($order_id){
			// don't forget appropriate sanitization if you are using a different field type
			$rc_affiliate_id = trim($_COOKIE["rc_affiliate_id"]);
			setcookie("rc_affiliate_id", "",time()-(60*60),"/");
			if(!empty($rc_affiliate_id)) {
				update_post_meta($order_id,'affiliate',sanitize_text_field($rc_affiliate_id));
			}

			// don't forget appropriate sanitization if you are using a different field type
			$hubspotutk = trim($_COOKIE["hubspotutk"]);
			if(!empty($hubspotutk)) {
				update_post_meta($order_id,'hubspotutk',sanitize_text_field($hubspotutk));
			}

			/****************** Newly added for WC abandoned cart  ****************************/
			// don't forget appropriate sanitization if you are using a different field type
			$rc_cart_hash = trim($_COOKIE["rc_cart_hash"]);
			setcookie("rc_cart_hash", "",time()-(60*60),"/");
			if(!empty($rc_cart_hash)) {
				update_post_meta($order_id,'rc_cart_hash',sanitize_text_field($rc_cart_hash));
			}
			/****************************** End here  *************************************/
		}
	}

	/****************************** Newly added for WC abandoned cart  *************************************/
	if(!function_exists('rc_checkout_create')){
		function rc_checkout_create($rc_cart_hash){
			//add code here
			return $rc_cart_hash;
		}
	}

	if(!function_exists('rc_checkout_update')){
		function rc_checkout_update($rc_cart_hash){
			//add code here
			return $rc_cart_hash;
		}
	}

	if(!function_exists('load_user_data_js')){
		function load_user_data_js(){
			$rc_cart_hash = trim($_COOKIE["rc_cart_hash"]);
			$email = null;
			$billing_first_name = null;
			$billing_last_name = null;

			if(!empty($rc_cart_hash)){
				global $wpdb;

				$query = "SELECT meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && (meta_key = 'email' || meta_key = 'first_name' || meta_key = 'last_name')";

				$results = $wpdb->get_results($wpdb->prepare($query,$rc_cart_hash));

				if(!empty($results)){
					foreach($results as $result){
						if(!empty($result->meta_key) && !empty($result->meta_value)){
							switch ($result->meta_key) {
								case 'email':
									$email = $result->meta_value;
									break;
								case 'first_name':
									$billing_first_name = $result->meta_value;
									break;
								case 'last_name':
									$billing_last_name = $result->meta_value;
									break;
							}
						}
					}
				}elseif(is_user_logged_in()){
					$current_user = wp_get_current_user();
					$email = (!empty($current_user->user_email)) ? $current_user->user_email : null;
					$billing_first_name = (!empty($current_user->user_firstname)) ? $current_user->user_firstname : null;
					$billing_last_name = (!empty($current_user->user_lastname)) ? $current_user->user_lastname : null;
				}
			}

			$set_email_val_jquery = '';
			$set_billing_first_name_val_jquery = '';
			$set_billing_last_name_val_jquery = '';
			if(!empty($email)){
				$set_email_val_jquery = 'jQuery(\'input#billing_email\').val("'.$email.'")';
			}

			if(!empty($billing_first_name)){
				$set_billing_first_name_val_jquery = 'jQuery(\'input#billing_first_name\').val("'.$billing_first_name.'")';
			}

			if(!empty($billing_last_name)){
				$set_billing_last_name_val_jquery = 'jQuery(\'input#billing_last_name\').val("'.$billing_last_name.'")';
			}

		echo '<script type="text/javascript">
				'.$set_email_val_jquery.'

				'.$set_billing_first_name_val_jquery.'

				'.$set_billing_last_name_val_jquery.'

        			jQuery( \'input#billing_email\' ).on( \'change\', function() {
					rc_validate_and_post();
            			});

				jQuery( \'input#billing_first_name\' ).on( \'change\', function() {
					rc_validate_and_post();
            			});

				jQuery( \'input#billing_last_name\' ).on( \'change\', function() {
					rc_validate_and_post();
            			});

				function rc_validate_and_post(){
					billing_first_name =  jQuery.trim(jQuery(\'#billing_first_name\').val());
					billing_last_name = jQuery.trim(jQuery(\'#billing_last_name\').val());
					billing_email = jQuery.trim(jQuery(\'#billing_email\').val());

					if(!billing_first_name || !billing_last_name || !billing_email){
					}else{
						if(RCValidateEmail(billing_email)){
							rc_post_user_data();
						}
					}
				} 

				// Function that validates email address through a regular expression.
				function RCValidateEmail(email) {
					var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
					if (filter.test(email)) {
						return true;
					}
					else {
						return false;
					}
				}

				function rc_post_user_data(){
				   var data = {
                    			billing_first_name	: jQuery(\'#billing_first_name\').val(),
                    			billing_last_name	: jQuery(\'#billing_last_name\').val(),
                    			billing_email		: jQuery(\'#billing_email\').val(),
                    			action: \'save_user\'
                    		   };					

				   jQuery.post( "'.get_admin_url().'admin-ajax.php", data, function(response) {

				   });
				}

		</script>';

		}
	}

	if(!function_exists('save_user_callback')){
		function save_user_callback() {		
			//if ( ! is_user_logged_in() ) {            
				global $woocommerce;   

				$cart = array(); 
				if(!empty($_POST['billing_email'])){
					$email = strtolower(trim($_POST['billing_email']));
				}else{
					$email = null;
				}
				if (!empty($_POST['billing_first_name'])){
					$billing_first_name = trim($_POST['billing_first_name']);
				} else {
					$billing_first_name = '';
				}
				if (!empty($_POST['billing_last_name'])){
					$billing_last_name = trim($_POST['billing_last_name']);
				} else {
					$billing_last_name = '';
				}
				if ( function_exists('WC') ) {
					$cart['cart'] = WC()->session->cart;
				} else {
					$cart['cart'] = $woocommerce->session->cart;
				}
				// If a record is present in the users table for the same email id
				if(!empty($email) && !empty($billing_first_name) && !empty($billing_last_name) && !empty($cart['cart'])){
					$rc_cart_hash = trim($_COOKIE["rc_cart_hash"]);
					if(empty($rc_cart_hash)){
						//$rc_cart_hash = uniqid();
						$rc_cart_hash = (int)(microtime(true) * 1000);
						setcookie("rc_cart_hash",$rc_cart_hash ,time()+(60*60),"/");
					}
					if(!empty($rc_cart_hash)){
						saveCheckoutData($rc_cart_hash,$email,$billing_first_name,$billing_last_name,$cart['cart']);
					}
				}
			//}
		}
	}	

	if(!function_exists('saveCheckoutData')){
		function saveCheckoutData($rc_cart_hash,$email,$billing_first_name,$billing_last_name,$cart,$user_id = 0){
			global $wpdb;

			if(!empty($email) && !empty($billing_first_name) && !empty($billing_last_name) && !empty($cart) && !empty($rc_cart_hash)){
				$cart_data = serialize($cart);
				$email_query = "SELECT rcmeta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'email'";
				$first_name_query = "SELECT rcmeta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'first_name'";
				$last_name_query = "SELECT rcmeta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'last_name'";
				$cart_query = "SELECT rcmeta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'rc_persistent_cart'";
				if(!empty($user_id)){
					$user_id_query = "SELECT rcmeta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'user_id'";
				}

				$email_result = $wpdb->get_row($wpdb->prepare($email_query,$rc_cart_hash));
				unset($email_query);
				$first_name_result = $wpdb->get_row($wpdb->prepare($first_name_query,$rc_cart_hash));
				unset($first_name_query);
				$last_name_result = $wpdb->get_row($wpdb->prepare($last_name_query,$rc_cart_hash));
				unset($last_name_query);
				$cart_result = $wpdb->get_row($wpdb->prepare($cart_query,$rc_cart_hash));
				unset($cart_query);

				if(!empty($user_id_query)){
					$user_id_result = $wpdb->get_row($wpdb->prepare($user_id_query,$rc_cart_hash));
					unset($user_id_query);
				}

				$current_time = current_time( 'mysql' );

				$is_update = false;

				$email_query = '';
				$first_name_query = '';
				$last_name_query = '';
				$cart_query = '';
				$user_id_query = '';
				if(!empty($user_id)){
					if(!empty($user_id_result) && !empty($user_id_result->rcmeta_id)){
						if($user_id_result->meta_value !== $user_id){
							$user_id_query = "UPDATE `" . $wpdb->prefix . "usermeta_revenueconduit` SET meta_value = '".$user_id."', modified = '".$current_time."' WHERE rcmeta_id = '".$user_id_result->rcmeta_id."' ";
						}
					}else{
						$user_id_query = "INSERT INTO `".$wpdb->prefix . "usermeta_revenueconduit`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$rc_cart_hash."', 'checkout', 'user_id', '".$user_id."', '".$current_time."', '".$current_time."' )";
					}
					if(!empty($user_id_query)){
						$wpdb->query( $user_id_query );
					}
				}

				if(!empty($email_result) && !empty($email_result->rcmeta_id)){
					if($email_result->meta_value !== $email){
						$email_query = "UPDATE `" . $wpdb->prefix . "usermeta_revenueconduit` SET meta_value = '".$email."', modified = '".$current_time."' WHERE rcmeta_id = '".$email_result->rcmeta_id."' ";
						$is_update = true;
					}
				}else{
					$email_query = "INSERT INTO `".$wpdb->prefix . "usermeta_revenueconduit`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$rc_cart_hash."', 'checkout', 'email', '".$email."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($email_query)){
					$wpdb->query( $email_query );
				}

				if(!empty($first_name_result) && !empty($first_name_result->rcmeta_id)){
					if($first_name_result->meta_value !== $billing_first_name){
						$first_name_query = "UPDATE `" . $wpdb->prefix . "usermeta_revenueconduit` SET meta_value = '".$billing_first_name."', modified = '".$current_time."' WHERE rcmeta_id = '".$first_name_result->rcmeta_id."' ";
						$is_update = true;
					}
				}else{
					$first_name_query = "INSERT INTO `".$wpdb->prefix . "usermeta_revenueconduit`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$rc_cart_hash."', 'checkout', 'first_name', '".$billing_first_name."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($first_name_query)){
					$wpdb->query( $first_name_query );
				}

				if(!empty($last_name_result) && !empty($last_name_result->rcmeta_id)){
					if($last_name_result->meta_value !== $billing_last_name){
						$last_name_query = "UPDATE `" . $wpdb->prefix . "usermeta_revenueconduit` SET meta_value = '".$billing_last_name."', modified = '".$current_time."' WHERE rcmeta_id = '".$last_name_result->rcmeta_id."' ";
						$is_update = true;
					}
				}else{
					$last_name_query = "INSERT INTO `".$wpdb->prefix . "usermeta_revenueconduit`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$rc_cart_hash."', 'checkout', 'last_name', '".$billing_last_name."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($last_name_query)){
					$wpdb->query( $last_name_query );
				}

				if(!empty($cart_result) && !empty($cart_result->rcmeta_id)){
					if($cart_result->meta_value !== $cart_data){
						$cart_query = "UPDATE `" . $wpdb->prefix . "usermeta_revenueconduit` SET meta_value = '".$cart_data."', modified = '".$current_time."' WHERE rcmeta_id = '".$cart_result->rcmeta_id."' ";
						$is_update = true;
					}
				}else{
					$cart_query = "INSERT INTO `".$wpdb->prefix . "usermeta_revenueconduit`( entity_id, entity_type, meta_key, meta_value, created, modified) VALUES ( '".$rc_cart_hash."', 'checkout', 'rc_persistent_cart', '".$cart_data."', '".$current_time."', '".$current_time."' )";
				}
				if(!empty($cart_query)){
					$wpdb->query( $cart_query );
				}

				if($is_update){
					//fire bolow action to checkout update webhook
					do_action( 'woocommerce_rc_checkout_update_action', $rc_cart_hash );
				}else{
					//fire bolow action to checkout create webhook
					do_action( 'woocommerce_rc_checkout_create_action', $rc_cart_hash );
				}
			}
		}
	}

	if(!function_exists('rc_save_custom_persistant_cart')){
		function rc_save_custom_persistant_cart(){
			global $wpdb;
			$user_id = get_current_user_id();
			$current_cart = WC()->session->get( 'cart' );
			$rc_cart_hash = trim($_COOKIE["rc_cart_hash"]);	
			if (!empty($user_id) && !empty( $current_cart ) && is_array( $current_cart ) &&  sizeof( $current_cart ) > 0 ){
				if(empty($rc_cart_hash)){
					//$rc_cart_hash = uniqid();
					$rc_cart_hash = (int)(microtime(true) * 1000);
                                        setcookie("rc_cart_hash",$rc_cart_hash ,time()+(60*60),"/");
                                }

				$email_query = "SELECT rcmeta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'email'";
				$first_name_query = "SELECT rcmeta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'first_name'";
				$last_name_query = "SELECT rcmeta_id,meta_key,meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'last_name'";

				$email_result = $wpdb->get_row($wpdb->prepare($email_query,$rc_cart_hash));
				unset($email_query);
				$first_name_result = $wpdb->get_row($wpdb->prepare($first_name_query,$rc_cart_hash));
				unset($first_name_query);
				$last_name_result = $wpdb->get_row($wpdb->prepare($last_name_query,$rc_cart_hash));
				unset($last_name_query);

				$current_user = wp_get_current_user();
				if(!empty($email_result) && !empty($email_result->meta_value)){
					$email = $email_result->meta_value;
				}else{
					$email = $current_user->user_email;
				}
				if(!empty($first_name_result) && !empty($first_name_result->meta_value)){
					$billing_first_name = $first_name_result->meta_value;
				}else{
					$billing_first_name = (!empty($current_user->user_firstname)) ? $current_user->user_firstname : 'No Name';
				}
				if(!empty($last_name_result) && !empty($last_name_result->meta_value)){
					$billing_last_name = $last_name_result->meta_value;
				}else{
					$billing_last_name = (!empty($current_user->user_lastname)) ? $current_user->user_lastname : 'No Name';
				}

				if(!empty($rc_cart_hash) && !empty($email) && !empty($billing_first_name) && !empty($billing_last_name)){
					saveCheckoutData($rc_cart_hash,$email,$billing_first_name,$billing_last_name,$current_cart,$user_id);
				}
			}
		}
	}

	if(!function_exists('check_and_load_cart_from_abandoned_url')){
		function check_and_load_cart_from_abandoned_url(){
			if(is_cart() && !empty($_GET['rc_cart_hash'])){
				global $wpdb, $woocommerce;
				
				$rc_cart_hash = trim($_GET['rc_cart_hash']);
				$cart_url = $woocommerce->cart->get_cart_url();
		
				$cart_query = "SELECT meta_value FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout' && meta_key = 'rc_persistent_cart'";

				$meta = $wpdb->get_row($wpdb->prepare($cart_query,$rc_cart_hash));
				unset($cart_query);

				if(!empty($meta->meta_value)){
					$meta_value = unserialize($meta->meta_value);
					if(!empty($meta_value)){
						//add products of meta values in cart
						foreach($meta_value as $line_item){
							if(!empty($line_item['product_id']) && !empty($line_item['quantity'])){
								//added this code to show variants by Vilas
								$variation_id = 0;
								$variation = array();
								if(!empty($line_item['variation_id'])){
									$variation_id = $line_item['variation_id'];
								}
								if(!empty($line_item['variation'])){
									$variation = $line_item['variation'];
								}
								WC()->cart->add_to_cart( $line_item['product_id'], $line_item['quantity'], $variation_id, $variation );
							}
						}
						setcookie("rc_cart_hash",$rc_cart_hash,time()+(60*60),"/");
						wp_redirect($cart_url);
					}
				}
			}
		}
	}

	if(!function_exists('logout_callback')){
		function logout_callback(){
			setcookie("rc_cart_hash", "",time()-(60*60),"/");
		}
	}

	/****************************** End here  *************************************/

	if(!function_exists('rc_parse_request')){
		function rc_parse_request($wp){
			$query_params = $wp->query_vars;
			if(!empty($query_params['affiliate'])){
				setcookie("rc_affiliate_id", trim($query_params['affiliate']),time()+(3600*24*30),"/");
			}
		}
	}

	if(!function_exists('rc_add_query_vars_filter')){
		function rc_add_query_vars_filter( $vars ){
			$vars[] = "affiliate";
			return $vars;
		}
	}

	//add_action($hook,$callback_function,$priority,$no_of_parameter);
	add_filter( 'query_vars', 'rc_add_query_vars_filter' );
	add_action( 'parse_query', 'rc_parse_request');
	add_action('woocommerce_checkout_update_order_meta', 'rc_save_extra_checkout_fields', 10, 1);

	/****************************** Newly added for WC abandoned cart  *************************************/
	add_action( 'wp_ajax_save_user', 'save_user_callback' );
	add_action( 'wp_ajax_nopriv_save_user', 'save_user_callback' );

	add_action('woocommerce_after_checkout_billing_form', 'load_user_data_js');

	add_action('template_redirect', 'check_and_load_cart_from_abandoned_url',10,1);
	add_action('woocommerce_checkout_init', 'rc_save_custom_persistant_cart', 10, 1);
	add_action('wp_logout', 'logout_callback');
				

	/********************* Custom hook function************************/
	add_action('woocommerce_rc_checkout_update_action', 'rc_checkout_update', 10, 1);
	add_action('woocommerce_rc_checkout_create_action', 'rc_checkout_create', 10, 1);	
	
	/******************************************************************/	
	
	add_action( 'woocommerce_api_loaded', function(){
			include_once( 'class-wc-api-rc-checkout.php' );
			});

	add_filter( 'woocommerce_api_classes', function( $classes ){
			$classes[] = 'WC_API_RCCheckout';
			return $classes;
			});

	/****************************** End here  *************************************/
}
?>
