<?php
class WC_API_RCCheckout extends WC_API_Resource {
        protected $base = '/revenueconduit/checkouts';

        public function register_routes( $routes ) {
		# GET|POST /checkouts
                /*$routes[ $this->base ] = array(
                        array( array( $this, 'get_checkouts' ), WC_API_Server::READABLE )
                );
		
		# GET /checkouts/count
                $routes[ $this->base . '/count' ] = array(
                        array( array( $this, 'get_checkouts_count' ), WC_API_Server::READABLE ),
                );*/

		# GET|PUT|DELETE /checkouts/<id>	//accept only digits
                $routes[ $this->base . '/(?P<id>\d+)' ] = array(
                        array( array( $this, 'get_checkout' ), WC_API_Server::READABLE ),
                );

                return $routes;
        }
        
        public function get_checkouts() {
                return array( 'checkout' => 'Data' );
        }

	public function get_checkouts_count() {
                return array( 'checkout' => 'Data' );
        }

	public function get_checkout($id) {
		global $wpdb;
		global $woocommerce;
		$cart_url = $woocommerce->cart->get_cart_url();

		$ret = array();
		$customer_data = array();
		$created = $modified = null;
		$cart_url = $cart_url.'?rc_cart_hash='.$id;

		$query = "SELECT meta_key,meta_value,created,modified FROM `".$wpdb->prefix."usermeta_revenueconduit` WHERE entity_id = %d && entity_type = 'checkout'";

		$results = $wpdb->get_results($wpdb->prepare($query,$id));

		if(!empty($results)){
			foreach($results as $result){
				if(!empty($result->meta_key) && !empty($result->meta_value)){
					switch ($result->meta_key) {
						case 'email':
							$customer_data['email'] = $result->meta_value;
							if(!empty($result->created)){
								$created = $this->getLatestDate($created,$result->created);
							}
							if(!empty($result->modified)){
								$modified = $this->getLatestDate($created,$result->modified);
							}
							break;
						case 'first_name':
							$customer_data['first_name'] = $result->meta_value;
							if(!empty($result->created)){
								$created = $this->getLatestDate($created,$result->created);
							}
							if(!empty($result->modified)){
								$modified = $this->getLatestDate($created,$result->modified);
							}
							break;
						case 'last_name':
							$customer_data['last_name'] = $result->meta_value;
							if(!empty($result->created)){
								$created = $this->getLatestDate($created,$result->created);
							}
							if(!empty($result->modified)){
								$modified = $this->getLatestDate($created,$result->modified);
							}
							break;
						case 'user_id':
							$customer_data['id'] = $result->meta_value;
							break;
						case 'rc_persistent_cart':
							$cart = unserialize($result->meta_value);
							if(!empty($cart)){
								$ret['id'] = $id;
								$ret['cart_url'] = $cart_url;
								$ret['cart'] = $cart;
							}
							if(!empty($result->created)){
								$created = $this->getLatestDate($created,$result->created);
							}
							if(!empty($result->modified)){
								$modified = $this->getLatestDate($created,$result->modified);
							}
							break;
					}
				}
			}
		}

		if(!empty($ret['cart'])){
			if(!empty($created)){
				$ret['created_at'] = $this->server->format_datetime( $created );
			}
			if(!empty($modified)){
				$ret['updated_at'] = $this->server->format_datetime( $modified );
			}
			if(!empty($customer_data)){
				$ret['customer'] = $customer_data;
			}
		}

                return array( 'checkout' => $ret);
        }
	
	private function getLatestDate($current,$fromSource){
		if(!empty($fromSource)){
			if(empty($current)){
				$current = $fromSource;
			}else{
				if($fromSource > $current){
					$current = $fromSource;
				}
			}
		}
		return $current;
	}
}
