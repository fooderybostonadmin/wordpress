<?php
/**
 * WooCommerce Address Validation
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Address Validation to newer
 * versions in the future. If you wish to customize WooCommerce Address Validation for your
 * needs please refer to http://docs.woothemes.com/document/address-validation/ for more information.
 *
 * @package     WC-Address-Validation/Admin
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013-2016, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

defined( 'ABSPATH' ) or exit;

/**
 * Address Validation Admin class
 *
 * Loads / saves the admin settings page, Adds Order admin page customizations
 *
 * @since 1.0
 */
class WC_Address_Validation_Admin {

	/** @var string page tab ID */
	public $tab_id = 'address_validation';


	/**
	 * Setup admin class
	 *
	 * @since  1.0
	 */
	public function __construct() {

		// Add 'Address Validation' tab
		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_tab' ), 100 );

		// Show general settings
		add_action( 'woocommerce_settings_tabs_' . $this->tab_id, array( $this, 'show_settings' ) );

		// Show provider information
		add_action( 'woocommerce_admin_field_wc_address_validation_providers', array( $this, 'show_provider_information' ) );

		// Add admin CSS
		add_action( 'admin_print_styles', array( $this, 'add_styles' ) );

		// Save settings
		add_action( 'woocommerce_update_options_' . $this->tab_id, array( $this, 'save_settings' ) );

		// Save provider settings
		foreach( wc_address_validation()->get_handler_instance()->provider_class_names as $provider_class_name ) {

			add_action( 'woocommerce_update_options_' . $this->tab_id . '_' . $provider_class_name, array( $this, 'save_settings' ) );
		}

		// save active provider on general settings page
		add_action( 'woocommerce_update_options_address_validation', array( $this, 'save_active_provider' ) );
	}


	/**
	 * Add 'Address Validation' tab to WooCommerce Settings after 'Shipping' tab
	 *
	 * @since 1.0
	 * @param array $settings_tabs tabs array sans 'Address Validation' tab
	 * @return array $settings_tabs now with 100% more 'Address Validation' tab!
	 */
	public function add_settings_tab( $settings_tabs ) {

		$new_settings_tabs = array();

		foreach ( $settings_tabs as $tab_id => $tab_title ) {

			$new_settings_tabs[ $tab_id ] = $tab_title;

			// Add our tab after 'Address Validation' tab
			if ( 'shipping' == $tab_id ) {
				$new_settings_tabs[ $this->tab_id ] = __( 'Address Validation', 'woocommerce-address-validation' );
			}
		}

		return $new_settings_tabs;
	}

	/**
	 * Show general settings page
	 *
	 * @since 1.0
	 */
	public function show_settings() {

		$current_section = ( empty( $_REQUEST['section'] ) ) ? '' : sanitize_text_field( urldecode( $_REQUEST['section'] ) );

		$links = array(
			sprintf( '<a href="%1$s"%2$s>%3$s</a>',
				admin_url( 'admin.php?page=wc-settings&tab=address_validation' ),
				$current_section ? '' : ' class="current"',
				__( 'General Options', 'woocommerce-address-validation' )
			)
		);

		foreach ( wc_address_validation()->get_handler_instance()->get_providers() as $provider_class_name => $provider ) {

			$links[] = sprintf( '<a href="%1$s"%2$s>%3$s</a>',
				esc_url( add_query_arg( array( 'section' => get_class( $provider ) ), admin_url( 'admin.php?page=wc-settings&tab=address_validation' ) ) ),
				( $provider_class_name == $current_section ) ? ' class="current"' : '',
				esc_html( $provider->get_title() )
			);
		}

		?><ul class="subsubsub"><li><?php echo implode( ' | </li><li>', $links ); ?></li></ul><br class="clear" /><?php

		// Show provider settings
		if ( $current_section ) {

			foreach ( wc_address_validation()->get_handler_instance()->get_providers() as $provider_class_name => $provider ) {

				if ( $provider_class_name == $current_section ) {
					$provider->admin_options();
					break;
				}
			}

		} else {

			// show general settings
			woocommerce_admin_fields( $this->get_settings() );
		}
	}


	/**
	 * Add styling for providers table
	 *
	 * @since 1.0
	 */
	public function add_styles() {

		?><style>table.wc_address_validation{position:relative}table.wc_address_validation td{vertical-align:middle;padding:7px} table.wc_address_validation th{padding:9px 7px !important;vertical-align:middle} table.wc_address_validation td.name{font-weight:bold} table.wc_address_validation .settings{text-align:right} table.wc_address_validation .radio,table.wc_address_validation .default,table.wc_address_validation .status{text-align:center}table.wc_address_validation .radio .tips,table.wc_address_validation .default .tips,table.wc_address_validation .status .tips{margin:0 auto} table.wc_address_validation .radio input,table.wc_address_validation .default input,table.wc_address_validation .status input{margin:0}</style><?php
	}


	/**
	 * Display provider table
	 *
	 * @since 1.0
	 */
	public function show_provider_information() {
	?>
		<tr valign="top">
			<th scope="row" class="titledesc"><?php _e( 'Providers', 'woocommerce-address-validation' ); ?></th>
			<td class="forminp">
				<table class="wc_address_validation widefat" cellspacing="0">
					<thead>
					<tr>
						<th><?php _e( 'Active?', 'woocommerce-address-validation' ); ?></th>
						<th><?php _e( 'Provider', 'woocommerce-address-validation' ); ?></th>
						<th><?php _e( 'Supported Features', 'woocommerce-address-validation' ); ?></th>
					</tr>
					</thead>
					<tbody>
						<?php foreach ( wc_address_validation()->get_handler_instance()->get_providers() as $provider_class_name => $provider ) : ?>
					<tr>
						<td width="1%" class="radio">
							<input <?php echo ( ! $provider->is_configured() ) ? 'disabled ' : ''; ?>type="radio" name="<?php echo esc_attr( 'wc_address_validation_active_provider' ); ?>" value="<?php echo esc_attr( $provider_class_name ); ?>" <?php checked( get_class( wc_address_validation()->get_handler_instance()->get_active_provider() ), $provider_class_name ); ?>/>
						</td>
						<td>
							<p><strong><?php echo esc_html( $provider->get_title() ); ?></strong><br/>
								<small><?php echo esc_html( __( 'Provider ID', 'woocommerce-address-validation' ) . ': ' . $provider->id ); ?></small></p>
						</td>
						<td>
							<p><?php echo implode( ', ', $provider->get_supported_features() ); ?></p>
						</td>
					</tr>

						<?php endforeach; ?>

					</tbody>
				</table>
			</td>
		</tr>
	<?php
	}


	/**
	 * Save settings page
	 *
	 * @since 1.0
	 */
	public function save_settings() {

		$current_section = ( empty( $_REQUEST['section'] ) ) ? '' : sanitize_text_field( urldecode( $_REQUEST['section'] ) );

		if ( $current_section ) {

			// save provider settings
			$provider = new $current_section();
			do_action( 'wc_address_validation_update_provider_options_' . $provider->id );

		} else {

			// save general settings
			woocommerce_update_options( $this->get_settings() );
		}
	}


	/**
	 * Saves active provider
	 *
	 * @since 1.0
	 */
	public function save_active_provider() {

		$current_section = ( empty( $_REQUEST['section'] ) ) ? '' : sanitize_text_field( urldecode( $_REQUEST['section'] ) );

		if ( ! $current_section ) {

			update_option( 'wc_address_validation_active_provider', ( isset( $_POST[ 'wc_address_validation_active_provider' ] ) ) ? $_POST[ 'wc_address_validation_active_provider' ] : '' );
		}
	}


	/**
	 * Returns settings array for use by output/save functions
	 *
	 * @since  1.0
	 * @return array settings
	 */
	public static function get_settings() {

		return array(

			array(
				'name' => __( 'General Options', 'woocommerce-address-validation' ),
				'type' => 'title'
			),

			array(
				'name'     => __( 'Geocode Addresses?', 'woocommerce-address-validation' ),
				'desc'     => __( 'Enable this to save customer\'s latitude and longitude to their order, if supported by the active provider.', 'woocommerce-address-validation' ),
				'id'       => 'wc_address_validation_geocode_addresses',
				'default'  => 'no',
				'type'     => 'checkbox',
			),

			array(
				'name'     => __( 'Classify Addresses?', 'woocommerce-address-validation' ),
				'desc'     => __( 'Enable this to save a customer\'s address classification (e.g. Residential or Commercial) to their order, if supported by the active provider.', 'woocommerce-address-validation' ),
				'id'       => 'wc_address_validation_classify_addresses',
				'default'  => 'no',
				'type'     => 'checkbox',
			),

			array(
				'name'     => __( 'Force Customer to look-up address via Postcode?', 'woocommerce-address-validation' ),
				'desc'     => __( 'Enable this to force customers to look-up their address via Postcode before displaying the address fields. This is only supported with providers who support Postcode Lookup. ', 'woocommerce-address-validation' ),
				'id'       => 'wc_address_validation_force_postcode_lookup',
				'default'  => 'no',
				'type'     => 'checkbox',
			),

			array(
				'name'     => __( 'Debug Mode', 'woocommerce-address-validation' ),
				'desc'     => __( 'Enable this to output debug messages on the checkout page to help with troubleshooting.', 'woocommerce-address-validation' ),
				'id'       => 'wc_address_validation_debug_mode',
				'default'  => 'no',
				'type'     => 'checkbox',
			),

			array( 'type' => 'wc_address_validation_providers' ), // @see WC_Address_Validation_Admin::show_provider_information()

			array( 'type' => 'sectionend' ),

		);
	}


} // end \WC_Address_Validation_Admin class
