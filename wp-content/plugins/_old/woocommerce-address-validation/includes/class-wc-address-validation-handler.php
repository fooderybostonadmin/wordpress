<?php
/**
 * WooCommerce Address Validation
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Address Validation to newer
 * versions in the future. If you wish to customize WooCommerce Address Validation for your
 * needs please refer to http://docs.woothemes.com/document/address-validation/ for more information.
 *
 * @package     WC-Address-Validation/Handler
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013-2016, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

defined( 'ABSPATH' ) or exit;

/**
 * Address Validation Handler class
 *
 * Handles address validation / postcode lookup
 *
 * @since 1.0
 */
class WC_Address_Validation_Handler {


	/** @var array loaded provider class names*/
	public $provider_class_names;

	/** @var object active provider class */
	private $active_provider;

	/** @var array loaded provider instances */
	private $providers;


	/**
	 * Load providers and setup hooks
	 *
	 * @since 1.0
	 */
	public function __construct() {

		// Allow plugins to add providers
		$this->provider_class_names = apply_filters( 'wc_address_validation_providers',
			array(
				'WC_Address_Validation_Provider_SmartyStreets',
				'WC_Address_Validation_Provider_Crafty_Clicks',
				'WC_Address_Validation_Provider_Postcode_Anywhere',
				'WC_Address_Validation_Provider_Postcode_Software_Dot_Net',
				'WC_Address_Validation_Provider_Postcode_Dot_Nl'
			)
		);

		// load active ('wp' is the earliest hook with access to page conditionals)
		add_action( 'wp', array( $this, 'load_validation' ) );

		// load validation javascript
		add_action( 'wp_enqueue_scripts', array( $this, 'load_validation_js' ) );

		// save latitude/longitude/address classification to order
		add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'save_address_meta' ) );
	}


	/**
	 * Add hooks to show postcode lookup / address validation forms (only on checkout page)
	 *
	 * @since 1.0
	 */
	public function load_validation() {

		global $wp;

		// only load on checkout or edit-address pages and if an active provider is set
		if ( ! $this->is_validation_required() ) {
			return;
		}

		// add postcode lookup if supported
		if ( is_object( $this->get_active_provider() ) && $this->get_active_provider()->supports( 'postcode_lookup' ) ) {

			add_action( 'wp_footer', array( $this, 'show_billing_postcode_lookup' ), 0 );

			if ( WC()->cart->needs_shipping_address() || is_account_page() ) {
				add_action( 'wp_footer', array( $this, 'show_shipping_postcode_lookup' ), 0 );
			}
		}

		/* address validation loaded with JS */
	}


	/**
	 * Load javascript for postcode/address validation on checkout page
	 *
	 * @since 1.0
	 */
	public function load_validation_js() {
		global $wp;

		// only load on checkout or edit-address pages and if an active provider is set
		if ( ! $this->is_validation_required() || ! is_object( $this->get_active_provider() ) ) {
			return;
		}

		$params = array(
			'nonce'                 => wp_create_nonce( 'wc_address_validation' ),
			'debug_mode'            => 'yes' == get_option( 'wc_address_validation_debug_mode' ),
			'force_postcode_lookup' => 'yes' == get_option( 'wc_address_validation_force_postcode_lookup' ),
			'ajax_url'              => admin_url( 'admin-ajax.php', 'relative' ),
			'countries'             => $this->get_active_provider()->get_supported_countries(),
			'billing_postcode'      => is_user_logged_in() ? get_user_meta( get_current_user_id(), 'billing_postcode', true ) : '',
			'shipping_postcode'     => is_user_logged_in() ? get_user_meta( get_current_user_id(), 'shipping_postcode', true ) : '',
			'ajax_loader_url'       => wc_address_validation()->get_framework_assets_url() . '/images/ajax-loader.gif',
		);

		// load postcode lookup JS
		if ( $this->get_active_provider()->supports( 'postcode_lookup' ) ) {

			wp_enqueue_script( 'wc_address_validation_postcode_lookup', wc_address_validation()->get_plugin_url() . '/assets/js/frontend/wc-address-validation-postcode-lookup.min.js', array( 'jquery', 'woocommerce' ), WC_Address_Validation::VERSION, true );

			wp_localize_script( 'wc_address_validation_postcode_lookup', 'wc_address_validation_postcode_lookup', $params );

			// Add a bit of CSS to ensure themes are not hiding the postcode lookup results
			echo '<style type="text/css">.wc_address_validation_results.form-row { overflow: visible !important; }</style>';
		}

		// load address validation JS
		if ( $this->get_active_provider()->supports( 'address_validation' ) && 'WC_Address_Validation_Provider_SmartyStreets' == get_class( $this->get_active_provider() ) ) {

				// load SmartyStreets LiveAddress jQuery plugin
				wp_enqueue_script( 'wc_address_validation_smarty_streets', '//d79i1fxsrar4t.cloudfront.net/jquery.liveaddress/2.8/jquery.liveaddress.min.js', array( 'jquery' ), '2.8', true );

				// load LiveAddress integration script
				wp_enqueue_script( 'wc_address_validation', wc_address_validation()->get_plugin_url() . '/assets/js/frontend/wc-address-validation.min.js', array( 'wc_address_validation_smarty_streets' ), WC_Address_Validation::VERSION, true );

				$params['smarty_streets_key'] = $this->get_active_provider()->html_key;
				$params['plus_four_code']     = $this->get_active_provider()->plus_four_code;

				wp_localize_script( 'wc_address_validation', 'wc_address_validation', $params );
		}

		/**
		 * Allow other providers to load JS for postcode/address validation on checkout page
		 *
		 * @since 1.0
		 * @param object $active_provider An instance of the active provider's class
		 * @param WC_Address_Validation_Handler $wc_address_validation_handler
		 */
		do_action( 'wc_address_validation_load_js', $this->get_active_provider(), $this );

	}


	/**
	 * Helper function to show billing postcode lookup form
	 *
	 * @since 1.0
	 */
	public function show_billing_postcode_lookup() {

		// Get postcode lookup template
		wc_get_template( 'checkout/form-postcode-lookup.php', array( 'address_type' => 'billing', 'requires_house_number' => ( 'postcode_dot_nl' == $this->get_active_provider()->id ) ), '', wc_address_validation()->get_plugin_path() . '/templates/' );
	}

	/**
	 * Helper function to show shipping postcode lookup form
	 *
	 * @since 1.0
	 */
	public function show_shipping_postcode_lookup() {

		// Get postcode lookup template
		wc_get_template( 'checkout/form-postcode-lookup.php', array( 'address_type' => 'shipping', 'requires_house_number' => ( 'postcode_dot_nl' == $this->get_active_provider()->id ) ), '', wc_address_validation()->get_plugin_path() . '/templates/' );
	}


	/**
	 * Get the active provider instance
	 *
	 * @since 1.0
	 * @return object the active provider
	 */
	public function get_active_provider() {

		if ( is_object( $this->active_provider ) ) {
			return $this->active_provider;
		}

		$active_provider = get_option( 'wc_address_validation_active_provider' );

		if ( class_exists( $active_provider ) ) {
			return $this->active_provider = new $active_provider;
		}
	}


	/**
	 * Get all loaded providers
	 *
	 * @since 1.0
	 * @return array providers
	 */
	public function get_providers() {

		if ( is_object( $this->providers ) ) {
			return $this->providers;
		}

		foreach ( $this->provider_class_names as $provider_class ) {

			$this->providers[ $provider_class ] = new $provider_class();
		}

		return $this->providers;
	}


	/**
	 * Save latitude / longitude / address classification as order meta if enabled & available
	 *
	 * @since 1.0
	 * @param int $order_id order ID to save to
	 */
	public function save_address_meta( $order_id ) {

		// geocoding
		if ( 'yes' == get_option( 'wc_address_validation_geocode_addresses' ) ) {

			// latitude
			if ( isset( $_POST['wc_address_validation_latitude'] ) && ! empty( $_POST['wc_address_validation_latitude'] ) && is_numeric( $_POST['wc_address_validation_latitude'] ) ) {
				add_post_meta( $order_id, '_wc_address_validation_latitude', trim( $_POST['wc_address_validation_latitude'] ) );
			}

			// longitude
			if ( isset( $_POST['wc_address_validation_longitude'] ) && ! empty( $_POST['wc_address_validation_longitude'] ) && is_numeric( $_POST['wc_address_validation_latitude'] ) ) {
				add_post_meta( $order_id, '_wc_address_validation_longitude', trim( $_POST['wc_address_validation_longitude'] ) );
			}
		}

		// address classification
		if ( 'yes' == get_option( 'wc_address_validation_classify_addresses' ) && isset( $_POST['wc_address_validation_classification'] ) && ! empty( $_POST['wc_address_validation_classification'] ) ) {
			update_post_meta( $order_id, '_wc_address_validation_classification', trim( $_POST['wc_address_validation_classification'] ) );
		}
	}


	/**
	 * Checks if validation should be loaded on the current page
	 *
	 * @since 1.3.2
	 * @return bool true if validation should be loaded
	 */
	public function is_validation_required() {
		global $wp;

		// check if active_provider is set and we are on the checkout page or edit-address page
		$validation_reqired = ( is_checkout() && empty( $wp->query_vars['order-pay'] ) && ! isset( $wp->query_vars['order-received'] ) )
				|| isset( $wp->query_vars['edit-address'] );

		// in some situations `is_checkout()` can be true in the admin so we must ensure we're not in the admin
		$validation_reqired = ! is_admin() && $validation_reqired;

		// One Page Checkout
		if ( ! $validation_reqired && function_exists( 'is_wcopc_checkout' ) ) {
			$validation_reqired =  is_wcopc_checkout();
		}

		/**
		 * Filter if validation should be loaded on the current page
		 *
		 * @since 1.3.3
		 * @param bool $validation_reqired True if validation should be loaded, false otherwise
		 */
		return apply_filters( 'wc_address_validation_validation_required', $validation_reqired );
	}


} // end \WC_Address_Validation_Providers class
