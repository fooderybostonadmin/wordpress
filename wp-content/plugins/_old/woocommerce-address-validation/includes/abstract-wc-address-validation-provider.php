<?php
/**
 * WooCommerce Address Validation
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Address Validation to newer
 * versions in the future. If you wish to customize WooCommerce Address Validation for your
 * needs please refer to http://docs.woothemes.com/document/address-validation/ for more information.
 *
 * @package     WC-Address-Validation/Provider
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013-2016, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

defined( 'ABSPATH' ) or exit;

/**
 * Address Validation Validator class
 *
 * Extended by address providers to handle address/postcode validation and lookup
 *
 * @since 1.0
 */
abstract class WC_Address_Validation_Provider extends WC_Settings_API {

	/** @var string unique prefix for saving settings */
	public $plugin_id = 'wc_address_validation_';

	/** @var string unique ID for the validator, required */
	public $id;

	/** @var string title used on settings page */
	protected $title;

	/** @var array array of countries this validator is valid for */
	protected $countries = array();

	/** @var array features this validator supports (e.g. post code lookup) */
	protected $supports = array();


	/**
	 * Return the provider's title
	 *
	 * @since 1.0
	 * @return string the provider title
	 */
	public function get_title() {

		if ( empty( $this->title ) ) {
			return ucwords( str_replace( array( '_', '-' ), '', $this->id ) );
		} else {
			return $this->title;
		}
	}


	/**
	 * Return an array of formatted supported features
	 *
	 * @since 1.0
	 * @return array supported features
	 */
	public function get_supported_features() {

		return array_map( 'ucwords', str_replace( '_', ' ', $this->supports ) );
	}


	/**
	 * Return an array of supported countries
	 *
	 * @since 1.2
	 * @return array supported countries
	 */
	public function get_supported_countries() {

		return $this->countries;
	}


	/**
	 * Check if provider is configured correctly, overridden by child providers
	 *
	 * @since 1.0
	 * @return bool true if configured, false otherwise
	 */
	public function is_configured() {

		return true;
	}


	/**
	 * Postcode lookup function stub to be overridden by child providers
	 *
	 * This method only needs to be overridden for providers that support postcode lookup
	 *
	 * @since 1.0
	 * @param string $postcode
	 * @param string $house_number Optional. Used by Postcode.nl.
	 */
	public function lookup_postcode( $postcode, $house_number = '' ) { }


	/**
	 * Check if a provider supports a given feature
	 *
	 * Options = 'postcode_lookup', 'address_validation', 'geocoding', 'address_classification'
	 *
	 * @since 1.0
	 * @param string $feature the name of a feature to test support for
	 * @return bool true if the provider supports the feature, false otherwise
	 */
	public function supports( $feature ) {

		return apply_filters( 'wc_address_validation_provider_supports', in_array( $feature, $this->supports ) ? true : false, $feature, $this );
	}


	/**
	 * Show the title / description and settings for provider
	 *
	 * @since 1.0
	 */
	public function admin_options() {

		// clear current provider as active provider if not configured
		if ( ! $this->is_configured() && get_class( $this ) == get_class( wc_address_validation()->get_handler_instance()->get_active_provider() ) ) {
			update_option( 'wc_address_validation_active_provider', '' );
		}

		?><h3><?php echo esc_html( $this->get_title() ); ?></h3><?php

		echo ( ! empty( $this->description ) ) ? wpautop( $this->description ) : '';

		?><table class="form-table">
		<?php $this->generate_settings_html(); ?>
		<tr valign="top">
		<th scope="row" class="titledesc"><label for="<?php echo esc_attr( $this->id . '_supported_features' ); ?>"><?php _e( 'Supported Features', 'woocommerce-address-validation' ); ?></label></th>
		<td class="forminp">
		<fieldset><legend class="screen-reader-text"><span><?php _e( 'Supported Features', 'woocommerce-address-validation' ); ?></span></legend>
		<p><?php echo esc_html( implode( ', ', $this->get_supported_features() ) ); ?>
		</fieldset></td>
		</tr>
		</table><?php
	}


} // end \WC_Address_Validator_Provider abstract class
