<?php

function fca_pc_premium_save( $data ) {

	//PREMIUM SETTINGS
	$data['utm_support'] = empty( $_POST['fca_pc']['utm_support'] ) ? '' : 'on';
	$data['user_parameters'] = empty( $_POST['fca_pc']['user_parameters'] ) ? '' : 'on';
	$data['advanced_matching'] = empty( $_POST['fca_pc']['advanced_matching'] ) ? '' : 'on';
	$data['amp_integration'] = empty( $_POST['fca_pc']['amp_integration'] ) ? '' : 'on';
	
	//WOOCOMMERCE SETTINGS
	$data['woo_integration'] = empty( $_POST['fca_pc']['woo_integration'] ) ? '' : 'on';
	$data['woo_extra_params'] = empty( $_POST['fca_pc']['woo_extra_params'] ) ? '' : 'on';
	$data['woo_delay'] = empty( $_POST['fca_pc']['woo_delay'] ) ? 0 : intVal( $_POST['fca_pc']['woo_delay'] );
	
	//WOOCOMMERCE FEED
	$data['woo_feed'] = empty( $_POST['fca_pc']['woo_feed'] ) ? '' : 'on';
	$data['woo_excluded_categories'] = empty( $_POST['fca_pc']['woo_excluded_categories'] ) ? array() : array_map( 'fca_pc_sanitize_text_array', $_POST['fca_pc']['woo_excluded_categories'] );
	$data['woo_product_id'] = empty( $_POST['fca_pc']['woo_product_id'] ) ? 'post_id' : $_POST['fca_pc']['woo_product_id'];
	$data['woo_desc_mode'] = empty( $_POST['fca_pc']['woo_desc_mode'] ) ? 'description' : $_POST['fca_pc']['woo_desc_mode'];
	$data['google_product_category'] = empty( $_POST['fca_pc']['google_product_category'] ) ? '' : $_POST['fca_pc']['google_product_category'];
	
	//EDD SETTINGS
	$data['edd_integration'] = empty( $_POST['fca_pc']['edd_integration'] ) ? '' : 'on';
	$data['edd_delay'] = empty( $_POST['fca_pc']['edd_delay'] ) ? 0 : intVal( $_POST['fca_pc']['edd_delay'] );
	$data['edd_extra_params'] = empty( $_POST['fca_pc']['edd_extra_params'] ) ? '' : 'on';
	
	//EDD FEED
	$data['edd_feed'] = empty( $_POST['fca_pc']['edd_feed'] ) ? '' : 'on';
	$data['edd_excluded_categories'] = empty( $_POST['fca_pc']['edd_excluded_categories'] ) ? array() : array_map( 'fca_pc_sanitize_text_array', $_POST['fca_pc']['edd_excluded_categories'] );
	$data['edd_desc_mode'] = empty( $_POST['fca_pc']['edd_desc_mode'] ) ? 'content' : $_POST['fca_pc']['edd_desc_mode'];
	
	//MULTIPLE PIXELS
	$data['ids'] = empty( $_POST['fca_pc']['ids'] ) ? '' : array_filter( array_map( 'fca_pc_bigintval', $_POST['fca_pc']['ids'] ) );

	return $data;
}

function fca_pc_editor_premium_data() {
	$premium_data = array(
		'customParamTemplate' => fca_pc_custom_param_row(),
		'package' => FCA_PC_PLUGIN_PACKAGE,
		'pixelInputTemplate' => fca_sp_draw_pixel_input_row(),
	);
			
	wp_localize_script( 'fca_pc_admin_js', 'fcaPcPremiumData', $premium_data );
}

function fca_pc_additional_pixel_inputs( $id_array = array() ) {
	$hidden = empty( $id_array ) ? 'style="display:none;"' : '';
	
	ob_start(); ?>
	<tr id='fca_sp_pixel_ids' <?php echo $hidden ?>>
		<th>Additional IDs</th>
		<td id='fca_sp_pixel_id_td'>
		<?php 
			forEach( $id_array as $pixel_id ) {
				echo fca_sp_draw_pixel_input_row( $pixel_id );
			
			} ?>
		</td>
	</tr>
	
	<?php 
	return ob_get_clean();

}

function fca_sp_draw_pixel_input_row( $pixel_id = '' ) {
	
	ob_start(); ?>
		<div class="fca-pc-field fca-pc-field-text fca_deletable_item">
			<input type="text" placeholder="e.g. 123456789123456" class="fca-pc-input-text fca-pc-id" name="fca_pc[ids][]" value="<?php echo $pixel_id ?>">
			<?php echo fca_pc_delete_icons(); ?>
		</div>			
	<?php 

	return ob_get_clean();
}