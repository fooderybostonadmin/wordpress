<?php

/************************************
Licensing & Automatic Updates - implementation. Built on top of EDD-SL (https://easydigitaldownloads.com/extensions/software-licensing/) 
http://docs.easydigitaldownloads.com/article/383-automatic-upgrades-for-wordpress-plugins
 ************************************* */
 
// load our custom updater
if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	include FCA_PC_PLUGIN_DIR . '/includes/licensing/EDD_SL_Plugin_Updater.php';
}

function fca_pc_license() {
	
	$license_key = get_option( 'fca_pc_license_key' );
	
	$edd_updater = new EDD_SL_Plugin_Updater( 'https://fatcatapps.com/', FCA_PC_PLUGIN_FILE, array(
			'version'	=> FCA_PC_PLUGIN_VER,
			'license'	=> $license_key,
			'item_name' => FCA_PC_PLUGIN_NAME,
			'author'	=> 'Fatcat Apps',
			'url'		=> home_url()
		)
	);
	
}
add_action( 'admin_init', 'fca_pc_license' );

//register setting sub page   
function fca_pc_license_menu() {
	add_submenu_page(
		'fca_pc_settings_page',
		__('Licensing', 'facebook-conversion-pixel'),
		__('Licensing', 'facebook-conversion-pixel'),
		'manage_options',
		'pixel-cat-license',
		'fca_pc_license_page'
	);
}
add_action( 'admin_menu', 'fca_pc_license_menu' );

function fca_pc_license_page() {
	
	$error_msg = fca_pc_activate_license();
	$error_msg .= fca_pc_deactivate_license();
	$license  = get_option( 'fca_pc_license_key' );
	$status	  = get_option( 'fca_pc_license_status', 'inactive' );
		
	?>

	<div class="wrap">
		<?php if ( $error_msg ) {
			echo	"<div class='notice error'>
						<p>$error_msg</p>
					</div>";
		} ?>
		
		<form method="post">

			<?php wp_nonce_field( 'fca_pc_license_nonce', 'fca_pc_license_nonce' ); ?>

			<h3>
				<?php _e('License', 'facebook-conversion-pixel'); ?>
				<?php if( $status == 'valid' ) { ?>
					<span style="color: #fff; background: #7ad03a; font-size: 13px; padding: 4px 6px 3px 6px; margin-left: 5px;"><?php _e('ACTIVE', 'facebook-conversion-pixel'); ?></span>
				<?php } elseif($status == 'expired' ) { ?>
					<span style="color: #fff; background: #dd3d36; font-size: 13px; padding: 4px 6px 3px 6px; margin-left: 5px;"><?php _e('EXPIRED', 'facebook-conversion-pixel'); ?></span>
				<?php } else { ?>
					<span style="color: #fff; background: #dd3d36; font-size: 13px; padding: 4px 6px 3px 6px; margin-left: 5px;"><?php _e('INACTIVE.', 'facebook-conversion-pixel'); ?></span>
				<?php } ?>
			</h3>

			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e('License Key', 'facebook-conversion-pixel'); ?>
						</th>
						<td>
							<input id="fca_pc_license_key" name="fca_pc_license_key" type="text" class="regular-text" value="<?php echo $license ?>" /><br/>
							<label class="description" for="fca_pc_license_key"><?php _e('Enter your license key', 'facebook-conversion-pixel'); ?></label>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e('Activate License', 'facebook-conversion-pixel'); ?>
						</th>
						<td>
							<?php if( $status == 'valid' ) { ?>
								<input type="submit" class="button-secondary" name="fca_pc_license_deactivate" value="<?php _e('Deactivate License', 'facebook-conversion-pixel'); ?>"/>
							<?php } else { ?>
								<input type="submit" class="button-secondary" name="fca_pc_license_activate" value="<?php _e('Activate License', 'facebook-conversion-pixel'); ?>"/>
							<?php } ?>
						</td>
					</tr>

				</tbody>
			</table>
		</form>
	
	</div>
	<?php
}

/************************************
*	 Activating the license		   * 
************************************* */
function fca_pc_activate_license() {
	if( isset( $_POST['fca_pc_license_activate'] ) && isset( $_POST['fca_pc_license_key'] )  ) {
		// run a quick security check 
		if( !check_admin_referer( 'fca_pc_license_nonce', 'fca_pc_license_nonce' ) ) {
			return; // get out if we didn't click the Activate button
		}
		
		$license = fca_pc_sanitize_license( $_POST[ 'fca_pc_license_key' ] );
		
		$api_params = array(
			'edd_action'=> 'activate_license',
			'license'	=> $license,
			'item_name' => urlencode( FCA_PC_PLUGIN_NAME ), // the name of our product in EDD
			'url'		=> home_url()
		);

		// Call the API.
		$response = wp_remote_post( 'https://fatcatapps.com/', array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.' );
			}
		} else {

			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			if ( false === $license_data->success ) {
			
				switch( $license_data->error ) {
					case 'expired' :
						$message = sprintf(
							__( 'Your license key expired on %s.' ),
							date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
						);
						break;
					case 'revoked' :
						$message = __( 'Your license key has been disabled.' );
						break;
					case 'missing' :
						$message = __( 'Invalid license.' );
						break;
					case 'invalid' :
					case 'site_inactive' :
						$message = __( 'Your license is not active for this URL.' );
						break;
					case 'item_name_mismatch' :
						$message = sprintf( __( 'This appears to be an invalid license key for %s.' ), FCA_PC_PLUGIN_NAME );
						break;
					case 'no_activations_left':
						$message = __( 'Your license key has reached its activation limit.' );
						break;
					default :
						$message = __( 'An error occurred, please try again.' );
						break;
				}

			}
		
		}
				
		// $license_data->license will be either "valid" or "invalid"
		update_option( 'fca_pc_license_status', $license_data->license );
		update_option( 'fca_pc_license_key', $license );
		
		// Check if anything passed on a message constituting a failure
		if ( !empty( $message ) ) {
			return $message;
		}
	}
	
	return false;

}
	
/************************************
* Deactivating the license
************************************/

function fca_pc_deactivate_license() {

	if( isset( $_POST['fca_pc_license_deactivate'] ) ) {

		// run a quick security check 
		if( ! check_admin_referer( 'fca_pc_license_nonce', 'fca_pc_license_nonce' ) ) {
			return; // get out if we didn't click the Activate button
		}

		// retrieve the license from the database
		$license = get_option( 'fca_pc_license_key' );

		// data to send in our API request
		$api_params = array( 
			'edd_action'=> 'deactivate_license', 
			'license'	=> $license,
			'item_name' => FCA_PC_PLUGIN_NAME, // the name of our product in EDD
			'url'		=> home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( 'https://fatcatapps.com/', array( 'body' => $api_params, 'timeout' => 15, 'sslverify' => false ) );
		
		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
					
			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.' );
			}
		}
		

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		if( $license_data->license == 'deactivated' || $license_data->license == 'failed' ) {
			delete_option( 'fca_pc_license_status' );
			delete_option( 'fca_pc_license_key' );
		}
		if ( !empty( $message ) ) {
			return $message;
		}
	}
	
	return false;
}

function fca_pc_sanitize_license( $key ) {
	$old = get_option( 'fca_pc_license_key' );
	if( $old && $old != $key ) {
		delete_option( 'fca_pc_license_status' ); // new license has been entered, so must reactivate
	}
	return htmlentities( trim($key) );
}

//LICENSE CHECK
function fca_pc_check_license() {
	
	$store_url = 'https://fatcatapps.com/';
	$item_name = FCA_PC_PLUGIN_NAME;
	$license = get_option( 'fca_pc_license_key' );
	$api_params = array(
		'edd_action' => 'check_license',
		'license' => $license,
		'item_name' => urlencode( $item_name ),
		'url' => home_url()
	);
	
	$response = wp_remote_post( $store_url, array( 'body' => $api_params, 'timeout' => 15, 'sslverify' => false ) );
	
  	if ( is_wp_error( $response ) ) {
		return false;
  	}

	$license_data = json_decode( wp_remote_retrieve_body( $response ) );
	
	update_option( 'fca_pc_license_status', $license_data->license );
	
}
add_action('fca_pc_license_check', 'fca_pc_check_license');

if( !wp_next_scheduled( 'fca_pc_license_check' ) ) {
	wp_schedule_event(time(), 'daily', 'fca_pc_license_check');
}