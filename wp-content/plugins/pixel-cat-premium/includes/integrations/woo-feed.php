<?php


function fca_pc_woo_rss_feed() {
	$options = get_option( 'fca_pc', array() );
	$post_count = wp_count_posts( 'product' )->publish;
		
	fca_pc_do_feed_head();
	$pages = ceil( $post_count / 100 );
	for ( $i = 0; $i < $pages; $i++ ) {
		fca_pc_do_feed_body( $options, $i );
	}
	fca_pc_do_feed_footer();
	
}

function fca_pc_do_feed_head() {
		
	echo "<?xml version='1.0'?>"; //PHP DOESNT LIKE <? IN THE XML
	ob_start(); ?>

	<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
		<channel>
			<title><?php echo bloginfo( 'name' ) ?></title>
			<link><?php echo site_url() ?></link>
			<description><?php bloginfo( 'description' ) ?></description>
	<?php echo ob_get_clean();
	
}

function fca_pc_do_feed_footer() {
	ob_start(); ?>
		</channel>
	</rss><?php
	echo ob_get_clean();
}

function fca_pc_do_feed_body( $options, $offset = 0 ) {
	
	$search = array(
		'post_type' => 'product',
		'post_status' => 'publish',
		'posts_per_page' => 100,
		'offset' => 100 * $offset,
		'orderby' => 'ID',
		'order' => 'ASC'
	);
	
	$products = get_posts( $search );
	$woo_id_mode = empty( $options['woo_product_id'] ) ? 'post_id' : $options['woo_product_id'];
	$woo_desc_mode = empty( $options['woo_desc_mode'] ) ? 'description' : $options['woo_desc_mode'];
	$excluded_product_cats = empty( $options['woo_excluded_categories'] ) ? array() : $options['woo_excluded_categories'];	
	
	ob_start();
	forEach( $products as $p ) {
		$wc_product = wc_get_product( $p->ID );
		
		//CHECK FOR EXCLUDED CATEGORIES
		$cat_ids = array_merge( $wc_product->get_category_ids(), $wc_product->get_tag_ids() );
		if ( count ( array_intersect( $cat_ids, $excluded_product_cats ) ) === 0 ) {
			$product_id = $woo_id_mode === 'post_id' ? $wc_product->get_id() : $wc_product->get_sku();
			$image = fca_pc_woo_product_image( $p->ID ); ?>
			<item>
				<g:id><?php echo $product_id ?></g:id>
				<g:title><?php echo fca_pc_encode_xml( $wc_product->get_title() ) ?></g:title>
				<g:description><?php echo $woo_desc_mode == 'description' ? fca_pc_encode_xml( $wc_product->get_description() ) : fca_pc_encode_xml( $wc_product->get_short_description() ) ?></g:description>
				<g:link><?php echo $wc_product->get_permalink() ?></g:link>
				<g:image_link><?php echo $image ?></g:image_link>
				<?php fca_pc_woo_feed_brand( $wc_product ) ?>
				<g:condition><?php fca_pc_woo_feed_condition( $wc_product ) ?></g:condition>
				<g:availability><?php echo $wc_product->is_in_stock() ? 'in stock' : 'out of stock' ?></g:availability>
				<g:price><?php fca_pc_woo_feed_price( $wc_product ) ?></g:price>
<?php fca_pc_woo_feed_extra_fields( $wc_product, $options ) ?>
			</item>
	<?php } 
		clean_post_cache( $p->ID );
	}
	
	echo ob_get_clean();
}

function fca_pc_woo_feed_price( $wc_product ) {
	$the_price = wc_get_price_to_display( $wc_product );
	
	if ( $the_price ) {
		echo $the_price . ' ' . get_woocommerce_currency();
	} else {
		echo '0';
	}
}

function fca_pc_woo_feed_condition( $wc_product ) {
	if ( $wc_product->get_attribute('condition') ) {
		echo fca_pc_encode_xml( $wc_product->get_attribute('condition') );
	} else {
		echo 'new';
	}
}

function fca_pc_woo_feed_brand( $wc_product ) {
	if ( $wc_product->get_attribute('gtin') ) {
		echo '<g:gtin>' . fca_pc_encode_xml( $wc_product->get_attribute('gtin') ) . "</g:gtin>\n";
	} else if ( $wc_product->get_attribute('mpn') ) {
		echo '<g:mpn>' . fca_pc_encode_xml( $wc_product->get_attribute('mpn') ) . "</g:mpn>\n";
	} else if ( $wc_product->get_attribute('brand') ) {
		echo '<g:brand>' . fca_pc_encode_xml( $wc_product->get_attribute('brand') ) . "</g:brand>\n";
	} else {
		echo '<g:brand>';
		fca_pc_encode_xml( bloginfo( 'name' ) ); 
		echo "</g:brand>\n";
	}
}

function fca_pc_woo_product_image( $post_id ) {
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
	$placeholder = plugins_url() . '/woocommerce/assets/images/placeholder.png';
	$image = empty( $image[0] ) ? $placeholder : $image[0];
	return $image;
}

function fca_pc_woo_feed_extra_fields( $wc_product, $options ) {
	$extra_fields = array(
		'color',
		'google_product_category',
		'gender',
		'pattern',
		'material',
		'size'
	);
	
	$google_product_category = empty( $options['google_product_category'] ) ? '' : $options['google_product_category'];
	$tabs = "\t\t\t\t";
	forEach ( $extra_fields as $field ) {
		if ( $field === 'google_product_category' ) {
			if ( $wc_product->get_attribute( $field ) ) {
				echo $tabs . "<g:$field>" . fca_pc_encode_xml( $wc_product->get_attribute( $field ) ) . "</g:$field>\n";
			} else if ( $google_product_category ) {
				echo $tabs . "<g:$field>" . fca_pc_encode_xml( $google_product_category ) . "</g:$field>\n";
			}
		} else {
			if ( $wc_product->get_attribute( $field ) ) {
				echo $tabs . "<g:$field>" . fca_pc_encode_xml( $wc_product->get_attribute( $field ) ) . "</g:$field>\n";
			}
		}
	}
}

function fca_pc_encode_xml( $string ) {
	
	return htmlspecialchars ( strip_tags ( $string ) );
}

function fca_pc_add_woo_rss_feed(){
	$options = get_option( 'fca_pc', 'no options' );
	
	if ( !empty( $options['woo_feed'] ) ) {
		add_feed( 'pixelcat', 'fca_pc_woo_rss_feed' );	
	}
}
add_action('init', 'fca_pc_add_woo_rss_feed');

function fca_pc_woo_feed_content_type( $content_type, $type ) {
	if ( 'pixelcat' === $type ) {
		return feed_content_type( 'rss-http' );
	}
	return $content_type;
}
add_filter( 'feed_content_type', 'fca_pc_woo_feed_content_type', 10, 2 );
